//
//  UserDefaultsManager.swift
//  SmatQDemo
//
//  Created by Aki Wang on 2019/4/2.
//  Copyright © 2019 smartq. All rights reserved.
//

import Foundation
import UIKit

class UserDefaultsManager: NSObject{
    private static let recordInstance = UserDefaultsManager()
    static private var instance : UserDefaultsManager { return recordInstance }
    @objc static func getInstance() -> UserDefaultsManager { return instance }
    
    //MARK: Init with dependency
    private override init() {
        //Use the default container for production environment
    }
    
    
    let ud = UserDefaults.standard
    
    //Proj Setting
    /** lang 語系*/
    let KEY_LANG = "lang"
    let KEY_API_ADDRESS = "KEY_API_ADDRESS"
    let KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN"
    let KEY_IS_LOGIN = "KEY_IS_LOGIN"
    let KEY_USER_ID = "KEY_USER_ID"
    let KEY_API_TOKEN = "KEY_API_TOKEN"
    
    let KEY_USER_ACCOUNT = "KEY_USER_ACCOUNT"
    let KEY_PASSWORD = "KEY_PASSWORD"
    let KEY_LOGIN_DATA = "KEY_LOGIN_DATA"
    let KEY_RADIO_ISON = "KEY_RADIO_ISON"
    
    let KEY_USER_NAME = "KEY_USER_PHONE"
    let KEY_USER_IMAGE = "KEY_USER_IMAGE"
    let KEY_CLOCK_PLAY = "KEY_CLOCK_PLAY"
    let KEY_CLOCK_STOP = "KEY_CLOCK_STOP"
    let KEY_PLAY_SWITCH_ISON = "KEY_PLAY_SWITCH_ISON"
    let KEY_STOP_SWITCH_ISON = "KEY_STOP_SWITCH_ISON"
    let KEY_PLAY_NOTIFICATION = "KEY_PLAY_NOTIFICATION"
    let KEY_STOP_NOTIFICATION = "KEY_STOP_NOTIFICATION"
    let KEY_LAST_CHANNEL = "KEY_LAST_CHANNEL"
        
    //show education
    let KEY_EDUCATION_INT = "KEY_EDUCATION_INT"
    let KEY_EDU_ID = "KEY_EDU_ID"
    
    func setLang(lang: String){
        ud.set(lang, forKey: KEY_LANG)
    }
    
    func getLang() -> String{
        return ud.value(forKey: KEY_LANG) as? String ?? ""
    }
    
    func setApiAddress(address: String){
        ud.set(address, forKey: KEY_API_ADDRESS)
    }
    
    func getApiAddress() -> String{
        let address = ud.value(forKey: KEY_API_ADDRESS) as? String
        if address != nil && address != "" {
            return address ?? ApiService.defApiAddress
        }
        return ApiService.defApiAddress
    }
    
    func setDeviceToken(apiToken: String){
        ud.set(apiToken, forKey: KEY_DEVICE_TOKEN)
    }
    
    func getDeviceToken() -> String{
        return ud.value(forKey: KEY_DEVICE_TOKEN) as? String ?? ""
    }
    
    func setUserAccount(username: String){
        ud.set(username, forKey: KEY_USER_ACCOUNT)
    }
    
    func getUserAccount() -> String {
        return ud.value(forKey: KEY_USER_ACCOUNT) as? String ?? ""
    }
    
    func setPassword(password: String){
        ud.set(password, forKey: KEY_PASSWORD)
    }
    
    func getPassword() -> String {
        return ud.value(forKey: KEY_PASSWORD) as? String ?? ""
    }
    
    func setLogin(isLogin: Bool){
        ud.set(isLogin, forKey: KEY_IS_LOGIN)
    }
    
    func isLogin() -> Bool {
        return ud.value(forKey: KEY_IS_LOGIN) as? Bool ?? false
    }
    
    func getUserId() -> Int{
        return getLoginData()?.user_id ?? 0
    }
    
    func setLoginData(data: LoginModel.Data?){
        ud.set(data?.toJsonString() ?? "", forKey: KEY_LOGIN_DATA)
    }
    
    func getLoginData() -> LoginModel.Data?{
        let data = ud.value(forKey: KEY_LOGIN_DATA) as? String ?? ""
        return data.toCodable()
    }
    
    func setApiToken(apiToken: String){
        ud.set(apiToken, forKey: KEY_API_TOKEN)
    }
    
    func getApiToken() -> String {
        return ud.value(forKey: KEY_API_TOKEN) as? String ?? ""
    }
    
    func setRadioIsOn(isOn: Bool) {
        ud.set(isOn, forKey: KEY_RADIO_ISON)
    }
    
    func getRadioIsOn() -> Bool {
        return ud.value(forKey: KEY_RADIO_ISON) as? Bool ?? true
    }
    
    func setUserName(name: String) {
        ud.set(name, forKey: KEY_USER_NAME)
    }
    
    func getUserName() -> String {
        return ud.value(forKey: KEY_USER_NAME) as? String ?? ""
    }
    
    func setUserImage(image: UIImage) {
        ud.setValue(image, forKey: KEY_USER_IMAGE)
    }
    
    func getUserImage() -> UIImage {
        return ud.value(forKey: KEY_USER_IMAGE) as! UIImage
    }
    
    func setClockStopTime(time: String) {
        ud.setValue(time, forKey: KEY_CLOCK_STOP)
    }
    
    func getClockStopTime() -> String {
        return ud.value(forKey: KEY_CLOCK_STOP) as? String ?? ""
    }
    
    func setPlaySwitchIsOn(isOn: Bool) {
        ud.setValue(isOn, forKey: KEY_PLAY_SWITCH_ISON)
    }
    
    func getPlaySwitchIsOn() -> Bool {
        return ud.value(forKey: KEY_PLAY_SWITCH_ISON) as? Bool ?? true
    }
    
    func setStopSwitchIsOn(isOn: Bool) {
        ud.setValue(isOn, forKey: KEY_STOP_SWITCH_ISON)
    }
    
    func getStopSwitchIsOn() -> Bool {
        return ud.value(forKey: KEY_STOP_SWITCH_ISON) as? Bool ?? true
    }
    
    func setPlayNotificationIsOn(isOn: Bool) {
        ud.setValue(isOn, forKey: KEY_PLAY_NOTIFICATION)
    }
    
    func getPlayNotificationIsOn() -> Bool {
        return ud.value(forKey: KEY_PLAY_NOTIFICATION) as? Bool ?? true
    }
    
    func setStopNotificationIsOn(isOn: Bool) {
        ud.setValue(isOn, forKey: KEY_STOP_NOTIFICATION)
    }
    
    func getStopNotificationIsOn() -> Bool {
        return ud.value(forKey: KEY_STOP_NOTIFICATION) as? Bool ?? true
    }
    
    func setEducationInt(int: Int) {
        ud.setValue(int, forKey: KEY_EDUCATION_INT)
    }
    
    func getEducationInt() -> Int {
        return ud.value(forKey: KEY_EDUCATION_INT) as? Int ?? 0
    }
    
    func setEduId(id: String) {
        ud.setValue(id, forKey: KEY_EDU_ID)
    }
    
    func getEduId() -> String {
        return ud.value(forKey: KEY_EDU_ID) as? String ?? ""
    }
    
    func setLastTimeChannel(channel: Int) {
        ud.set(channel, forKey: KEY_LAST_CHANNEL)
    }
    
    func getLastTimeChannel() -> Int {
        return ud.value(forKey: KEY_LAST_CHANNEL) as? Int ?? 1
    }
}
