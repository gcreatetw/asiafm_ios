//
//  UIApplicationExt.swift
//  asiafm
//
//  Created by Aki Wang on 2021/8/14.
//

import UIKit

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    class func topTabViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topTabViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            return tabController
        }
        if let presented = controller?.presentedViewController {
            return topTabViewController(controller: presented)
        }
        return nil
    }
}
