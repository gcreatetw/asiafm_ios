//
//  UIView+Ext.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/22.
//

import Foundation
import UIKit

extension UIView {
    func addAnchors(top: NSLayoutYAxisAnchor? = nil, paddingTop: CGFloat = 0,
                    bottom: NSLayoutYAxisAnchor? = nil, paddingBottom: CGFloat = 0,
                    leading: NSLayoutXAxisAnchor? = nil, paddingLeading: CGFloat = 0,
                    trailing: NSLayoutXAxisAnchor? = nil, paddingTrailing: CGFloat = 0,
                    height: CGFloat? = nil, width: CGFloat? = nil) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
        }
        if let left = leading {
            leadingAnchor.constraint(equalTo: left, constant: paddingLeading).isActive = true
        }
        if let right = trailing {
            trailingAnchor.constraint(equalTo: right, constant: paddingTrailing).isActive = true
        }
        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}
