//
//  Notification.swift
//  Treatrip
//
//  Created by Aki Wang on 2021/4/16.
//

import Foundation
import UIKit

extension Notification.Name {
    static var test = Notification.Name(rawValue: "test")
    static var logout = Notification.Name(rawValue: "logout")
    static var qrcode_scan = Notification.Name(rawValue: "qrcode_scan")
    static var add_shoe = Notification.Name(rawValue: "add_shoe")
    static var change_lang = Notification.Name(rawValue: "change_lang")
    static var keyboardWillShowNotification = UIResponder.keyboardWillShowNotification
    static var keyboardWillHideNotification = UIResponder.keyboardWillHideNotification
    
}
