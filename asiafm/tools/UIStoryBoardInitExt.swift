//
//  UIStoryBoardInitExt.swift
//  asiafm
//
//  Created by StevenTang on 2022/2/14.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    static let Login = UIStoryboard.init(name: "Login", bundle: nil)
    static let TabBar = UIStoryboard.init(name: "Tab", bundle: nil)
    static let Website = UIStoryboard.init(name: "Website", bundle: nil)
    static let Education = UIStoryboard.init(name: "Education", bundle: nil)
    
//MARK: - burgerView
    
    static let HeaderLine = UIStoryboard.init(name: "HeaderLine", bundle: nil)
    
    static let FlyHeaderLine = UIStoryboard.init(name: "FlyHeaderLine", bundle: nil)
    
    static let LifeReport = UIStoryboard.init(name: "LifeReport", bundle: nil)
    
    static let ActivityReport = UIStoryboard.init(name: "ActivityReport", bundle: nil)
    
    static let Coupon = UIStoryboard.init(name: "Coupon", bundle: nil)
    
    static let WinnerList = UIStoryboard.init(name: "WinnerList", bundle: nil)
    
//MARK: - PersonalSetting
    
    static let PersonalSetting = UIStoryboard.init(name: "PersonalSettingStoryboard", bundle: nil)
    
    static let SettingClock = UIStoryboard.init(name: "SettingClock", bundle: nil)
    
    static let OfficialFbClub = UIStoryboard.init(name: "OfficialFbClub", bundle: nil)
    
    static let AboutAsiaFamily = UIStoryboard.init(name: "AboutAsiaFamily", bundle: nil)
    
    static let OfficialWebsite = UIStoryboard.init(name: "AsiaFmOfficialWebsite", bundle: nil)
    
    static let IgPage = UIStoryboard.init(name: "IgPage", bundle: nil)
}
