//
//  CurrentDevice.swift
//  asiafm
//
//  Created by StevenTang on 2021/12/11.
//

import Foundation
import UIKit

extension UIDevice {
    public class func isPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    public class func isPhone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    public class func isCarPlay() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .carPlay
    }
}
