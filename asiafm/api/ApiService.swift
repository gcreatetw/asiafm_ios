//
//  ApiService.swift
//  ChargerApp
//
//  Created by Aki Wang on 2020/10/13.
// "https://demo.gcreate.com.tw/gc_asiafm/wp-json/api/"

import Foundation
import Alamofire

struct ApiService{
    static var defApiAddress = "https://www.asiafm.com.tw/wp-json/api/"
    static var asiaURL = "https://www.asiafm.com.tw/wp-json/api/"
    static var flyURL = "https://www.flyradio.com.tw/wp-json/api/"
    
    /** sample 同步 */
    static func Sample(email: String, password: String) -> ApiResponse<LoginModel>{
        let url = "\(asiaURL)auth/login"//URL
        let headers = [
            "Authorization": "Bearer \(UserDefaultsManager.getInstance().getApiToken())",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters = [
            "email" : "\(email)",
            "password" : "\(password)"
        ]
        let result: ApiResponse<LoginModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    /** sample 異步 */
    static func Sample(email: String, password: String, callBack: @escaping  (ApiResponse<LoginModel>) -> Void){
        let url = "\(asiaURL)auth/login"//URL
        let headers = [
            "Authorization": "Bearer \(UserDefaultsManager.getInstance().getApiToken())",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters = [
            "email" : "\(email)",
            "password" : "\(password)"
        ]
        //異步
        ApiTool.request(url: url, method: .post, parameters: parameters , headers: headers, callBack: callBack)
    }
    
    /** 登入 */
    static func login(username: String, password: String) -> ApiResponse<LoginModel>{
        let url = "https://www.asiafm.com.tw/wp-json/api/user/login"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["device_token"] = ud.getDeviceToken()
        parameters["username"] = username
        parameters["password"] = password
        let result: ApiResponse<LoginModel> = ApiTool.execute(url: url, method: .post, parameters: parameters, headers: headers)
        return result
    }
    
    /** 註冊 */
    static func register(username: String, password: String) -> ApiResponse<LoginModel>{
        let url = "https://www.asiafm.com.tw/wp-json/api/user/register"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["device_token"] = ud.getDeviceToken()
        parameters["username"] = username
        parameters["password"] = password
        let result: ApiResponse<LoginModel> = ApiTool.execute(url: url, method: .post, parameters: parameters , headers: headers)
        return result
    }
    
    static func getYoutubeSearch() -> ApiResponse<YoutubeSearchModel>{
        let url = "https://www.asiafm.com.tw/wp-json/api/youtube/channel"
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["part"] = "snippet"
        parameters["key"] = "AIzaSyCfeJuS9ZYmFT5lvhoI48WfUEk-JpJdezs"
        parameters["channelId"] = "UCPoQPt-ptT-X-Fl4nvtKuzg"
        parameters["order"] = "date"
        parameters["maxResults"] = 20 //65536
        let params = parameters.map { "\($0)=\($1)" }.joined(separator: "&")
        let result: ApiResponse<YoutubeSearchModel> = ApiTool.execute(url: "\(url)?\(params)", method: .get, headers: headers)
        print("VideoArea === api \(result)")
        return result
    }
    
    /// 最近十首歌
    static func getChannelInfo(_ channel: RadioChannel = RadioPlayer.shared.nowChannel) -> ApiResponse<ChannelInfoModel> {
        var url = "\(asiaURL)channel/info?stationID=\(channel.id)"
        if channel == .FLY {
            url = "\(flyURL)channel/info?stationID=\(channel.id)"
        }
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let result: ApiResponse<ChannelInfoModel> = ApiTool.execute(url: url, method: .get, headers: header)
        return result
    }
    
    enum PostType: String {
        /// 大頭條 927 亞洲
        case BIG_NEWS_ASIA = "asia927-bignews"
        /// 大頭條 923 亞太
        case BIG_NEWS_APAC = "asia923-bignews"
        /// 大頭條 895 飛揚
        case BIG_NEWS_FLY = "flyradio_news"
        /// 好康贈獎
        case GOOD_LUCK_GIFT = "goodluckgift"
        /// 娛樂新聞
        case ENTERTAINMENT_NEWS = "entertainment-news"
        /// 得獎名單
        case WINNERS_LIST = "winnerslist"
        /// 活動報馬仔
        case ACTIVITY_REPORT = "activityreport"
        /// 生活情報讚
        case LIFT_REPORT = "lifereport"
    }
    
    static func getPostInfo(_ postType: PostType, _ limit: Int = 20, _ channel: RadioChannel = RadioPlayer.shared.nowChannel) -> ApiResponse<PostInfoModel> {
        var url = ""
        if channel == .ASIA || channel == .APAC {
            url = "\(asiaURL)post/info?postType=\(postType.rawValue)&limit=\(limit)"
            if postType == .BIG_NEWS_ASIA || postType == .BIG_NEWS_APAC {
                url = "\(asiaURL)post/asia/info?postSlug=\(postType.rawValue)&limit=\(limit)"
            }
        } else if channel == .FLY {
            url = "\(flyURL)post/info?postType=\(postType.rawValue)&limit=\(limit)"
        }
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let result: ApiResponse<PostInfoModel> = ApiTool.execute(url: url, method: .get, headers: header)
        return result
    }
    
    enum WeeklyType: Int, CaseIterable {
        /// 週日
        case SUN = 1020
        /// 週一
        case MON = 1021
        /// 週二
        case TUES = 1022
        /// 週三
        case WED = 1023
        /// 週四
        case THUR = 1024
        /// 週五
        case FRI = 1025
        /// 週六
        case SAT = 1026
    }
    /// 節目表
    static func getRundown(_ weeklyType: WeeklyType, _ channel: RadioChannel = RadioPlayer.shared.nowChannel) -> ApiResponse<RundownModel> {
        var url = ""
        if channel == .ASIA || channel == .APAC {
            url = "\(asiaURL)station/program?"
        } else if channel == .FLY {
            url = "\(flyURL)station/program?"
        }
        switch channel {
        case .ASIA:
            url += "stationSlug=asia927"
        case .APAC:
            url += "stationSlug=asia923"
        case .FLY:
            url += "stationSlug=flyradio"
        default:
            url += "stationSlug=asia927"
        }
        url += "&weekdayId=\(weeklyType.rawValue)"
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let result: ApiResponse<RundownModel> = ApiTool.execute(url: url, method: .get, headers: header)
        return result
    }
    
    /// 尋歌專區
    static func getSearchMusicData(_ startDate: String, _ timePeriod: String, _ channel: RadioChannel = RadioPlayer.shared.nowChannel) -> ApiResponse<SearchMusicModel> {
        var url = ""
        if channel == .ASIA || channel == .APAC {
            url = "\(asiaURL)search/music?"
        } else if channel == .FLY {
            url = "\(flyURL)search/music?"
        }
        url += "stationID=\(channel.id)&startDate=\(startDate)&timePeriod=\(timePeriod)"
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let result: ApiResponse<SearchMusicModel> = ApiTool.execute(url: url, method: .get, headers: header)
        return result
    }
    
    //會員資料更新
    static func getUpdateNumberData(userId: String, gender: String, birthday: String, address: String, avatar: String, name: String, email: String) -> ApiResponse<UpdateNumberDataModel> {
        let url = "https://www.asiafm.com.tw/wp-json/api/user/update"
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["userId"] = userId
        parameters["gender"] = gender
        parameters["birthday"] = birthday
        parameters["address"] = address
        parameters["avatar"] = avatar
        parameters["name"] = name
        parameters["email"] = email
        
        let result: ApiResponse<UpdateNumberDataModel> = ApiTool.execute(url: url, method: .post, parameters: parameters, headers: header)
        return result
    }
    //更新密碼
    static func getUpdateUserPasswordData(userId: Int, oldPassword: String, newPassword: String) -> ApiResponse<UpdateNumberDataModel> {
        let url = "https://www.asiafm.com.tw/wp-json/api/user/updatePassword"
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["userId"] = userId
        parameters["oldPassword"] = oldPassword
        parameters["newPassword"] = newPassword
        let result: ApiResponse<UpdateNumberDataModel> = ApiTool.execute(url: url, method: .post, parameters: parameters, headers: header)
        return result
    }
    //登入（好像是重複的）
    static func getLoginData() -> ApiResponse<LoginDataModel> {
        let url = "https://www.asiafm.com.tw/wp-json/api/user/login"
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["username"] = ud.getUserAccount()
        parameters["password"] = ud.getPassword()
        
        let result: ApiResponse<LoginDataModel> = ApiTool.execute(url: url, method: .post, parameters: parameters, headers: header)
        
        return result
    }
//MARK: - 判斷帳號是否註冊
    static func checkRegister(username: String) -> ApiResponse<CheckRegisterModel> {
        let url = "https://www.asiafm.com.tw/wp-json/api/user/login"
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["event"] = "false"
        parameters["username"] = username
        parameters["password"] = "-1"
        
        let result: ApiResponse<CheckRegisterModel> = ApiTool.execute(url: url, method: .post, parameters: parameters, headers: header)
        
        return result
    }
//MARK: - 忘記密碼驗證後-更新密碼
    static func demoUserPasswordData(event: String ,username: String, oldPassword: String, newPassword: String) -> ApiResponse<NewPasswordDataModel> {
        let url = "https://www.asiafm.com.tw/wp-json/api/user/updatePassword"
        let header = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        var parameters = [String:Any]()
        parameters["event"] = event
        parameters["username"] = username
        parameters["oldPassword"] = oldPassword
        parameters["newPassword"] = newPassword
        let result: ApiResponse<NewPasswordDataModel> = ApiTool.execute(url: url, method: .post, parameters: parameters, headers: header)
        return result
    }
}
