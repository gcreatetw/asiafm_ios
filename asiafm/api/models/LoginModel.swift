//
//  LoginModel.swift
//  ChargerApp
//
//  Created by Aki Wang on 2020/10/13.
//

import Foundation

struct LoginModel : Codable {
    let code: Int?
    let message: String?
    let errors: String?
    let data: Data?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case errors = "errors"
        case data = "data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        errors = try values.decodeIfPresent(String.self, forKey: .errors)
        data = try values.decodeIfPresent(Data.self, forKey: .data)
    }
    
    struct Data : Codable {
        let user_id: Int?
        let name: String?
        let phone: String?
        let email: String?
        let gender: String?
        let birthday: String?
        let address: String?
        let avatar: String?
        
        enum CodingKeys: String, CodingKey {
            case user_id = "user_id"
            case name = "name"
            case phone = "phone"
            case email = "email"
            case gender = "gender"
            case birthday = "birthday"
            case address = "address"
            case avatar = "avatar"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            phone = try values.decodeIfPresent(String.self, forKey: .phone)
            email = try values.decodeIfPresent(String.self, forKey: .email)
            gender = try values.decodeIfPresent(String.self, forKey: .gender)
            birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
            address = try values.decodeIfPresent(String.self, forKey: .address)
            avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        }
    }
}
