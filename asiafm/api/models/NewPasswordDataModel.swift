//
//  NewPasswordDataModel.swift
//  asiafm
//
//  Created by StevenTang on 2022/2/17.
//

import Foundation

struct NewPasswordDataModel: Codable {
    let code: Int
    let message: String
    let errors: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case errors = "errors"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)!
        message = try values.decodeIfPresent(String.self, forKey: .message)!
        errors = try values.decodeIfPresent(String.self, forKey: .errors)
    }
}
