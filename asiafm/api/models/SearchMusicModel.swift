//
//  SearchMusicModel.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/24.
//

import Foundation

struct SearchMusicModel: Codable {
    let code: Int?
    let message: String?
    let list: [List]?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case list = "list"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        list = try values.decodeIfPresent([List].self, forKey: .list)
    }
    
    struct List: Codable {
        let title: String?
        let artist: String?
        
        enum CodingKeys: String, CodingKey {
            case title = "title"
            case artist = "artist"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = try values.decode(String.self, forKey: .title)
            artist = try values.decode(String.self, forKey: .artist)
        }
    }

}
