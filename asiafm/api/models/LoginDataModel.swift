//
//  LoginDataModel.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/15.
//

import Foundation

struct LoginDataModel: Codable {
    let code: Int?
    let message: String?
    let errors: String?
    let data: loginData?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case errors = "errors"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        errors = try values.decodeIfPresent(String.self, forKey: .errors)
        data = try values.decodeIfPresent(loginData.self, forKey: .data)
    }
    
    
    struct loginData: Codable {
        let userId: Int?
        let name: String?
        let phone: String?
        let email: String?
        let gender: String?
        let birthday: String?
        let address: String?
        let avatar: String?
        
        enum CodingKeys: String, CodingKey {
            case userId = "userId"
            case name = "name"
            case phone = "phone"
            case email = "email"
            case gender = "gender"
            case birthday = "birthday"
            case address = "address"
            case avatar = "avatar"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            userId = try values.decodeIfPresent(Int.self, forKey: .userId)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            phone = try values.decodeIfPresent(String.self, forKey: .phone)
            email = try values.decodeIfPresent(String.self, forKey: .email)
            gender = try values.decodeIfPresent(String.self, forKey: .gender)
            birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
            address = try values.decodeIfPresent(String.self, forKey: .address)
            avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        }
    }
    
}
