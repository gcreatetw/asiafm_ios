//
//  FlyradioChannelModel.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/23.
//

import Foundation

struct FlyradioChannelModel: Codable {
    let code: Int?
    let message: String?
    let info: [Info]?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case info = "info"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        info = try values.decodeIfPresent([Info].self, forKey: .info)
    }
    
    struct Info: Codable {
        let evenId: String?
        let eventTitle: String?
        let startTime: String?
        let endTime: String?
        let timeInterval: String?
        let weekdayId: String?
        let weekDay: String?
        let djId: String?
        let djName: String?
        let stationSlug: String?
        let imgUrl: String?
        let guid: String?
       
        
        enum CodingKeys: String, CodingKey {
            case evenId = "evenId"
            case eventTitle = "eventTitle"
            case startTime = "startTime"
            case endTime = "endTime"
            case timeInterval = "timeInterval"
            case weekdayId = "weekdayId"
            case weekDay = "weekDay"
            case djId = "djId"
            case djName = "djName"
            case stationSlug = "stationSlug"
            case imgUrl = "imgUrl"
            case guid = "guid"
            
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            evenId = try values.decodeIfPresent(String.self, forKey: .evenId)
            eventTitle = try values.decodeIfPresent(String.self, forKey: .eventTitle)
            startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
            endTime = try values.decodeIfPresent(String.self, forKey: .endTime)
            timeInterval = try values.decodeIfPresent(String.self, forKey: .timeInterval)
            weekdayId = try values.decodeIfPresent(String.self, forKey: .weekdayId)
            weekDay = try values.decodeIfPresent(String.self, forKey: .weekDay)
            djId = try values.decodeIfPresent(String.self, forKey: .djId)
            djName = try values.decodeIfPresent(String.self, forKey: .djName)
            stationSlug = try values.decodeIfPresent(String.self, forKey: .stationSlug)
            imgUrl = try values.decodeIfPresent(String.self, forKey: .imgUrl)
            guid = try values.decodeIfPresent(String.self, forKey: .guid)
        }
    }
}
