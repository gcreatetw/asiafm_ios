//
//  HeadLineModel.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/16.
//

import Foundation

struct CouponModel: Codable {
    let code: Int?
    let message: String?
    let info: [Info]?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case info = "info"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        info = try values.decodeIfPresent([Info].self, forKey: .info)
    }
    
    struct Info: Codable {
        let imgUrl: String?
        let guid: String?
        let postId: String?
        let postType: String?
        let postDate: String?
        let postTitle: String?
        
        enum CodingKeys: String, CodingKey {
            case imgUrl = "imgUrl"
            case guid = "guid"
            case postId = "postId"
            case postType = "postType"
            case postDate = "postDate"
            case postTitle = "postTitle"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            imgUrl = try values.decodeIfPresent(String.self, forKey: .imgUrl)
            guid = try values.decodeIfPresent(String.self, forKey: .guid)
            postId = try values.decodeIfPresent(String.self, forKey: .postId)
            postType = try values.decodeIfPresent(String.self, forKey: .postType)
            postDate = try values.decodeIfPresent(String.self, forKey: .postDate)
            postTitle = try values.decodeIfPresent(String.self, forKey: .postTitle)
        }
    }
    
}
