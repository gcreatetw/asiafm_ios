//
//  UpdateUserPasswordModel.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/15.
//

import Foundation

struct UpdateUserPasswordModel: Codable {
    let code: Int?
    let message: String?
    let errors: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case errors = "errors"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        errors = try values.decodeIfPresent(String.self, forKey: .errors)
    }
}
