//
//  FlyradioChannelInfoModel.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/27.
//

import Foundation

struct FlyradioChannelInfoModel: Codable {
    let code: Int?
    let message: String?
    let latestMusic: [LatestMusic]?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case latestMusic = "latestMusic"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        latestMusic = try values.decodeIfPresent([LatestMusic].self, forKey: .latestMusic)
    }
    
    struct LatestMusic: Codable {
        let rank: String?
        let picture_url: String?
        let song_name: String?
        let artist_name: String?
        
        enum CodingKeys: String, CodingKey {
            case rank = "rank"
            case picture_url = "picture_url"
            case song_name = "song_name"
            case artist_name = "artist_name"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            rank = try values.decodeIfPresent(String.self, forKey: .rank)
            picture_url = try values.decodeIfPresent(String.self, forKey: .picture_url)
            song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
            artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        }
    }
}
