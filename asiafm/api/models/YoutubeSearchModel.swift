//
//  YoutubeSearchModel.swift
//  asiafm
//
//  Created by Aki Wang on 2021/8/10.
//

import Foundation

struct YoutubeSearchModel : Codable {
    
    let kind : String?
    let etag : String?
    let nextPageToken : String?
    let regionCode : String?
    let pageInfo : PageInfo?
    let items : [Item]?
    
    enum CodingKeys: String, CodingKey {
        case kind = "kind"
        case etag = "etag"
        case nextPageToken = "nextPageToken"
        case regionCode = "regionCode"
        case pageInfo = "pageInfo"
        case items = "items"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        kind = try values.decodeIfPresent(String.self, forKey: .kind)
        etag = try values.decodeIfPresent(String.self, forKey: .etag)
        nextPageToken = try values.decodeIfPresent(String.self, forKey: .nextPageToken)
        regionCode = try values.decodeIfPresent(String.self, forKey: .regionCode)
        pageInfo = try values.decodeIfPresent(PageInfo.self, forKey: .pageInfo)
        items = try values.decodeIfPresent([Item].self, forKey: .items)
    }
    
    struct PageInfo : Codable {
        let totalResults : Int?
        let resultsPerPage : Int?
        
        enum CodingKeys: String, CodingKey {
            case totalResults = "totalResults"
            case resultsPerPage = "resultsPerPage"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults)
            resultsPerPage = try values.decodeIfPresent(Int.self, forKey: .resultsPerPage)
        }
    }
    
    struct Item : Codable {
        let kind : String?
        let etag : String?
        let id : Id?
        let snippet : Snippet?
        
        enum CodingKeys: String, CodingKey {
            case kind = "kind"
            case etag = "etag"
            case id = "id"
            case snippet = "snippet"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            kind = try values.decodeIfPresent(String.self, forKey: .kind)
            etag = try values.decodeIfPresent(String.self, forKey: .etag)
            id = try values.decodeIfPresent(Id.self, forKey: .id)
            snippet = try values.decodeIfPresent(Snippet.self, forKey: .snippet)
        }
    }
    
    struct Id : Codable {
        let kind : String?
        let videoId : String?
        
        enum CodingKeys: String, CodingKey {
            case kind = "kind"
            case videoId = "videoId"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            kind = try values.decodeIfPresent(String.self, forKey: .kind)
            videoId = try values.decodeIfPresent(String.self, forKey: .videoId)
        }
    }
    
    struct Snippet : Codable {
        let publishedAt : String?
        let channelId : String?
        let title : String?
        let description : String?
        let channelTitle : String?
        let liveBroadcastContent : String?
        let publishTime : String?
        let thumbnails : Thumbnails?
        
        enum CodingKeys: String, CodingKey {
            case publishedAt = "publishedAt"
            case channelId = "channelId"
            case title = "title"
            case description = "description"
            case channelTitle = "channelTitle"
            case liveBroadcastContent = "liveBroadcastContent"
            case publishTime = "publishTime"
            case thumbnails = "thumbnails"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            publishedAt = try values.decodeIfPresent(String.self, forKey: .publishedAt)
            channelId = try values.decodeIfPresent(String.self, forKey: .channelId)
            title = try values.decodeIfPresent(String.self, forKey: .title)
            description = try values.decodeIfPresent(String.self, forKey: .description)
            channelTitle = try values.decodeIfPresent(String.self, forKey: .channelTitle)
            liveBroadcastContent = try values.decodeIfPresent(String.self, forKey: .liveBroadcastContent)
            publishTime = try values.decodeIfPresent(String.self, forKey: .publishTime)
            thumbnails = try values.decodeIfPresent(Thumbnails.self, forKey: .thumbnails)
        }
    }
    
    struct Thumbnails : Codable {
        let defaultData : ThumbnailData?
        let medium : ThumbnailData?
        let high : ThumbnailData?
        
        enum CodingKeys: String, CodingKey {
            case defaultData = "defaultData"
            case medium = "medium"
            case high = "high"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            defaultData = try values.decodeIfPresent(ThumbnailData.self, forKey: .defaultData)
            medium = try values.decodeIfPresent(ThumbnailData.self, forKey: .medium)
            high = try values.decodeIfPresent(ThumbnailData.self, forKey: .high)
        }
        
        struct ThumbnailData : Codable {
            let url : String?
            let width : Int?
            let height : Int?
            
            enum CodingKeys: String, CodingKey {
                case url = "url"
                case width = "width"
                case height = "height"
            }
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                url = try values.decodeIfPresent(String.self, forKey: .url)
                width = try values.decodeIfPresent(Int.self, forKey: .width)
                height = try values.decodeIfPresent(Int.self, forKey: .height)
            }
        }
    }
}
