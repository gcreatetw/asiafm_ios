//
//  MyCarPlaySceneDelegate.swift
//  asiafm
//
//  Created by StevenTang on 2021/12/27.
//

import CarPlay
import MediaPlayer

class CarPlaySceneDelegate: UIResponder {
    var interfaceController: CPInterfaceController?
}

extension CarPlaySceneDelegate: CPTemplateApplicationSceneDelegate {
    func templateApplicationScene(_ templateApplicationScene: CPTemplateApplicationScene, didConnect interfaceController: CPInterfaceController) {
        self.interfaceController = interfaceController
        
        let asiaItem = CPListItem(text: "亞洲電台", detailText: "FM92.7",image: UIImage(named: "circle92.7"))
        let asiaFmItem = CPListItem(text: "亞太電台", detailText: "FM92.3",image: UIImage(named: "circle92.3"))
        let flyFmItem = CPListItem(text: "飛揚電台", detailText: "FM89.5",image: UIImage(named: "circle89.5"))
        let section = CPListSection(items: [asiaItem, asiaFmItem, flyFmItem])
        let listTemplate = CPListTemplate(title: "亞洲廣播家族", sections: [section])
        //Apple CarPlay Gulid 範例碼出錯
        //interfaceController.pushTemplate(listTemplate, animated: true)
        interfaceController.setRootTemplate(listTemplate, animated: true)

        if #available(iOS 14.0, *) {
            interfaceController.setRootTemplate(listTemplate, animated: true, completion: nil)
            print("Carplay iOS14")
        }
    }
    
    func templateApplicationScene(_ templateApplicationScene: CPTemplateApplicationScene, didDisconnectInterfaceController interfaceController: CPInterfaceController) {
        self.interfaceController = nil
    }
}
