//
//  WebSiteViewController.swift
//  Stock
//
//  Created by Aki Wang on 2021/5/30.
//

import UIKit
import WebKit

/*
 let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
 if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
     next.modalPresentationStyle = .fullScreen
     next.url = "https://google.com"
     next.titleString = "Google"
     self.present(next, animated: true)
 }
 **/
class WebSiteViewController: BaseViewController , WKUIDelegate{

    @IBOutlet weak var vMain: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    
    let webConfiguration = WKWebViewConfiguration()
    lazy var webView: WKWebView = {
        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: 0.0, height: vMain.frame.size.height))
//        WKWebView(frame: .zero, configuration: webConfiguration)
        return WKWebView(frame: customFrame, configuration: webConfiguration)
    }()
    
    var url: String = ""
    var doubleUrl: String = ""
    var titleString: String = "亞洲電臺"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initWebView()
        setTitleLabel(title: titleString)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbTitle.text = titleString
        loadUrl(url: url)
        doubleLoadUrl(url: doubleUrl)
    }
    
    @IBAction func onClickBack(_ send: UIButton) {
        dismiss(animated: true)
    }
    
    private func initWebView(){
        webView.translatesAutoresizingMaskIntoConstraints = false
        //關掉webView的view超出介面滑動跟縮放
        webView.scrollView.bounces = false
        webView.scrollView.bouncesZoom = false
        webView.scrollView.delegate = self
        vMain.addSubview(webView)
        webView.topAnchor.constraint(equalTo: vMain.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: vMain.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: vMain.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: vMain.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: vMain.heightAnchor).isActive = true
        webView.uiDelegate = self
    }
    /*
    let link = URL(string: url)!
    let request = URLRequest(url: link)
    webView.load(request)
    */
    
    //        var allowedQueryParamAndKey = NSCharacterSet.urlQueryAllowed
    //        allowedQueryParamAndKey.remove(charactersIn: ";/?:@&=+$,")
    
    func loadUrl(url: String){
        if let url = URL(string: url) {
            let request = URLRequest(url: url)
            //print("=====error Url \(url) ")
            webView.load(request)
        }
    }
    
    func doubleLoadUrl(url: String){
        if let url = URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) {
            let request = URLRequest(url: url)
            //print("=====error Url \(url) ")
            webView.load(request)
        }
    }
    
    func setTitleLabel(title: String) {
        titleString = title
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension WebSiteViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

//解html符號
//extension String {
//    var htmlToAttributedString: NSAttributedString? {
//        guard let data = data(using: .utf8) else { return nil }
//        //let data = data(using: .utf8) ?? nil
//        //print("UTF8 === \(data)")
//        do {
//            return try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch {
//            return nil
//        }
//    }
//    var htmlToString: String {
//        return htmlToAttributedString?.string ?? ""
//    }
//}
