//
//  SearchViewController.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/5.
//

import UIKit
import FRadioPlayer

class SearchViewController: BaseViewController {
    
    private var searchDataList: [SearchMusicModel.List] = []
    var dateDataSource: [String] = []
    var periodDataSource: [String] = ["00:00:00-02:00:00", "02:00:00-04:00:00", "04:00:00-06:00:00", "06:00:00-08:00:00", "08:00:00-10:00:00", "10:00:00-12:00:00", "12:00:00-14:00:00", "14:00:00-16:00:00", "16:00:00-18:00:00", "18:00:00-20:00:00", "20:00:00-22:00:00", "22:00:00-00:00:00"]
    var displayPeriodStringAry: [String] = ["00:00-02:00", "02:00-04:00", "04:00-06:00", "06:00-08:00", "08:00-10:00", "10:00-12:00", "12:00-14:00", "14:00-16:00", "16:00-18:00", "18:00-20:00", "20:00-22:00", "22:00-00:00"]
    
    var dateButton = UIButton()
    var periodButton = UIButton()
    let dateTransparentView = UIView()
    let periodTransparentView = UIView()
    let dateTableView = UITableView()
    let periodTableView = UITableView()
    
    var hourStringToInt: Int = 0
    var searchChannelId: Int = 1
    var hourString: String = ""
    var dateString: String = ""
    var apiTimeString: String = ""
    var displayPeriodString: String = ""
    var todayValue = Date()
    var sevenDayAgoAry: [String] = []
    var seletedDateStash: String = ""
    var seletedPeriodStash: String = ""
    
    @IBOutlet weak var dateArrowImageView: UIImageView!
    @IBOutlet weak var periodArrowImageView: UIImageView!
    @IBOutlet weak var songListTableView: UITableView!
    @IBOutlet weak var setTimeView: UIView!
    @IBOutlet weak var vTitle: UIView!
    
    @IBOutlet weak var selectDateButton: UIButton!
    @IBAction func selectDateAct(_ sender: Any) {
       // dateArrowImageView.image = UIImage(named: "arrowtriangle.up.fill")
        dateDataSource = sevenDayAgoAry
        dateButton = selectDateButton
        addDateTransparentView(frames: selectDateButton.frame)
    }
    
    @IBOutlet weak var selectPeriodButton: UIButton!
    @IBAction func selectPeriodAct(_ sender: Any) {
       // periodDataSource =
        periodButton = selectPeriodButton
        addPeriodTransparentView(frames: selectPeriodButton.frame)
    }
       
    override func viewDidLoad() {
        super.viewDidLoad()
        songListTableView.delegate = self
        songListTableView.dataSource = self
        initTitleView()
        setDateTableView()
        setPeriodTableView()
        setHourFormatter()
        setDateFormatter()
        timeToInt()
        
        let nib = UINib(nibName: "SearchMusicListHeaderView", bundle: nil)
        songListTableView.register(nib, forHeaderFooterViewReuseIdentifier: "SearchMusicListHeaderView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        settingView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        removeDateTransparentView()
        removePeriodTransparentView()
    }
    
    override func viewWillLayoutSubviews() {
        setDateButton(button: selectDateButton)
        setPeriodCorner(button: selectPeriodButton)
        syncApi()
        songListTableView.reloadData()
    }

    
    func setDateButton(button: UIButton) {
        button.cornerRadius = button.frame.height / 2
        button.setTitle(dateString, for: .normal)
    }
    
    func setPeriodCorner(button: UIButton) {
        button.cornerRadius = button.frame.height / 2
        button.setTitle(displayPeriodString, for: .normal)
    }
    
    func setDateTableView() {
        dateTableView.delegate = self
        dateTableView.dataSource = self
        dateTableView.isScrollEnabled = true
        dateTableView.register(SearchDateTableViewCell.self, forCellReuseIdentifier: "DATE_Cell")
        dateTableView.separatorStyle = .none
    }
    
    func setPeriodTableView() {
        periodTableView.delegate = self
        periodTableView.dataSource = self
        periodTableView.isScrollEnabled = true
        periodTableView.register(SearchPeriodTableViewCell.self, forCellReuseIdentifier: "PERIOD_Cell")
        periodTableView.separatorStyle = .none
    }
    
    func addDateTransparentView(frames: CGRect) {
        let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        dateTransparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(dateTransparentView)
        
        dateTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + self.selectDateButton.frame.height + 200, width: frames.width + 0, height: 0)
        self.view.addSubview(dateTableView)
        dateTableView.layer.cornerRadius = 10
        
        dateTransparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        dateTableView.reloadData()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(removeDateTransparentView))
        dateTransparentView.addGestureRecognizer(tapGesture)
        dateTransparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.dateTransparentView.alpha = 0.1
            self.dateTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + self.selectDateButton.frame.height + 200, width: frames.width + 0, height: CGFloat(280))
        }, completion: nil)
        
    }
    
    @objc func removeDateTransparentView() {
        let frames = dateButton.frame
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.dateTransparentView.alpha = 0
            self.dateTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y +  self.selectDateButton.frame.height + 200, width: frames.width + 0, height: 0)
        }, completion: nil)
    }
    
    func addPeriodTransparentView(frames: CGRect) {
        let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        periodTransparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(periodTransparentView)
        
        periodTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + self.selectPeriodButton.frame.height + 200, width: frames.width + 0, height: 0)
        self.view.addSubview(periodTableView)
        periodTableView.layer.cornerRadius = 10
        
        periodTransparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        periodTableView.reloadData()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(removePeriodTransparentView))
        periodTransparentView.addGestureRecognizer(tapGesture)
        periodTransparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.periodTransparentView.alpha = 0.1
            self.periodTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + self.selectPeriodButton.frame.height + 200, width: frames.width + 0, height: CGFloat(280))
        }, completion: nil)
    }
    
    @objc func removePeriodTransparentView() {
        let frames = periodButton.frame
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.periodTransparentView.alpha = 0
            self.periodTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y +  self.selectPeriodButton.frame.height + 200, width: frames.width + 0, height: 0)
        }, completion: nil)
    }
    
    var headerBackgroundView: UIImageView!
    var radioRemoteView: RadioRemoteView!
    private func initTitleView(){
        // header 漸層背景
        headerBackgroundView = UIImageView().apply {
            vTitle.addSubview($0)
            $0.clipsToBounds = true
            $0.snp.makeConstraints { maker in
                maker.width.equalToSuperview()
                maker.top.equalToSuperview()
                maker.bottom.equalToSuperview()
            }
        }
        
        let lbTitle = UILabel().apply {
            vTitle.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.centerX.equalToSuperview()
                maker.top.equalToSuperview().offset(topSafeArea + 15)
            }
            $0.text = "尋歌專區"
            $0.textColor = .txt_white
            $0.font = $0.font.withSize(20).bold
        }
        
        // radio remote view
        radioRemoteView = RadioRemoteView().apply {
            vTitle.addSubview($0)
            $0.isRealChangeChannel = false
            $0.delegate = self
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(lbTitle.snp.bottom).offset(10)
                maker.width.equalToSuperview()
                maker.height.equalTo(110)
            }
        }
    }
    
    private func settingView(){
        let channel = RadioPlayer.shared.nowChannel
        radioRemoteView.scrollToRadioChannl(channel: channel, animated: false)
        changeRadioChannel(channel)
    }

    let player = FRadioPlayer.shared
    var nowChannel: RadioChannel = .ASIA
    func changeRadioChannel(_ channel: RadioChannel) {
        if let background = channel.background {
            headerBackgroundView.image = background
        }
        
//        let channel = channel ?? nowChannel
//        if player.radioURL?.absoluteString != channel.streamURL {
//            nowChannel = channel
//            player.radioURL = URL(string: channel.streamURL)
//        }else {
//            //player.isAutoPlay = true
//        }
        syncApi()

    }
    
    //ApiService.getSearchMusicData(stationID: self.searchChannelId, startDate: self.dateString, timePeriod: self.apiTimeString).apply
    
    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            ApiService.getSearchMusicData(self.dateString, self.apiTimeString, self.radioRemoteView.nowChannel).apply {
                if $0.isSuccess {
                    self.searchDataList = $0.data?.list ?? []
                }
            }
            DispatchQueue.main.async {
                self.songListTableView.reloadData()
            }
        }
    }
    
    func setHourFormatter() {
        let hourFormatter = DateFormatter()
        hourFormatter.dateFormat = "HH:mm:ss"
        hourFormatter.locale = Locale(identifier: "zh_TW")
        
        hourString = hourFormatter.string(from: Date())
    }
    
    func setDateFormatter() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "zh_TW")
        var yesterday = TimeInterval()
        dateString = dateFormatter.string(from: todayValue)
        
        for i in 0...6 {
            yesterday = TimeInterval(-(24 * 60 * 60) * i)
            let lastSevenDay = todayValue.addingTimeInterval(yesterday)
            let setDateStiing = dateFormatter.string(from: lastSevenDay)
            sevenDayAgoAry.append(setDateStiing)
        }
    }
        
    func timeToInt() {
        hourStringToInt = (hourString as NSString).integerValue
        
        switch hourStringToInt {
        case 0..<2:
            apiTimeString = periodDataSource[0]
            displayPeriodString = displayPeriodStringAry[0]
        case 2..<4:
            apiTimeString = periodDataSource[1]
            displayPeriodString = displayPeriodStringAry[1]
        case 4..<6:
            apiTimeString = periodDataSource[2]
            displayPeriodString = displayPeriodStringAry[2]
        case 6..<8:
            apiTimeString = periodDataSource[3]
            displayPeriodString = displayPeriodStringAry[3]
        case 8..<10:
            apiTimeString = periodDataSource[4]
            displayPeriodString = displayPeriodStringAry[4]
        case 10..<12:
            apiTimeString = periodDataSource[5]
            displayPeriodString = displayPeriodStringAry[5]
        case 12..<14:
            apiTimeString = periodDataSource[6]
            displayPeriodString = displayPeriodStringAry[6]
        case 14..<16:
            apiTimeString = periodDataSource[7]
            displayPeriodString = displayPeriodStringAry[7]
        case 16..<18:
            apiTimeString = periodDataSource[8]
            displayPeriodString = displayPeriodStringAry[8]
        case 18..<20:
            apiTimeString = periodDataSource[9]
            displayPeriodString = displayPeriodStringAry[9]
        case 20..<22:
            apiTimeString = periodDataSource[10]
            displayPeriodString = displayPeriodStringAry[10]
        case 22..<24:
            apiTimeString = periodDataSource[11]
            displayPeriodString = displayPeriodStringAry[11]
            
        default:
            return
        }
    }
}

extension SearchViewController: RadioRemoteViewDelegate {
    func onChangeRadioChannel(channel: RadioChannel) {
        changeRadioChannel(channel)
    }
}

extension SearchViewController: RadioPlayerDelegate {
    func latestMusicListDidChange(_ latestMusicList: [ChannelInfoModel.LatestMusic], _ channel: RadioChannel) {}
    
    func playerChannelDidChange(_ playerState: FRadioPlayerState, _ channel: RadioChannel) {
        if playerState != .loading {
            return
        }
        radioRemoteView.scrollToRadioChannl(channel: channel, animated: false)
        changeRadioChannel(channel)
    }
    
    func playbackChannelDidChange(_ playbackState: FRadioPlaybackState, _ channel: RadioChannel) {}
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == songListTableView {
            return 30
        }
        
        let unCell = CGFloat(0)
        return unCell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == songListTableView {
            let header = self.songListTableView.dequeueReusableHeaderFooterView(withIdentifier: "SearchMusicListHeaderView") as! SearchMusicListHeaderView
            
            return header
        }

        let unView = UIView()
        return unView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == dateTableView {
            return dateDataSource.count
        }
        if tableView == periodTableView {
            return displayPeriodStringAry.count
        }
        return searchDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == dateTableView {
           guard let dateCell = tableView.dequeueReusableCell(withIdentifier: "DATE_Cell", for: indexPath) as? SearchDateTableViewCell else { return UITableViewCell()}
            dateCell.textLabel?.text = dateDataSource[indexPath.row]
            dateCell.textLabel?.textAlignment = .center
            dateCell.textLabel?.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            if seletedDateStash == dateDataSource[indexPath.row] {
                dateCell.textLabel?.textColor = .def_tab_selected
            }
            if dateString == dateDataSource[indexPath.row] {
                dateCell.textLabel?.textColor = .def_tab_selected
            }
            
            return dateCell
        }
        
        if tableView == periodTableView {
            guard let periodCell = tableView.dequeueReusableCell(withIdentifier: "PERIOD_Cell", for: indexPath) as? SearchPeriodTableViewCell else { return UITableViewCell()}
            periodCell.textLabel?.text = displayPeriodStringAry[indexPath.row]
            periodCell.textLabel?.textAlignment = .center
            periodCell.textLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            if seletedPeriodStash == displayPeriodStringAry[indexPath.row] {
                periodCell.textLabel?.textColor = .def_tab_selected
            }
            if displayPeriodString == displayPeriodStringAry[indexPath.row] {
                periodCell.textLabel?.textColor = .def_tab_selected
            }
            
            return periodCell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(SearchMusicTableViewCell.self)", for: indexPath) as! SearchMusicTableViewCell
    
        if indexPath.row < searchDataList.count {
            cell.setValue(searchDataList[indexPath.row])
        }
        
       // let unCell = UITableViewCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == dateTableView {
            return 60
        }else if tableView == periodTableView {
            return 50
        }
        else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == dateTableView {
            guard let dateCell = dateTableView.cellForRow(at: indexPath) as? SearchDateTableViewCell else {return}
            dateCell.textLabel?.text = dateDataSource[indexPath.row]
            dateCell.textLabel?.textAlignment = .center
            dateCell.textLabel?.textColor = .def_tab_selected
            seletedDateStash = dateDataSource[indexPath.row]
            dateString = dateDataSource[indexPath.row]
            
            removeDateTransparentView()
        }
        
        if tableView == periodTableView {
            guard let periodCell = periodTableView.cellForRow(at: indexPath) as? SearchPeriodTableViewCell else {return}
            periodCell.textLabel?.text = displayPeriodStringAry[indexPath.row]
            periodCell.textLabel?.textAlignment = .center
            periodCell.textLabel?.textColor = .def_tab_selected
            seletedPeriodStash = displayPeriodStringAry[indexPath.row]
            displayPeriodString = displayPeriodStringAry[indexPath.row]
            //api time
            apiTimeString = periodDataSource[indexPath.row]
            
            removePeriodTransparentView()
        }
    }
}
