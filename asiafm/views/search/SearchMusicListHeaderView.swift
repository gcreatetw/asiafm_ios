//
//  SearchMusicListHeaderView.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/21.
//

import UIKit

class SearchMusicListHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var songNameLabel: UILabel!
    
    @IBOutlet weak var performerNameLabel: UILabel!
}
