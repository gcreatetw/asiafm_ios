//
//  SearchMusicTableViewCell.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/21.
//

import UIKit

class SearchMusicTableViewCell: UITableViewCell {
    
    @IBOutlet weak var songLable: UILabel!
    @IBOutlet weak var performerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setValue(_ data: SearchMusicModel.List) {
        songLable.text = data.title
        performerLabel.text = data.artist
    }
    
    
    func setFlyValue(_ data: SearchFlyradioModel.List) {
        songLable.text = data.title
        performerLabel.text = data.artist
    }
}
