//
//  CouponViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/16.
//

import UIKit

class CouponViewController: BaseViewController {
    private var couponDataSource = [PostInfoModel.Info]()
    
    @IBOutlet weak var couponActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var couponTableView: UITableView!
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func personalSettingAct(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "PersonalSettingViewController")
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        self.present(vc, animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        couponTableView.delegate = self
        couponTableView.dataSource = self
        couponTableView.separatorStyle = .none
        setActivityIndicator()
        syncApi()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            
//            self.couponActivityIndicator.isHidden = true
//            self.couponTableView.isHidden = false
//        }
    }
    
    func setActivityIndicator() {
        couponActivityIndicator.center = view.center
        couponActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        couponActivityIndicator.startAnimating()
        couponActivityIndicator.isHidden = false
    }

    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            let response = ApiService.getPostInfo(.GOOD_LUCK_GIFT)
            if response.isSuccess {
                self.couponDataSource = response.data?.info ?? []
                
                DispatchQueue.main.async {
                    self.couponTableView.reloadData()
                    self.couponActivityIndicator.stopAnimating()
                    self.couponActivityIndicator.isHidden = true
                }
            }
        }
    }

}

extension CouponViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couponDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: CouponTableViewCell.self, for: indexPath)
        if indexPath.row < couponDataSource.count {
            cell.setValue(couponDataSource[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .right), dismissing: .push(direction: .left))
            next.doubleUrl = couponDataSource[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "好康贈獎")
            self.present(next, animated: true, completion: nil)
        }
    }
}
