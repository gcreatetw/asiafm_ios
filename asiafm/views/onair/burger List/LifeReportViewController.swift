//
//  LifeReportViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/16.
//

import UIKit

class LifeReportViewController: BaseViewController {
    private var lifeReportDataSource = [PostInfoModel.Info]()
    
    @IBOutlet weak var lifeReportActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lifeReportTableView: UITableView!
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func personalSettingAct(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "PersonalSettingViewController")
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        self.present(vc, animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lifeReportTableView.delegate = self
        lifeReportTableView.dataSource = self
        lifeReportTableView.separatorStyle = .none
        
        setActivityIndicator()
        self.syncApi()
    }
    
    func setActivityIndicator() {
        lifeReportActivityIndicator.center = view.center
        lifeReportActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        lifeReportActivityIndicator.startAnimating()
        lifeReportActivityIndicator.isHidden = false
    }
    
    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            let response = ApiService.getPostInfo(.LIFT_REPORT)
            if response.isSuccess {
                self.lifeReportDataSource = response.data?.info ?? []
                
                DispatchQueue.main.async {
                    self.lifeReportTableView.reloadData()
                    self.lifeReportActivityIndicator.stopAnimating()
                    self.lifeReportActivityIndicator.isHidden = true
                }
            }
        }
    }
    
}

extension LifeReportViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lifeReportDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: LifeReportTableViewCell.self, for: indexPath)
        if indexPath.row < lifeReportDataSource.count {
            cell.setValue(lifeReportDataSource[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .right), dismissing: .push(direction: .left))
            next.url = lifeReportDataSource[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "生活情報讚")
            
            self.present(next, animated: true, completion: nil)
        }
    }
}
