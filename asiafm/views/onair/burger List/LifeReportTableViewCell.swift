//
//  LifeReportTableViewCell.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/16.
//

import UIKit

class LifeReportTableViewCell: UITableViewCell {

    @IBOutlet weak var titleImageView: UIImageView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setValue(_ data: PostInfoModel.Info) {
        if data.imgUrl == "" {
            titleImageView.image = UIImage(named: "hamber_Default")
        }else {
            titleImageView.kf.setImage(with: URL(string: data.imgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        }
        
        dateLabel.text = data.postDate?.toDate("yyyy-MM-dd HH:mm:ss")?.toString("yyyy-MM-dd") ?? ""
        titleLabel.text = data.postTitle
    }

}
