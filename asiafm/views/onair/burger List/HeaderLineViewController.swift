//
//  HeaderLineViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/16.
//

import UIKit

class HeaderLineViewController: BaseViewController {
    var articleCount: Int = 0
    var headerLineBool: Bool = false
    private var headerLineDataSource = [PostInfoModel.Info]()
    
    @IBOutlet weak var headerLineActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var headerLineTableView: UITableView!
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func personalSettingAct(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "PersonalSettingViewController")
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        self.present(vc, animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerLineTableView.delegate = self
        headerLineTableView.dataSource = self
        headerLineTableView.separatorStyle = .none
       // headerLineTableView.isHidden = true
        syncApi()
        setActivityIndicator()

    }

    func setActivityIndicator() {
        headerLineActivityIndicator.center = view.center
        headerLineActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        headerLineActivityIndicator.startAnimating()
        headerLineActivityIndicator.isHidden = false
    }
    
    private func syncApi() {
       // print("HeaderLine syncApi")
        DispatchQueue.init(label: "Api").async {
            let response = ApiService.getPostInfo(.BIG_NEWS_ASIA, 20, .ASIA)
            
            if response.isSuccess {
                self.headerLineDataSource = response.data?.info ?? []
                DispatchQueue.main.async {
                    self.headerLineTableView.reloadData()
                    self.headerLineActivityIndicator.stopAnimating()
                    self.headerLineActivityIndicator.isHidden = true
                }
            }
        }
    }

}

extension HeaderLineViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headerLineDataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: HeaderLineTableViewCell.self, for: indexPath)
        if indexPath.row < headerLineDataSource.count {
            cell.setValue(headerLineDataSource[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .right), dismissing: .push(direction: .left))
            next.url = headerLineDataSource[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "ASIA大頭條")
          
            self.present(next, animated: true, completion: nil)
        }
    }



}
