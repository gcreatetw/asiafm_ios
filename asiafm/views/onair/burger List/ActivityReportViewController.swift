//
//  ActivityReportViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/16.
//

import UIKit

class ActivityReportViewController: BaseViewController {
    private var activityReportDataSource = [PostInfoModel.Info]()
    
    @IBOutlet weak var acReportActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityReportTableView: UITableView!
    
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func personalSettingAct(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "PersonalSettingViewController")
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        self.present(vc, animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityReportTableView.delegate = self
        activityReportTableView.dataSource = self
        activityReportTableView.separatorStyle = .none
        
        setActivityIndicator()
        self.syncApi()
    }

    func setActivityIndicator() {
        acReportActivityIndicator.center = view.center
        acReportActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        acReportActivityIndicator.startAnimating()
        acReportActivityIndicator.isHidden = false
    }
    
    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            let response = ApiService.getPostInfo(.ACTIVITY_REPORT, 20)
            if response.isSuccess {
                self.activityReportDataSource = response.data?.info ?? []
                
                DispatchQueue.main.async {
                    self.activityReportTableView.reloadData()
                    self.acReportActivityIndicator.stopAnimating()
                    self.acReportActivityIndicator.isHidden = true
                }
            }
        }
    }
}


extension ActivityReportViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityReportDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ActivityReportTableViewCell.self, for: indexPath)
        
        if indexPath.row < activityReportDataSource.count {
            cell.setValue(activityReportDataSource[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .right), dismissing: .push(direction: .left))
            next.doubleUrl = activityReportDataSource[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "活動報馬仔")
            
            self.present(next, animated: true, completion: nil)
        }
    }
}
