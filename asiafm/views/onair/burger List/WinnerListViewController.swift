//
//  WinnerListViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/17.
//

import UIKit

class WinnerListViewController: BaseViewController {
    private var winnerListDataSource = [PostInfoModel.Info]()
    
    @IBOutlet weak var winnerActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var winnerListTableView: UITableView!
    
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func personalSettingAct(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "PersonalSettingViewController")
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        self.present(vc, animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        winnerListTableView.delegate = self
        winnerListTableView.dataSource = self
        winnerListTableView.separatorStyle = .none
        
        setActivityIndicator()
        self.syncApi()
    }
    
    func setActivityIndicator() {
        winnerActivityIndicator.center = view.center
        winnerActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        winnerActivityIndicator.startAnimating()
        winnerActivityIndicator.isHidden = false
    }
    
    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            let response = ApiService.getPostInfo(.WINNERS_LIST)
            if response.isSuccess {
                self.winnerListDataSource = response.data?.info ?? []
                
                DispatchQueue.main.async {
                    self.winnerListTableView.reloadData()
                    self.winnerActivityIndicator.stopAnimating()
                    self.winnerActivityIndicator.isHidden = true
                }
            }
        }
    }
}

extension WinnerListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return winnerListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: WinnerListTableViewCell.self, for: indexPath)
        
        if indexPath.row < winnerListDataSource.count {
            cell.setValue(winnerListDataSource[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .right), dismissing: .push(direction: .left))
            next.doubleUrl = winnerListDataSource[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "得獎名單")
 
            self.present(next, animated: true, completion: nil)
        }
    }
    
}
