//
//  FlyHeaderLineViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/9.
//

import UIKit

class FlyHeaderLineViewController: BaseViewController {
    private var flyHeaderLineDataSource = [PostInfoModel.Info]()
    
    @IBOutlet weak var flyHeaderActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var FlyHeaderLineTableView: UITableView!
    
    @IBAction func personalSettingAct(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "PersonalSettingViewController")
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        self.present(vc, animated: true, completion: .none)
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FlyHeaderLineTableView.delegate = self
        FlyHeaderLineTableView.dataSource = self
        FlyHeaderLineTableView.separatorStyle = .none
        
        setActivityIndicator()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.syncApi()
            self.flyHeaderActivityIndicator.isHidden = true
            self.FlyHeaderLineTableView.isHidden = false
        }
    }
    
    func setActivityIndicator() {
        flyHeaderActivityIndicator.center = view.center
        flyHeaderActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
        flyHeaderActivityIndicator.startAnimating()
        flyHeaderActivityIndicator.isHidden = false
    }
    
    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            let response = ApiService.getPostInfo(.BIG_NEWS_FLY, 20, .FLY)
            if response.isSuccess {
                self.flyHeaderLineDataSource = response.data?.info ?? []

                DispatchQueue.main.async {
                    self.FlyHeaderLineTableView.reloadData()
                }
            }
        }
    }

}

extension FlyHeaderLineViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flyHeaderLineDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: FlyHeaderLineTableViewCell.self, for: indexPath)
        if indexPath.row < flyHeaderLineDataSource.count {
            cell.setValue(flyHeaderLineDataSource[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .right), dismissing: .push(direction: .left))
            next.url = flyHeaderLineDataSource[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "飛揚大頭條")
           // print("=======\(flyHeaderLineDataSource[indexPath.row].guid ?? "")")
          
            self.present(next, animated: true, completion: nil)
        }
    }
    
}
