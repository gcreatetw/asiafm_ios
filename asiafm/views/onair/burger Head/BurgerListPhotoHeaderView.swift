//
//  BurgerListPhotoHeaderView.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/15.
//

import UIKit

class BurgerListPhotoHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var burgerPhoto: UIImageView!
    
}
