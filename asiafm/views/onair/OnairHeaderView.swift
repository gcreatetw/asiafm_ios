//
//  OnairHeaderView.swift
//  asiafm
//
//  Created by Aki Wang on 2021/8/18.
//

import UIKit
import SnapKit

class OnairHeaderView: UIView {
    
    var radioRemoteView: RadioRemoteView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    var headerView: UIView!
    var headerBackgroundView: UIImageView!
    var headerPlayView: UIView!
    var headerHeightConstraint: Constraint!
    var headerPlayButton = UIButton()
    var onAirImageView: UIImageView!
    var ivOnAir: UIImageView!
    var lbSongTitle: UILabel!
    var lbAuthor: UILabel!
    var vAlpha: UIView!
    var ivAlpha: UIImageView!
    var imageSongCover: UIImageView!
    var gradientView: UIView!
    var gradientLayer = CAGradientLayer()
    private func initView(){
        // header
        self.apply {
            $0.backgroundColor = .transparent
        }
//        headerView = UIView().apply {
//            self.addSubview($0)
//            $0.snp.makeConstraints { maker in
//                maker.top.equalToSuperview()
//                maker.width.equalToSuperview()
//                self.headerHeightConstraint = maker.height.equalTo(400).constraint
//                maker.centerX.equalToSuperview()
//            }
//            $0.clipsToBounds = true
//            $0.backgroundColor = .red
//        }
        
        // header 漸層背景
        headerBackgroundView = UIImageView().apply {
            self.addSubview($0)
            $0.clipsToBounds = true
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                maker.width.equalToSuperview()
                maker.height.equalTo(300)
//                maker.bottom.lessThanOrEqualToSuperview()
            }
        }
        
        // radio remote view
        radioRemoteView = RadioRemoteView().apply {
            self.addSubview($0)
            $0.snp.makeConstraints { maker in

                maker.top.equalToSuperview().offset(30)
                //maker.top.equalTo(30)
                maker.width.equalToSuperview()
                maker.height.equalTo(100)
            }
        }
        
        // 專輯圖 播放暫停 歌名 表演者
        headerPlayView = UIView().apply {
            self.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(radioRemoteView.snp.bottom).offset(-10)
                maker.bottom.equalToSuperview().offset(0)
                maker.width.equalTo(300)
                maker.height.equalTo(300)
                maker.centerX.equalToSuperview()
            }
//            $0.backgroundColor = .lightGray
        }
        
        // 專輯圖
         imageSongCover = UIImageView().apply {
            headerPlayView.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(8)
                //maker.bottom.equalToSuperview()
                maker.centerX.equalToSuperview()
                maker.width.equalToSuperview()
                maker.height.equalToSuperview().offset(-12)
            }
            $0.cornerRadius = 6
            //$0.image = UIImage(named: "正在播放_927")
            $0.contentMode = .scaleAspectFill
            
            
        }
        
        //專輯漸層陰影
        gradientView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 330)).apply {
            headerPlayView.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                maker.bottom.equalToSuperview()
            }
        }
        gradientLayer.frame = gradientView.bounds
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.clear.cgColor, UIColor.darkGray.cgColor]
        gradientLayer.locations = [0, 0.5, 1.2]
        imageSongCover.layer.addSublayer(gradientLayer)
        
        
        // 播放暫停圖
//        let headerPlayImageView = UIImageView().apply {
//            headerPlayView.addSubview($0)
//            $0.snp.makeConstraints { maker in
//                maker.top.equalTo((300 - 80) / 2) //原本為-80
//                maker.left.equalTo((300 - 100) / 2)
//                maker.width.equalTo(100)
//                maker.height.equalTo(100)
//            }
//            $0.image = UIImage(named: "icon_PLAY_shadow")
//            $0.clipsToBounds = true
//        }
        
        headerPlayButton = UIButton().apply {
            headerPlayView.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo((300 - 80) / 2) //原本為-80
                maker.left.equalTo((300 - 100) / 2)
                maker.width.equalTo(100)
                maker.height.equalTo(100)
            }
            //$0.setImage(UIImage(named: "icon_PLAY_shadow"), for: .normal)
            $0.isEnabled = true
            $0.clipsToBounds = true
        }
              
        // ONAIR
        ivOnAir = UIImageView().apply {
            headerPlayView.addSubview($0)
            $0.snp.makeConstraints { maker in
                //maker.top.equalTo(headerPlayImageView.snp.bottom).offset(20)
                maker.top.equalTo(imageSongCover.snp.bottom).offset(-100)
                //maker.left.equalTo(15)
                maker.left.equalTo(imageSongCover.snp.left).offset(13)
            }
            $0.clipsToBounds = true
            $0.backgroundColor = .red
          //  $0.image = UIImage(named: "ON AIR (off)")
        }
        
        onAirImageView = UIImageView().apply {
            headerPlayView.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(imageSongCover.snp.bottom).offset(-100)
                maker.left.equalTo(imageSongCover.snp.left).offset(13)
            }
            $0.clipsToBounds = true
            $0.contentMode = .scaleAspectFill
        }
        
        // 歌曲名稱
         lbSongTitle = UILabel().apply {
            headerPlayView.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(ivOnAir.snp.bottom).offset(8)
                maker.left.equalTo(12)
                maker.right.equalTo(headerPlayView.snp.right).offset(-5)
            }
            $0.clipsToBounds = true
            $0.text = "歌曲名稱"
            $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            $0.font = $0.font.withSize(14).bold
        }
        
        // 表演者
        lbAuthor = UILabel().apply {
            headerPlayView.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(lbSongTitle.snp.bottom).offset(2)
                maker.left.equalTo(12)
                maker.right.equalTo(headerPlayView.snp.right).offset(-5)
            }
            $0.clipsToBounds = true
            $0.text = "表演者"
            $0.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            $0.font = $0.font.withSize(14).bold
        }
        //
//        self.bringSubviewToFront(radioRemoteView)
        
        vAlpha = UIView().apply {
            self.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.right.equalToSuperview()
                maker.left.equalToSuperview()
            }
            $0.backgroundColor = RadioPlayer.shared.nowChannel.backgroundColor
            $0.alpha = 0
        }
        
        let bgAlpha = UIView().apply {
            vAlpha.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(topSafeArea)
                maker.centerX.equalToSuperview()
                maker.width.equalTo(60)
                maker.height.equalTo(60)
            }
            $0.backgroundColor = .clear
            $0.layer.masksToBounds = false
            $0.layer.cornerRadius = 30
            $0.clipsToBounds = true
        }
        
        //下拉icon
        ivAlpha = UIImageView().apply {
            vAlpha.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(topSafeArea + 0)
                maker.centerX.equalToSuperview()
                maker.width.equalTo(60)
                maker.height.equalTo(60)
            }
            $0.image = UIImage(named: RadioPlayer.shared.nowChannel.image)
        }
        
        UIView().apply {
            // 讓 bgAlpha 觸控 點擊比較大
            vAlpha.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(bgAlpha.snp.top)
                maker.bottom.equalTo(bgAlpha.snp.bottom)
                maker.right.equalTo(bgAlpha.snp.right)
                maker.left.equalTo(bgAlpha.snp.left)
            }
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapAlpha(_ :)))
            $0.isUserInteractionEnabled = true
            $0.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    var onClickAlpha: (() -> ())?
    @objc func tapAlpha(_ sender : UITapGestureRecognizer) {
        onClickAlpha?()
    }
}
