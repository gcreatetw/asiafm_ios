//
//  OnairViewController.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/5.
//

import UIKit
import Hero
import FRadioPlayer
import SnapKit

class OnairViewController: BaseViewController {
    
    @IBOutlet weak var buttonSetting: UIButton!
    @IBOutlet weak var buttonBurger: UIButton!
    var onairRadioBool: Bool = ud.getRadioIsOn()
    var articleLimit: Int = 0
    var showEducationInt: Int = 0
        
    enum TableType: Int, CaseIterable {
        /// 最近播放
        case History
        case TopNews
        case AD1
        case Life
        case Event
        case Discount
        case AD2
        case PowerOfAsiafm
    }
    
    var imageView: UIImageView!
    var radioRemoteView: RadioRemoteView!
    private var albumAsiaDataList = [ChannelInfoModel.LatestMusic]()
    private var albumFlyDatalist = [FlyradioChannelInfoModel.LatestMusic]()
    
    // 大頭條
    private var AsiaHeaderLineDataList: [PostInfoModel.Info] = []
    // 生活情報讚
    private var onairLifeReportDataList: [PostInfoModel.Info] = []
    // 活動報馬仔
    private var activityReportDataList: [PostInfoModel.Info] = []
    // 好康贈獎
    private var couponDataList: [PostInfoModel.Info] = []
    
    
    @objc func headerPlayButtonAction() {
        if onairRadioBool == true {
            RadioPlayer.shared.play()
            headerView.headerPlayButton.setImage(UIImage(named: ""), for: .normal)
            onairRadioBool = false
        }else {
            RadioPlayer.shared.stop()
            headerView.headerPlayButton.setImage(UIImage(named: "icon_PLAY_shadow"), for: .normal)
            onairRadioBool = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        setBurgerTableView()
        setSpaceHeaderView()
       
        headerView.headerPlayButton.addTarget(self, action: #selector(headerPlayButtonAction), for: .touchUpInside)
        
        //與ud.getRadioIsOn() 連動
        if onairRadioBool == true {
            headerView.headerPlayButton.setImage(UIImage(named: ""), for: .normal)
            headerView.ivOnAir.image = UIImage(named: "LIVE (on)")
            onairRadioBool = false
        }else {
            headerView.headerPlayButton.setImage(UIImage(named: "icon_PLAY_shadow"), for: .normal)
            headerView.ivOnAir.image = UIImage(named: "ON AIR (off)")
            onairRadioBool = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showEduView()
        settingView()
        changeLatestMusicList()
        articleLimit = 5
    }
       
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        removeBurgerTransparentView()
        articleLimit = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if onairTableView.contentOffset.y == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.onairTableView.bounces = false
            }
        }else {
            self.onairTableView.bounces = true
        }
    }
    
    private func showEduView(){
        if ud.getEducationInt() == 0 {
            showEducationInt += 1
            ud.setEducationInt(int: showEducationInt)
            
            if let next = UIStoryboard.Education.instantiateViewController(withClass: EducationViewController.self) {
                next.modalPresentationStyle = .fullScreen
                self.present(next, animated: true)
            }
        }
    }
    
    private func initView(){
        initHeaderView()
        initTableView()
    }
    
    var headerView: OnairHeaderView!
    var headerHeightConstraint: LayoutConstraint!
    lazy var headerMaxHeight = 400.cgFloat + topSafeArea
    lazy var headerMinHeight = 64.cgFloat + topSafeArea
    var beginScrollOffsetY = 0.cgFloat
    private func initHeaderView(){
        // header
        headerView = OnairHeaderView(frame: .zero).apply {
            view.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                maker.width.equalToSuperview()
                self.headerHeightConstraint = maker.height.equalTo(headerMaxHeight).constraint.layoutConstraints[0]
                maker.centerX.equalToSuperview()
            }
            $0.clipsToBounds = true
            $0.radioRemoteView.delegate = self
            $0.onClickAlpha = {
                self.zoomOutHeader()
                self.onairTableView.scrollToTop(animated: false)
            }
        }
        view.bringSubviewToFront(buttonSetting)
        view.bringSubviewToFront(buttonBurger)
    }
    
    var onairTableView: UITableView!
    private func initTableView(){
        onairTableView = UITableView(frame: .zero, style: .grouped).apply {
            view.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(headerView.snp.bottom)
                maker.left.equalToSuperview()
                maker.right.equalToSuperview()
                maker.bottom.equalToSuperview()
            }
            $0.backgroundColor = .transparent
            
            $0.separatorInset = .zero
            $0.separatorStyle = .none
            
            $0.sectionFooterHeight = .zero
            $0.estimatedSectionFooterHeight = .zero

            $0.sectionHeaderHeight = UITableView.automaticDimension
            $0.estimatedSectionHeaderHeight = UITableView.automaticDimension
            
            $0.rowHeight = UITableView.automaticDimension
            $0.estimatedRowHeight = UITableView.automaticDimension
            //
            $0.register(headerFooterViewClassWith: OnairLatestMusicHeaderView.self)
            //
            $0.register(cellWithClass: OnairTableCell.self)
            $0.register(cellWithClass: OnairLatestMusicTableCell.self)
            $0.register(cellWithClass: OnairTopNewsTableCell.self)
            $0.register(cellWithClass: OnairLifeTableCell.self)
            $0.register(cellWithClass: OnairEventTableCell.self)
            $0.register(cellWithClass: OnairDiscountTableCell.self)
            $0.register(cellWithClass: OnairAdTableCell.self)
            //
            $0.dataSource = self
            $0.delegate = self
            $0.reloadData()
        }
    }
    
    private func settingView(){
        let channel = RadioPlayer.shared.nowChannel
        headerView.radioRemoteView.scrollToRadioChannl(channel: channel, animated: false)
        changeRadioChannel(channel)
    }
    
    private func changeLatestMusicList(_ latestMusicList: [ChannelInfoModel.LatestMusic] = RadioPlayer.shared.latestMusicList, _ channel: RadioChannel = RadioPlayer.shared.nowChannel) {
        guard let latestMusic = latestMusicList.first else {
            return
        }
        if latestMusicList.first?.picture_url != nil {
            headerView.imageSongCover.kf.setImage(with: URL(string: latestMusic.picture_url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        }
        
        self.headerView.lbSongTitle.text =  latestMusic.song_name
        self.headerView.lbAuthor.text = latestMusic.artist_name
        self.onairTableView.reloadData()
    }
    
    func changeRadioChannel(_ channel: RadioChannel) {
        let latestMusicList: [ChannelInfoModel.LatestMusic] = RadioPlayer.shared.latestMusicList
        //log(TAG, "changeRadioChannel \(channel)")
        if let background = channel.background {
            headerView.headerBackgroundView.image = background
        }
      
        //三台頻道的預設圖轉換
        if latestMusicList.first?.picture_url == nil {
            if let albumImage = channel.albumImage {
                switch channel {
                case .ASIA:
                    headerView.imageSongCover.image = albumImage
                case .APAC:
                    headerView.imageSongCover.image = albumImage
                case .FLY:
                    headerView.imageSongCover.image = albumImage
                default:
                    headerView.imageSongCover.image = UIImage(named: "image_cover_recently_played")
                }
            }
        }
        syncApi(channel)
    }
    
    @IBAction func personalSettingAct(_ sender: Any) {
        if allowToSetting == true {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "PersonalSettingViewController")
            vc.modalPresentationStyle = .fullScreen
            vc.hero.isEnabled = true
            vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
            self.present(vc, animated: true, completion: .none)
        }else {
            let alertVc = GuestLoginViewController.loadFromNib()
            alertVc.guestAlertPop = {
                self.dismiss(animated: true, completion: .none)
            }
            alertVc.contentText = "請登入會員\n使用個人設定功能"
            self.present(alertVc, animated: true, completion: nil)
        }
    }
    
//MARK: - 漢堡選單
    var burgerDataSource: [String] = ["ASIA大頭條", "飛揚大頭條", "生活情報讚", "活動報馬仔", "好康贈獎", "得獎名單"]
    var burgerButton = UIButton()
    let burgerTableView = UITableView()
    let burgerTransparentView = UIView()
    
    @IBOutlet weak var burgerListButton: UIButton!
    @IBAction func popBurgerList(_ sender: Any) {
        burgerButton = burgerListButton
        addBurgerTransparentView(frames: burgerListButton.frame)
    }
    
    func setBurgerTableView() {
        burgerTableView.delegate = self
        burgerTableView.dataSource = self
        burgerTableView.isScrollEnabled = false
        //burgerTableView.allowsSelection
        burgerTableView.register(BurgerListTableViewCell.self, forCellReuseIdentifier: "BURGER_Cells")
        burgerTableView.separatorStyle = .none
    }
    
    func addBurgerTransparentView(frames: CGRect) {
        let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        
        burgerTransparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(burgerTransparentView)
        
        //iPad & iPhone
        if UIDevice.isPad() {
            burgerTableView.frame = CGRect(x: self.view.bounds.minX - 10, y: self.view.bounds.minY + 130, width: 0, height: self.view.frame.height / 1.1)
            burgerTableView.layer.cornerRadius = 20
            burgerTableView.alpha = 0.9
            
            burgerTransparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
            burgerTableView.reloadData()
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(removeBurgerTransparentView))
            burgerTransparentView.addGestureRecognizer(tapGesture)
            burgerTransparentView.alpha = 0
            UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .overrideInheritedCurve, animations: {
                self.burgerTransparentView.alpha = 0.1
                self.burgerTableView.frame = CGRect(x: self.view.bounds.minX - 10, y: self.view.bounds.minY + 130, width: self.view.frame.width / 3 + 10, height: self.view.frame.height / 1.1)
            }, completion: nil)
            
        }else if UIDevice.isPhone(){
            burgerTableView.frame = CGRect(x: self.view.bounds.minX - 10, y: self.view.bounds.minY + 100, width: 0, height: self.view.frame.height / 1.3)
            
            burgerTableView.layer.cornerRadius = 10
            burgerTableView.alpha = 0.9
            
            burgerTransparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
            burgerTableView.reloadData()
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(removeBurgerTransparentView))
            burgerTransparentView.addGestureRecognizer(tapGesture)
            burgerTransparentView.alpha = 0
            UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .overrideInheritedCurve, animations: {
                self.burgerTransparentView.alpha = 0.1
                self.burgerTableView.frame = CGRect(x: self.view.bounds.minX - 10, y: self.view.bounds.minY + 100, width: self.view.frame.width / 2 + 10, height: self.view.frame.height / 1.3)
            }, completion: nil)
        }
        self.view.addSubview(burgerTableView)
    }
    
    @objc func removeBurgerTransparentView() {
       // let frames = burgerButton.frame
        //iPad & iPhone
        if UIDevice.isPad() {
            UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .overrideInheritedCurve, animations: {
                self.burgerTransparentView.alpha = 0
                self.burgerTableView.frame = CGRect(x: self.view.bounds.minX - 10, y: self.view.bounds.minY + 130, width: -50, height: self.view.frame.height / 1.3)
            }, completion: nil)
        }else if UIDevice.isPhone() {
            UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .overrideInheritedCurve, animations: {
                self.burgerTransparentView.alpha = 0
                self.burgerTableView.frame = CGRect(x: self.view.bounds.minX - 10, y: self.view.bounds.minY + 100, width: -50, height: self.view.frame.height / 1.3)
            }, completion: nil)
        }
    }
    
    func setBurgerHeaderView() {
        let nib = UINib(nibName: "BurgerListPhotoHeaderView", bundle: nil)
        burgerTableView.register(nib, forHeaderFooterViewReuseIdentifier: "BurgerListPhotoHeaderView")
    }
    
    func setSpaceHeaderView() {
        let nib = UINib(nibName: "SpaceHeaderView", bundle: nil)
        burgerTableView.register(nib, forHeaderFooterViewReuseIdentifier: "SpaceHeaderView")
    }
    
//MARK: - Api
    private var syncChannel: RadioChannel? = nil
    private func syncApi(_ channel: RadioChannel) {
        if syncChannel == channel {
            return
        }
        DispatchQueue.init(label: "api").async {
            self.syncChannel = channel
            // 大頭條
            var postType: ApiService.PostType = .BIG_NEWS_ASIA
            switch RadioPlayer.shared.nowChannel {
            case .ASIA:
                postType = .BIG_NEWS_ASIA
            case .APAC:
                postType = .BIG_NEWS_APAC
            case .FLY:
                postType = .BIG_NEWS_FLY
            default:
                postType = .BIG_NEWS_ASIA
            }
            
            ApiService.getPostInfo(postType, 5, channel).apply {
                if $0.isSuccess {
                    self.AsiaHeaderLineDataList = $0.data?.info ?? []
                    DispatchQueue.main.async {
                        self.onairTableView.reloadSections(IndexSet(integer: TableType.TopNews.rawValue), with: .none)
                    }
                }
            }
            // 生活情報讚
            ApiService.getPostInfo(.LIFT_REPORT, 10, channel).apply {
                if $0.isSuccess {
                    self.onairLifeReportDataList = $0.data?.info ?? []
                    DispatchQueue.main.async {
                        self.onairTableView.reloadSections(IndexSet(integer: TableType.Life.rawValue), with: .none)
                    }
                }
            }
            // 活動報馬仔
            ApiService.getPostInfo(.ACTIVITY_REPORT, 10, channel).apply {
                if $0.isSuccess {
                    self.activityReportDataList = $0.data?.info ?? []
                    DispatchQueue.main.async {
                        self.onairTableView.reloadSections(IndexSet(integer: TableType.Event.rawValue), with: .none)
                    }
                }
            }
            // 好康贈獎
            ApiService.getPostInfo(.GOOD_LUCK_GIFT, 10, channel).apply {
                if $0.isSuccess {
                    self.couponDataList = $0.data?.info ?? []
                    DispatchQueue.main.async {
                        self.onairTableView.reloadSections(IndexSet(integer: TableType.Discount.rawValue), with: .none)
                    }
                }
            }
            DispatchQueue.main.async { self.onairTableView.reloadData() }
        }
    }
    
    func setBurgerSb(vc: UIViewController) {
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .right), dismissing: .push(direction: .left))
        self.present(vc, animated: true, completion: .none)
    }
}

extension OnairViewController: RadioRemoteViewDelegate {
    func onChangeRadioChannel(channel: RadioChannel) {
        changeRadioChannel(channel)
    }
}

//與TabBar按鈕連動
extension OnairViewController: RadioPlayerDelegate {
    func latestMusicListDidChange(_ latestMusicList: [ChannelInfoModel.LatestMusic], _ channel: RadioChannel) {
        changeLatestMusicList(latestMusicList, channel)
    }
    
    func playerChannelDidChange(_ playerState: FRadioPlayerState, _ channel: RadioChannel) {
        if playerState != .loading {
            return
        }
        headerView.radioRemoteView.scrollToRadioChannl(channel: channel, animated: false)
        changeRadioChannel(channel)
    }
    
    func playbackChannelDidChange(_ playbackState: FRadioPlaybackState, _ channel: RadioChannel) {
        //Tab鈕作動時更新專輯的播放icon
        if playbackState == .playing {
            headerView.headerPlayButton.setImage(UIImage(named: ""), for: .normal)
            headerView.ivOnAir.image = UIImage(named: "LIVE (on)")
        }else {
            headerView.headerPlayButton.setImage(UIImage(named: "icon_PLAY_shadow"), for: .normal)
            headerView.ivOnAir.image = UIImage(named: "ON AIR (off)")
        }
    }
}

extension OnairViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == burgerTableView {
            return 1
        }
        return TableType.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == burgerTableView {
            return burgerDataSource.count
        }
    
        guard let type = TableType(rawValue: section) else { return  0 }
        switch type {
        case .History:
            return RadioPlayer.shared.latestMusicList.count > 0 ? 1 : 0
        case .TopNews:
            return AsiaHeaderLineDataList.count > 0 ? 1 : 0
        case .Life:
            return onairLifeReportDataList.count > 0 ? 1 : 0
        case .Event:
            return activityReportDataList.count > 0 ? 1 : 0
        case .Discount:
            return couponDataList.count > 0 ? 1 : 0
        case .AD1:
            return 0
        case .AD2:
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == burgerTableView {
            return 20
        }else if section == 2 || section == 6 {
            return 0
        }else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == burgerTableView {
            let burgerHeader = self.burgerTableView.dequeueReusableHeaderFooterView(withIdentifier: "SpaceHeaderView") as! SpaceHeaderView
            
            return burgerHeader
        }
        
        guard let type = TableType(rawValue: section) else { return UIView() }
        switch type {
        case .History:
            return tableView.dequeueReusableHeaderFooterView(withClass: OnairLatestMusicHeaderView.self)
        case .TopNews:
            let headerView = UIView()
            UILabel().apply {
                headerView.addSubview($0)
                if RadioPlayer.shared.nowChannel == .FLY {
                    $0.text = "飛揚大頭條"
                }else if RadioPlayer.shared.nowChannel == .ASIA {
                    $0.text = "亞洲大頭條"
                }else {
                    $0.text = "亞太大頭條"
                }
                
                $0.font = $0.font.withSize(18).bold
                $0.textColor = .txt_title
                $0.snp.makeConstraints { maker in
                    maker.top.equalToSuperview().offset(28)
                    maker.bottom.equalToSuperview().offset(-4)
                    maker.left.equalToSuperview().offset(24)
                }
            }
            return headerView
        case .Life:
            let headerView = UIView()
            UILabel().apply {
                headerView.addSubview($0)
                $0.text = "生活情報讚"
                $0.font = $0.font.withSize(18).bold
                $0.textColor = .txt_title
                $0.snp.makeConstraints { maker in
                    maker.top.equalToSuperview().offset(28)
                    maker.bottom.equalToSuperview().offset(-4)
                    maker.left.equalToSuperview().offset(24)
                }
            }
            return headerView
        case .Event:
            let headerView = UIView()
            UILabel().apply {
                headerView.addSubview($0)
                $0.text = "活動報馬仔"
                $0.font = $0.font.withSize(18).bold
                $0.textColor = .txt_title
                $0.snp.makeConstraints { maker in
                    maker.top.equalToSuperview().offset(28)
                    maker.bottom.equalToSuperview().offset(-4)
                    maker.left.equalToSuperview().offset(24)
                }
            }
            return headerView
        case .Discount:
            let headerView = UIView()
            UILabel().apply {
                headerView.addSubview($0)
                $0.text = "好康贈獎"
                $0.font = $0.font.withSize(18).bold
                $0.textColor = .txt_title
                $0.snp.makeConstraints { maker in
                    maker.top.equalToSuperview().offset(28)
                    maker.bottom.equalToSuperview().offset(-5)
                    maker.left.equalToSuperview().offset(24)
                }
            }
            return headerView
        case .AD1:
            let headerView = UIView()
            UIView().apply {
                headerView.addSubview($0)
                $0.snp.makeConstraints { maker in
                    maker.top.equalToSuperview().offset(40)
                    maker.bottom.equalToSuperview()
                    maker.left.equalToSuperview()
                }
            }
            return headerView
        case .AD2:
            let headerView = UIView()
            UIView().apply {
                headerView.addSubview($0)
                $0.snp.makeConstraints { maker in
                    maker.top.equalToSuperview().offset(40)
                    maker.bottom.equalToSuperview()
                    maker.left.equalToSuperview()
                }
            }
            return headerView
        case .PowerOfAsiafm:
            let headerView = UIView()
            UILabel().apply {
                headerView.addSubview($0)
                $0.text = "2021 ASIAFM 亞洲廣播家族 ©"
                $0.font = $0.font.withSize(12).bold
                $0.textColor = .txt_subtitle
                $0.snp.makeConstraints { maker in
                    maker.top.equalToSuperview().offset(12)
                    maker.bottom.equalToSuperview().offset(-12)
                    maker.centerX.equalToSuperview()
                }
            }
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == burgerTableView {
            let burgerCell = tableView.dequeueReusableCell(withIdentifier: "BURGER_Cells", for: indexPath)
            burgerCell.textLabel?.text = burgerDataSource[indexPath.row]
            burgerCell.textLabel?.textAlignment = .center
           // burgerCell.selectionStyle = .none
            return burgerCell
        }
        
        guard let type = TableType(rawValue: indexPath.section) else {
            let cell = tableView.dequeueReusableCell(withClass: OnairTableCell.self, for: indexPath)
            cell.setValue(index: indexPath)
            return cell
        }
        switch type {
        case .History:
            let cell = tableView.dequeueReusableCell(withClass: OnairLatestMusicTableCell.self, for: indexPath)
            cell.setValue(RadioPlayer.shared.latestMusicList)

            return cell
        case .TopNews:
            let cell = tableView.dequeueReusableCell(withClass: OnairTopNewsTableCell.self, for: indexPath)
            cell.setValue(AsiaHeaderLineDataList)
            
            return cell
        case .Life:
            let cell = tableView.dequeueReusableCell(withClass: OnairLifeTableCell.self, for: indexPath)
            cell.setValue(onairLifeReportDataList)
            
            return cell
        case .Event:
            let cell = tableView.dequeueReusableCell(withClass: OnairEventTableCell.self, for: indexPath)
            cell.setValue(activityReportDataList)
            
            return cell
        case .Discount:
            let cell = tableView.dequeueReusableCell(withClass: OnairDiscountTableCell.self, for: indexPath)
            cell.setValue(couponDataList)
            return cell
            
        case .AD1:
            let cell = tableView.dequeueReusableCell(withClass: OnairAdTableCell.self, for: indexPath)
            cell.setValue(ad: "ad")
                        return cell
        case .AD2:
            let cell = tableView.dequeueReusableCell(withClass: OnairAdTableCell.self, for: indexPath)
            cell.setValue(ad: "ad")
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withClass: OnairTableCell.self, for: indexPath)
            cell.setValue(index: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == burgerTableView {
            return 50
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        burgerTableView.deselectRow(at: indexPath, animated: false)
        
        if tableView == burgerTableView {
            switch indexPath.row {
            case 0: guard let next = UIStoryboard.HeaderLine.instantiateViewController(withClass: HeaderLineViewController.self) else { return }
                setBurgerSb(vc: next)
                
            case 1: guard let next = UIStoryboard.FlyHeaderLine.instantiateViewController(withClass: FlyHeaderLineViewController.self) else { return }
                setBurgerSb(vc: next)
                
            case 2: guard let next = UIStoryboard.LifeReport.instantiateViewController(withClass: LifeReportViewController.self) else { return }
                setBurgerSb(vc: next)
                
            case 3: guard let next = UIStoryboard.ActivityReport.instantiateViewController(withClass: ActivityReportViewController.self) else { return }
                setBurgerSb(vc: next)
                
            case 4: guard let next = UIStoryboard.Coupon.instantiateViewController(withClass: CouponViewController.self) else { return }
                setBurgerSb(vc: next)
                
            case 5: guard let next = UIStoryboard.WinnerList.instantiateViewController(withClass: WinnerListViewController.self) else { return }
                setBurgerSb(vc: next)
                
            default:
                return
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            headerHeightConstraint.constant += abs(scrollView.contentOffset.y)
        } else if scrollView.contentOffset.y > 0 && headerHeightConstraint.constant >= headerMinHeight {
            headerHeightConstraint.constant -= scrollView.contentOffset.y / 100
            if headerHeightConstraint.constant < headerMinHeight {
                self.headerHeightConstraint.constant = headerMinHeight
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        beginScrollOffsetY = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollViewDidEnd(scrollView)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollViewDidEnd(scrollView)
    }
    
    private func scrollViewDidEnd(_ scrollView: UIScrollView){
        let endScrollOffsetY = scrollView.contentOffset.y
//        log(TAG, "beginScrollOffsetY \(beginScrollOffsetY)")
//        log(TAG, "endScrollOffsetY \(endScrollOffsetY)")
//        log(TAG, "offset \(beginScrollOffsetY - endScrollOffsetY)")
        if beginScrollOffsetY - endScrollOffsetY < 0 {
            // 往上
            if headerHeightConstraint.constant < headerMaxHeight {
                zoomInHeader()
            } else { zoomOutHeader() }
        } else {
            // 往下
            if headerHeightConstraint.constant > headerMaxHeight / 2 {
                zoomOutHeader()
            } else { zoomInHeader() }
        }
    }
    
    private func zoomOutHeader(completion: (() -> Void)? = nil) {
        headerView.ivAlpha.image = UIImage(named: RadioPlayer.shared.nowChannel.image)
        headerView.vAlpha.backgroundColor = RadioPlayer.shared.nowChannel.backgroundColor
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, animations: {
            self.headerHeightConstraint.constant = self.headerMaxHeight
            self.view.layoutIfNeeded()
            self.headerView.vAlpha.alpha = 0
        }) { _ in
            completion?()
        }
    }
    
    private func zoomInHeader(completion: (() -> Void)? = nil) {
        headerView.ivAlpha.image = UIImage(named: RadioPlayer.shared.nowChannel.image)
        headerView.ivAlpha.contentMode = .scaleToFill
        headerView.vAlpha.backgroundColor = RadioPlayer.shared.nowChannel.backgroundColor
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, animations: {
            self.headerHeightConstraint.constant = self.headerMinHeight
            self.view.layoutIfNeeded()
            self.headerView.vAlpha.alpha = 1
        }) { _ in
            completion?()
        }
    }
}

//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let y = 400 - (scrollView.contentOffset.y + 400)
//        let height = min(max(y, 110 + topSafeArea), 400)
////        imageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: height)
//        headerView.snp.updateConstraints { maker in
//            maker.height.equalTo(height)
//        }
//    }
