//
//  EditMemberDataViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/22.
//

import UIKit

class EditMemberDataViewController: BaseViewController, UINavigationControllerDelegate {

    private var UpdateNumberData: UpdateNumberDataModel?
    private var errorString: String = ""
    private var memberId: Int = 0
    private var syncErrorString: String = "''"
    
    var inputNameString: String = ""
    var inputGenderString: String = ""
    var inputEmailString: String = ""
    var inputBirthdayString: String = ""
    var inputAddressString: String = ""
    
    var nameString: String = ""
    var genderString: String = ""
    var emailString: String = ""
    var birthdayString: String = ""
    var addressString: String = ""
    var toUploadString: String = ""
    var editBase64String: String = ""
    var compress64String: String = ""
    let fakeImge = UIImage()
    
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var headShotImageView: UIImageView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userGenderLabel: UILabel!
    @IBOutlet weak var userBirthdayLabel: UILabel!
    @IBOutlet weak var userAddressTextField: UITextField!
    @IBOutlet weak var svMain: UIScrollView!
    @IBOutlet weak var vMain: UIView!
    
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func headShotChange(_ sender: Any) {
        uploadPhotos()
    }
    
    @IBAction func confirmChangesAct(_ sender: Any) {
        if userNameTextField.text == "" {
            let controller = UIAlertController(title: "錯誤", message: "名稱欄位不能為空", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }else if  userEmailTextField.text == "" {
            let controller = UIAlertController(title: "錯誤", message: "電子信箱欄位不能為空", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }else {
            let imageData = (headShotImageView.image ?? fakeImge).jpegData(compressionQuality: 0.6)
            compress64String = imageData?.base64EncodedString() ?? ""
            
//            headShotImageView.image?.compressImageOnlength(maxLength: 500)
//            toUploadString = headShotImageView.image?.pngData()?.base64EncodedString() ?? ""
            syncApi()
            showEditVc()
        }
        
    }
    
    @IBAction func changeGenderButtonAction(_ sender: Any) {
        showGenderAlert{}
    }
    
    
    @IBAction func changeBirthdayButtonAction(_ sender: Any) {
        showBirthdayPickerAlert{}
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberLabel.text = ud.getUserAccount()
        menberLoginSyncApi()
        
        self.userNameTextField.attributedPlaceholder = NSAttributedString(string:
            "＊必填", attributes:
                [NSAttributedString.Key.foregroundColor:UIColor.red])
        
        self.userEmailTextField.attributedPlaceholder = NSAttributedString(string:
            "＊必填", attributes:
                [NSAttributedString.Key.foregroundColor:UIColor.red])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initEvent()
    }
    
    override func viewWillLayoutSubviews() {
        nameString = userNameTextField.text ?? ""
        emailString = userEmailTextField.text ?? ""
        genderString = userGenderLabel.text ?? ""
        birthdayString = userBirthdayLabel.text ?? ""
        addressString = userAddressTextField.text ?? ""
    }
    
    private func showEditVc(){
        let editVc = EditSuccessPopViewController.loadFromNib()
        
        editVc.dismissSuccessed = {
            self.dismiss(animated: true)
            self.dismiss(animated: true)
        }
        self.present(editVc, animated: true, completion: nil)
    }
    
    func dissmissFunc() {
        self.dismiss(animated: true)
    }
    
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    private func initEvent() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: .keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .keyboardWillHideNotification, object: nil)
        
        let arr = [userNameTextField, userEmailTextField, userAddressTextField]
        for i in 0..<arr.count {
            arr[i]?.delegate = self
            arr[i]?.tag = i
        }
    }
    
    private var keyboardHeight:CGFloat?
    @objc func keyboardWillChangeFrame(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            self.keyboardHeight = CGFloat(keyboardFrame.cgRectValue.height) - 130
            self.svMain.contentSize.height = self.vMain.height - (self.keyboardHeight ?? 0)
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let _: NSValue =
            notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            DispatchQueue.main.async {
                self.svMain.contentSize.height = self.vMain.height + (self.keyboardHeight ?? 0)
                self.svMain.setContentOffset(CGPoint(x:self.svMain.x, y:  (self.keyboardHeight ?? 0)), animated: true)
            }
        }
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        self.svMain.contentSize.height = self.svMain.contentSize.height - (self.keyboardHeight ?? 0)
        self.view.layoutIfNeeded()
        self.keyboardHeight = 0
        DispatchQueue.main.async {
            self.svMain.setContentOffset(CGPoint(x:0, y:  0), animated: true)
        }
    }
    
    private func showVerifyVc(){
        let editVc = CodeHadSendViewController.loadFromNib()
        
        editVc.dismissSuccessed = {
            self.dismiss(animated: true)
        }
        self.present(editVc, animated: true, completion: nil)
    }
    
    private func showGenderAlert(closure: @escaping () -> ()) {
        let alertVc = GenderScrollWheelViewController.loadFromNib()
        
        alertVc.onClickConfirm = { (gender: String) in
            self.userGenderLabel.text = gender
            alertVc.dismiss(animated: true, completion: nil)
        }
        
        alertVc.onClickDismiss = {
            alertVc.dismiss(animated: true) {
                closure()
            }
        }
        self.present(alertVc, animated: true, completion: nil)
    }
    
    private func showBirthdayPickerAlert(closure: @escaping () -> ()) {
        let alertVc = BirthdayPickerController.loadFromNib()
        
        alertVc.onClickDismiss = {
            alertVc.dismiss(animated: true) {
                closure()
            }
        }
        alertVc.onClickConfirm = { (birthday: String) in
            self.userBirthdayLabel.text = birthday
            alertVc.dismiss(animated: true, completion: nil)
        }
        self.present(alertVc, animated: true, completion: nil)
    }
    
    // 會員資料更新
    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            ApiService.getUpdateNumberData(userId: String(self.memberId), gender: self.genderString, birthday: self.birthdayString, address: self.addressString, avatar: self.compress64String, name: self.nameString, email: self.emailString).apply {
                if $0.isSuccess {
                    self.syncErrorString = $0.data?.message ?? ""
                }
            }
        }
    }
    
    private func menberLoginSyncApi() {
        DispatchQueue.init(label: "api").async {
            ApiService.getLoginData().apply {
                if $0.isSuccess {
                    self.memberId = $0.data?.data?.userId ?? 0
                    self.inputNameString = $0.data?.data?.name ?? ""
                    self.inputGenderString = $0.data?.data?.gender ?? ""
                    self.inputEmailString = $0.data?.data?.email ?? ""
                    self.inputBirthdayString = $0.data?.data?.birthday ?? ""
                    self.inputAddressString = $0.data?.data?.address ?? ""
                    self.editBase64String = $0.data?.data?.avatar ?? ""
                    
                    self.errorString = $0.data?.message ?? ""
                }
            }
            DispatchQueue.main.async {
                self.userNameTextField.text = self.inputNameString
                self.userEmailTextField.text = self.inputEmailString
                self.userGenderLabel.text = self.inputGenderString
                self.userBirthdayLabel.text = self.inputBirthdayString
                self.userAddressTextField.text = self.inputAddressString
                
                if self.editBase64String != "" {
                    let dataDecoded: NSData = NSData(base64Encoded: self.editBase64String, options: NSData.Base64DecodingOptions(rawValue: 0))!
                    self.headShotImageView.image = UIImage(data: dataDecoded as Data)
                    
                }else {
                    self.headShotImageView.image = UIImage(named: "userphoto")
                }
                
            }
        }
    }
    
    func uploadPhotos() {
        let photoSourceRequestController = UIAlertController(title: "", message: "選擇圖片", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "開啟相機", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .camera
                imagePicker.delegate = self
                
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        
        let photoLibraryAction = UIAlertAction(title: "開啟相簿", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .photoLibrary
                imagePicker.delegate = self
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: .none)
        
        photoSourceRequestController.addAction(cameraAction)
        photoSourceRequestController.addAction(photoLibraryAction)
        photoSourceRequestController.addAction(cancelAction)
        photoSourceRequestController.modalPresentationStyle = .fullScreen
        
        present(photoSourceRequestController, animated: true, completion: nil)
    }
    
    func resizeImage(_ image: UIImage, newHeight: CGFloat) -> UIImage {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = newImage!.jpegData(compressionQuality: 0.5)! as Data
        UIGraphicsEndImageContext()
        return UIImage(data:imageData)!
    }

}

extension EditMemberDataViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            headShotImageView.image = selectedImage
        }
        dismiss(animated: true, completion: .none)
    }
}

//尚未作用
extension UIImage {
    func compressImageOnlength(maxLength: Int) -> Data? {
        let maxL = maxLength * 1024 * 1024
        var compress:CGFloat = 0.9
        let maxCompress:CGFloat = 0.1
        var imageData = self.jpegData(compressionQuality: compress)
        while (imageData?.count)! > maxL && compress > maxCompress {
            compress -= 0.1
            imageData = self.jpegData(compressionQuality: compress)
        }
        return imageData
    }
}
