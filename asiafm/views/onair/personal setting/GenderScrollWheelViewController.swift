//
//  GenderScrollWheelViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/15.
//

import UIKit

class GenderScrollWheelViewController: UIViewController {

    var onClickDismiss: (() -> ())?
    var onClickConfirm: ((_ gender: String) -> ())?
    var onViewDidLoad: (() -> ())?
    
    let genderChoice: [String] = ["男", "女", "其他"]
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var genderPickerView: UIPickerView!
    
    @IBAction func confirmGenderAction(_ sender: Any) {
        let gender = genderChoice[genderPickerView.selectedRow(inComponent: 0)]
        self.onClickConfirm?(gender)
    }
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.onClickDismiss?()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
       // onViewDidLoad?()
    }

}

extension GenderScrollWheelViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        genderChoice.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        genderChoice[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont(name: "Futura", size: 24)
                pickerLabel?.textAlignment = .center
            }
            pickerLabel?.text = genderChoice[row]
            pickerLabel?.textColor = .def_tab_selected

            return pickerLabel!
    }
    
}
