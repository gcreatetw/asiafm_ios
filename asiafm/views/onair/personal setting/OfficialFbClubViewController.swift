//
//  OfficialFbClubViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/12.
//

import UIKit

class OfficialFbClubViewController: BaseViewController {

    let fbChannelName: [String] = ["亞洲電台FM92.7", "亞太電台FM92.3", "飛揚調頻FM89.5"]
    
    @IBOutlet weak var radioChannelTableView: UITableView!
   
    @IBAction func onClickDismiss(_ sender: Any) {
        dismiss(animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        radioChannelTableView.delegate = self
        radioChannelTableView.dataSource = self
    }
    
    func openFacebookApp(fbUrl: String) {
        let facebookURL = NSURL(string: fbUrl)!
        if UIApplication.shared.canOpenURL(facebookURL as URL) {
            UIApplication.shared.open(facebookURL as URL, completionHandler: nil)
        } else {
            UIApplication.shared.open(NSURL(string: fbUrl)! as URL)
        }
    }
    
}

extension OfficialFbClubViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fbChannelName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: OfficialFbClubTableViewCell.self, for: indexPath)
        cell.channelLabel.text = fbChannelName[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                openFacebookApp(fbUrl: "fb://page/?id=115266555162342")
                next.url = "https://www.facebook.com/asiaradiofamily"
                
                self.present(next, animated: true)
            }
        }
        
        if indexPath.row == 1 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                openFacebookApp(fbUrl: "fb://page/?id=1009171459190589")
                next.url = "https://zh-tw.facebook.com/asiafm923/"
                self.present(next, animated: true)
            }
        }
        
        if indexPath.row == 2 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                openFacebookApp(fbUrl: "fb://page/?id=237126950007339")
                next.url = "https://zh-tw.facebook.com/flyradioFM89.5/"
                self.present(next, animated: true)
            }
        }
    }
    
}
