//
//  BirthdayPickerController.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/17.
//

import UIKit

class BirthdayPickerController: UIViewController {

    var onClickDismiss: (() -> ())?
    var onClickConfirm: ((_ birthday: String) -> ())?
    var onViewDidLoad: (() -> ())?
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var birthdayDatePicker: UIDatePicker!
    
    @IBAction func choiceBirthdayAction(_ sender: Any) {
        
    }
    
    @IBAction func onClickDismiss(_ sender: Any) {
        self.onClickDismiss?()
    }
    
    @IBAction func confirmBirthdayButtonAction(_ sender: Any) {
        let userBirthday = birthdayDatePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let birthday = formatter.string(from: userBirthday)
        
        self.onClickConfirm?(birthday)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        onViewDidLoad?()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
