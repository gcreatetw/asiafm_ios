//
//  StartPlayingViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/23.
//

import UIKit

class StartPlayingViewController: UIViewController {

    var onClickDismiss: (() -> ())?
    var onClickConfirm: ((_ playTime: String, _ playDate: Date) -> ())?
    
    @IBOutlet weak var setPlayTimePicker: UIDatePicker!
    
    @IBAction func dismissOnClick(_ sender: Any) {
        self.onClickDismiss?()
    }
    
    @IBAction func confirmPlayTimeAction(_ sender: Any) {
        let setPlayTime = setPlayTimePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let playTime = formatter.string(from: setPlayTime)
        
        self.onClickConfirm?(playTime, setPlayTimePicker.date)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
