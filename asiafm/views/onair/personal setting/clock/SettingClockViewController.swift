//
//  SettingClockViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/22.
//

import UIKit
import UserNotifications

class SettingClockViewController: BaseViewController {

    var currentTime: String = ""
    var currentTimeDate = Date()
    var stopDate = Date()
    var stopTimer: Timer?
    
    @IBOutlet weak var stopPlayingLabel: UILabel!
    @IBOutlet weak var stopPlayingSwitch: UISwitch!
    
    @IBAction func stopSwitchAction(_ sender: UISwitch) {
        ud.setStopSwitchIsOn(isOn: sender.isOn)
        if sender.isOn == true {
            stopTimeNotifiction()
            ud.setStopNotificationIsOn(isOn: true)
        }else {
            ud.setStopNotificationIsOn(isOn: false)
            removeStopTimeNotification()
        }
    }
    
    @IBAction func dismissOnClick(_ sender: Any) {
        dismiss(animated: true, completion: .none)
    }
    
    @IBAction func stopEditButtonAction(_ sender: Any) {
        showSelectStopTimeAlarm{}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        stopPlayingSwitch.isOn = ud.getStopSwitchIsOn()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if ud.getClockStopTime() == "" {
            stopPlayingLabel.text = "12:00"
        }else {
            stopPlayingLabel.text = ud.getClockStopTime()
        }
    }
    
    //彈出視窗
    private func showSelectStopTimeAlarm(closure: @escaping () -> ()) {
        let alertVc = StopPlayingViewController.loadFromNib()
        
        alertVc.onClickConfirm = { (time: String) in
            self.stopPlayingLabel.text = time
            ud.setClockStopTime(time: time)
            alertVc.dismiss(animated: true, completion: nil)
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            dateFormatter.dateFormat = "HH:mm"
            self.stopDate = dateFormatter.date(from: time)!
            
            if ud.getStopSwitchIsOn() == true {
                self.stopTimeNotifiction()
            }
           // print("checkTime ====== \(self.stopDate)")
        }
        
        alertVc.onClickDismiss = {
            alertVc.dismiss(animated: true) {
                closure()
            }
        }
        self.present(alertVc, animated: true, completion: nil)
    }

//    func setCurrentTimeFormatter() {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "HH:mm"
//        let time = formatter.string(from: Date())
//
//        currentTime = time
//    }
    
//MARK: - StopTimer
    func setStopTimer() {
        stopTimer = Timer.scheduledTimer(timeInterval: 86400, target: self, selector: #selector(stopTimerFunc), userInfo: nil, repeats: true)
        RunLoop.current.add(stopTimer!, forMode: RunLoop.Mode.common)
    }

    @objc func stopTimerFunc() {
        RadioPlayer.shared.stop()
    }

    func launchStopTimer() {
        if self.stopTimer != nil {
            self.stopTimer?.fire()
        }
    }
    
//MARK: - StopTime
    
    func stopTimeNotifiction() {
        let content = UNMutableNotificationContent()
        content.title = "自動播放功能已關閉"
        content.userInfo["isPlay"] = false
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: stopPlayingLabel.text!)!
        
        let components = Calendar.current.dateComponents([ .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components,
                                                              repeats: false)
        let request = UNNotificationRequest(identifier: "stopNotificationl",
                                                      content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
       // print("發送通知\(components)")
    }
    
    func removeStopTimeNotification() {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["stopNotificationl"])
    }
    
}
