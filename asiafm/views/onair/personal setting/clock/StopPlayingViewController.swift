//
//  StopPlayingViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/23.
//

import UIKit

class StopPlayingViewController: UIViewController {

    var onClickDismiss: (() -> ())?
    var onClickConfirm: ((_ stopTime: String) -> ())?
    
    @IBOutlet weak var setStopTimePicker: UIDatePicker!
    
    @IBAction func dismissOnClick(_ sender: Any) {
        self.onClickDismiss?()
    }
    
    @IBAction func confirmStopTimeAction(_ sender: Any) {
        let setStopTime = setStopTimePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let stopTime = formatter.string(from: setStopTime)
        
        self.onClickConfirm?(stopTime)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
