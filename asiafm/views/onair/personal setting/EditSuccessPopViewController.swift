//
//  EditSuccessPopViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/23.
//

import UIKit

class EditSuccessPopViewController: UIViewController {

    var dismissSuccessed: (() -> ())?
    
    @IBAction func dismissSuccessPop(_ sender: Any) {

        self.dismissSuccessed?()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let nib = UINib(nibName: "EditSuccessPopView", bundle: nil)
//        EditSuccessPopViewController.register(nib, forHeaderFooterViewReuseIdentifier: "SearchMusicListHeaderView")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
