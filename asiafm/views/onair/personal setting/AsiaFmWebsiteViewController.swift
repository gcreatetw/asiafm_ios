//
//  AsiaFmWebsiteViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/26.
//

import UIKit

class AsiaFmWebsiteViewController: BaseViewController {

    let webChannelName: [String] = ["亞洲電台FM92.7", "亞太電台FM92.3", "飛揚調頻FM89.5"]
    
    @IBOutlet weak var AsiaFmWebsiteTableView: UITableView!
    
    @IBAction func onClickDismiss(_ sender: Any) {
        dismiss(animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AsiaFmWebsiteTableView.delegate = self
        AsiaFmWebsiteTableView.dataSource = self
    }

}

extension AsiaFmWebsiteViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return webChannelName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: AsiaFmWebsiteTableViewCell.self, for: indexPath)
        cell.websiteLabel.text = webChannelName[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        if indexPath.row == 0 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                next.url = "https://www.asiafm.com.tw/"
                next.titleString = "亞洲電台"
                self.present(next, animated: true)
            }
        }
        
        if indexPath.row == 1 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                next.url = "https://www.asiafm.com.tw/"
                next.titleString = "亞太電台"
                self.present(next, animated: true)
            }
        }
        
        if indexPath.row == 2 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                next.url = "https://www.flyradio.com.tw/"
                next.titleString = "飛揚調頻"
                self.present(next, animated: true)
            }
        }
    }
    
}
