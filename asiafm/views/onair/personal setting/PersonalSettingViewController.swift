//
//  PersonalSettingViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/22.
//

import UIKit

class PersonalSettingViewController: BaseViewController {
    
    private var userNameString: String = ""
    private var userBase64String: String = ""
    var decodedImage: UIImage?
    var radioSwitchBool = Bool()
    
    @IBOutlet weak var radioSwitch: UISwitch!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPhoneNumberLabel: UILabel!
    @IBOutlet weak var settingUserImage: UIImageView!
    
    func setStoryBoard(vc: UIViewController) {
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: .none)
    }

    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func editMemberData(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "EditMemberDataViewController")
        setStoryBoard(vc: vc)
    }
    
    @IBAction func changePasswordAct(_ sender: Any) {
        let vc = UIStoryboard.PersonalSetting.instantiateViewController(identifier: "ChangePasswordsViewController")
        setStoryBoard(vc: vc)
    }
    
    @IBAction func settingClockAction(_ sender: Any) {
        let vc = UIStoryboard.SettingClock.instantiateViewController(identifier: "SettingClockViewController")
        setStoryBoard(vc: vc)
    }
    
    
    @IBAction func gotoFbClub(_ sender: Any) {
        let clubVc = UIStoryboard.OfficialFbClub.instantiateViewController(identifier: "OfficialFbClubViewController")
        setStoryBoard(vc: clubVc)
    }
    //帳號登出
    @IBAction func onClickLogout(_ sender: Any) {
        let alertVc = AlertTextViewController.loadFromNib()
        alertVc.messageString = "確定要登出嗎"
        alertVc.onViewDidLoad = { }
        alertVc.onClickRight = {
            alertVc.dismiss(animated: true, completion: {
                alertVc.dismiss(animated: true){
                    cleanData()
                    RadioPlayer.shared.stop()
                    self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
                }
            })
        }
        alertVc.onClickLeft = {
            alertVc.dismiss(animated: true, completion: nil)
        }
        self.present(alertVc, animated: true, completion: nil)
    }
    
    @IBAction func gotoAboutAsia(_ sender: Any) {
        let vc = UIStoryboard.AboutAsiaFamily.instantiateViewController(identifier: "AboutAsiaFamily")
        setStoryBoard(vc: vc)
    }
    
    @IBAction func contactUsAction(_ sender: Any) {
        dialNumber()
    }
    
    @IBAction func gotoOffcialWebsite(_ sender: Any) {
        if let vc = UIStoryboard.OfficialWebsite.instantiateViewController(withClass: AsiaFmWebsiteViewController.self) {
            setStoryBoard(vc: vc)
        }
    }
    
    @IBAction func gotoIG(_ sender: Any) {
        if let vc = UIStoryboard.IgPage.instantiateViewController(withClass: IGViewController.self) {
            setStoryBoard(vc: vc)
        }
    }
    
    @IBAction func radioSwitchAction(_ sender: UISwitch) {
        radioSwitchBool = sender.isOn
        ud.setRadioIsOn(isOn: sender.isOn)
        if radioSwitchBool == true {
            RadioPlayer.shared.play()
            print("音樂開啟")
        }else {
            RadioPlayer.shared.stop()
            print("音樂關閉")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userPhoneNumberLabel.text = "手機：\(ud.getUserAccount())"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        radioSwitch.isOn = ud.getRadioIsOn()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        syncApi()
    }
    
    func dialNumber() {
        let urlString = "tel:03-2209207"
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                            completionHandler: { (success) in
                })
            } else { UIApplication.shared.openURL(url) }
        }
    }
    
    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            ApiService.getLoginData().apply {
                if $0.isSuccess {
                    self.userNameString = $0.data?.data?.name ?? ""
                    self.userBase64String = $0.data?.data?.avatar ?? ""
                }
            }
            DispatchQueue.main.async {
                self.userNameLabel.text = self.userNameString
                if self.userBase64String != "" {
                    let dataDecoded: NSData = NSData(base64Encoded: self.userBase64String, options: NSData.Base64DecodingOptions(rawValue: 0))!

                    self.settingUserImage.image = UIImage(data: dataDecoded as Data)
                }else {
                    self.settingUserImage.image = UIImage(named: "userphoto")
                    print("Not Base64 Image")
                }
                
            }
        }
    }
}
