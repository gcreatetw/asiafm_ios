//
//  IGViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/26.
//

import UIKit

class IGViewController: BaseViewController {

    let IgChannelName: [String] = ["亞洲電台FM92.7", "亞太電台FM92.3"]
    
    @IBOutlet weak var igTableVIew: UITableView!
    
    @IBAction func onClickDismiss(_ sender: Any) {
        dismiss(animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        igTableVIew.delegate = self
        igTableVIew.dataSource = self
    }

    func openIGApp(igUrl: String, webUrl: String) {
        let appUrl = URL(string: "instagram://user?username=\(igUrl)")!
        let application = UIApplication.shared
        
        if application.canOpenURL(appUrl) {
            application.open(appUrl)
        }else {
            let url = URL(string: webUrl)!
            application.open(url)
        }
    }
}


extension IGViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return IgChannelName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: IGTableViewCell.self, for: indexPath)
        
        cell.igLabel.text = IgChannelName[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                next.url = "https://www.instagram.com/asiafm_927/"
                openIGApp(igUrl: "asiafm_927", webUrl: next.url)
                
                self.present(next, animated: true, completion: nil)
            }
        }
        
        if indexPath.row == 1 {
            if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                
                next.url = "https://www.instagram.com/asiafm_923/"
                openIGApp(igUrl: "asiafm_923", webUrl: next.url)
                self.present(next, animated: true, completion: nil)
            }
        }
    }
    
    
}
