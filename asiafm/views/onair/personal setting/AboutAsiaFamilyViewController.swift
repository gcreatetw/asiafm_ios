//
//  AboutAsiaFamilyViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/26.
//

import UIKit

class AboutAsiaFamilyViewController: UIViewController {

    @IBOutlet weak var firstLabel: UILabel!
    
    @IBOutlet weak var secondLabel: UILabel!
    
    @IBOutlet weak var thirdLabel: UILabel!
    
    @IBOutlet weak var fourthLabel: UILabel!
    
    @IBOutlet weak var fifthLabel: UILabel!
    
    
    @IBAction func onClickDismiss(_ sender: Any) {
        dismiss(animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLineSpacing()
    }
    
    func setLineSpacing() {
        firstLabel.setLineSpacing(lineSpacing: 5.0, lineHeightMultiple: 0.0)
        secondLabel.setLineSpacing(lineSpacing: 5.0, lineHeightMultiple: 0.0)
        thirdLabel.setLineSpacing(lineSpacing: 5.0, lineHeightMultiple: 0.0)
        fourthLabel.setLineSpacing(lineSpacing: 5.0, lineHeightMultiple: 0.0)
        fifthLabel.setLineSpacing(lineSpacing: 5.0, lineHeightMultiple: 0.0)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
