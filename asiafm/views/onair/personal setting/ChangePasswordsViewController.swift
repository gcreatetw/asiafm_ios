//
//  ChangePasswordsViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/22.
//

import UIKit

class ChangePasswordsViewController: BaseViewController {

    private var changePasswordsDataSource: UpdateUserPasswordModel?
    private var changePasswordLoginData: LoginDataModel?
    
    var errorString: String = ""
    var syncErrorString: String = ""
    var oldPasswordString: String = ""
    var newPasswordString: String = ""
    var userPhoneNumber: String = ""
    var userId: Int = 0
    
    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func confirmChangesAct(_ sender: Any) {
        if errorString == "error" {
            let controller = UIAlertController(title: "通知", message: "輸入的舊密碼有誤", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }else if oldPasswordTextField.text == "" {
            let controller = UIAlertController(title: "請輸入您的舊密碼", message: "舊密碼欄位尚未輸入", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }else if enterNewPasswordTextField.text == "" {
            let controller = UIAlertController(title: "請輸入您的新密碼", message: "新密碼欄位尚未輸入", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }else if enterNewPasswordAgainTextField.text == "" {
            let controller = UIAlertController(title: "通知", message: "『請再次輸入密碼』欄位尚未輸入", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }else if oldPasswordTextField.text != ud.getPassword() {
            let controller = UIAlertController(title: "錯誤", message: "舊密碼輸入錯誤", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }
        
        else if enterNewPasswordAgainTextField.text != enterNewPasswordTextField.text {
            let controller = UIAlertController(title: "錯誤", message: "『請再次輸入新密碼』與『新密碼』不符", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default, handler: nil)
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }
        
        else {
            syncApi()
            showEditVc()
            ud.setPassword(password: enterNewPasswordAgainTextField.text ?? "")
        }
    }
    
    @IBAction func reverifyAct(_ sender: Any) {
        showVerifyVc()
    }
    
    @IBOutlet weak var firstStick: UIView!
    @IBOutlet weak var secondStick: UIView!
    @IBOutlet weak var thirdStick: UIView!
    
    @IBOutlet weak var newPasswordLabel: UILabel!
    @IBOutlet weak var reEnterPasswordLabel: UILabel!
    @IBOutlet weak var enterNewPasswordAgainLabel: UILabel!
    
    
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var enterNewPasswordTextField: UITextField!
    @IBOutlet weak var enterNewPasswordAgainTextField: UITextField!
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == oldPasswordTextField {
            newPasswordLabel.textColor = UIColor(named: "def_tab_selected")
            reEnterPasswordLabel.textColor = UIColor.black
            enterNewPasswordAgainLabel.textColor = UIColor.black
            
            firstStick.backgroundColor = UIColor(named: "def_tab_selected")
            secondStick.backgroundColor = UIColor(named: "def_btn2_bg_touch")
            thirdStick.backgroundColor = UIColor(named: "def_btn2_bg_touch")
        }
        if textField == enterNewPasswordTextField {
            newPasswordLabel.textColor = UIColor.black
            reEnterPasswordLabel.textColor = UIColor(named: "def_tab_selected")
            enterNewPasswordAgainLabel.textColor = UIColor.black
            
            firstStick.backgroundColor = UIColor(named: "def_btn2_bg_touch")
            secondStick.backgroundColor = UIColor(named: "def_tab_selected")
            thirdStick.backgroundColor = UIColor(named: "def_btn2_bg_touch")
        }
        if textField == enterNewPasswordAgainTextField {
            newPasswordLabel.textColor = UIColor.black
            reEnterPasswordLabel.textColor = UIColor.black
            enterNewPasswordAgainLabel.textColor = UIColor(named: "def_tab_selected")
            
            firstStick.backgroundColor = UIColor(named: "def_btn2_bg_touch")
            secondStick.backgroundColor = UIColor(named: "def_btn2_bg_touch")
            thirdStick.backgroundColor = UIColor(named: "def_tab_selected")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        oldPasswordTextField.delegate = self
        enterNewPasswordTextField.delegate = self
        enterNewPasswordAgainTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loginSyncApi()
        hiddenKeyboard()
    }
    
    override func viewWillLayoutSubviews() {
        oldPasswordString = oldPasswordTextField.text ?? ""
        newPasswordString = enterNewPasswordTextField.text ?? ""
    }
    
    private func showEditVc(){
        let editVc = EditSuccessPopViewController.loadFromNib()
        
        editVc.dismissSuccessed = {
            self.dismiss(animated: true)
            self.dismiss(animated: true)
        }
        self.present(editVc, animated: true, completion: nil)
    }
    
    private func showVerifyVc(){
        let editVc = CodeHadSendViewController.loadFromNib()
        
        editVc.dismissSuccessed = {
            self.dismiss(animated: true)
        }
        self.present(editVc, animated: true, completion: nil)
    }
    
    func hiddenKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    private func syncApi() {
        DispatchQueue.init(label: "api").async {
            ApiService.getUpdateUserPasswordData(userId: self.userId, oldPassword: self.oldPasswordString, newPassword: self.newPasswordString).apply {
                if $0.isSuccess {
                    self.syncErrorString = $0.data?.message ?? ""
                }
            }
        }
    }
    
    private func loginSyncApi() {
        DispatchQueue.init(label: "api").async {
            ApiService.getLoginData().apply {
                if $0.isSuccess {
                    self.userPhoneNumber = $0.data?.data?.phone ?? ""
                    self.userId = $0.data?.data?.userId ?? 0
                    self.errorString = $0.data?.message ?? ""
                }
            }
        }
    }
    
}
