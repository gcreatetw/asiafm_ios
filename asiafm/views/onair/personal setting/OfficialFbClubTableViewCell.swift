//
//  OfficialFbClubTableViewCell.swift
//  asiafm
//
//  Created by StevenTang on 2021/8/12.
//

import UIKit

class OfficialFbClubTableViewCell: UITableViewCell {

    @IBOutlet weak var channelLabel: UILabel!
    @IBOutlet weak var gotoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
