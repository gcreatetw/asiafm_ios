//
//  OnairTableCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/21.
//

import UIKit

class OnairTableCell: UITableViewCell {
    var label: UILabel!
    var bg: UIImageView!
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        bg = UIImageView()
        addSubview(bg)
        bg.backgroundColor = .lightGray
        bg.image = UIImage(named: "logo_895")
//        bg.contentMode = .scaleAspectFill
        bg.snp.remakeConstraints { maker in
//            maker.width.equalTo(screenWidth)
//            maker.height.equalTo(200)
//            maker.width.equalTo(200)
            maker.top.equalToSuperview().offset(5)
            maker.bottom.equalToSuperview().offset(-5)
            maker.left.equalToSuperview().offset(5)
            maker.right.equalToSuperview().offset(-5)
        }
        label = UILabel()
        addSubview(label)
        label.snp.makeConstraints { maker in
//            maker.center.equalToSuperview()
            maker.top.equalToSuperview().offset(5)
            maker.bottom.equalToSuperview().offset(-5)
            maker.left.equalToSuperview().offset(5)
            maker.right.equalToSuperview().offset(-5)
        }
        label.numberOfLines = 0
        label.text = "label \(frame.width) \(frame.height)"
        var str = ""
        for _ in 0...Int.random(in: 0...2) {
            str += "These projects are fully working examples of table views with variable row heights due to table view cells containing dynamic content in UILabels."
        }
        label.text = str
        
        self.backgroundColor = .yellow
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValue(index: IndexPath){
        var str = "\(index)"
        for _ in 0...(index.row % 3) {
            str += "These projects are fully working examples of table views with variable row heights due to table view cells containing dynamic content in UILabels."
        }
        label.text = str
    }
}
