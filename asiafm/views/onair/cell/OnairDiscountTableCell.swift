//
//  OnairDiscountTableCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/24.
//

import UIKit

class OnairDiscountTableCell: UITableViewCell {
    var collectionView: UICollectionView!
    var dataList = [PostInfoModel.Info]()
    var onClickItem: ((IndexPath, PostInfoModel.Info) -> ())?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .transparent
        selectionStyle = .none
        
        let flowlayout = UICollectionViewFlowLayout().apply {
            $0.itemSize = .init(width: 150, height: 184)
            $0.sectionInset = .init(top: 0, left: 28, bottom: 0, right: 8)
            $0.minimumLineSpacing = 28
            $0.scrollDirection = .horizontal
        }
        collectionView = UICollectionView(frame: contentView.bounds, collectionViewLayout: flowlayout).apply {
            addSubview($0)
            $0.backgroundColor = .transparent
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(12)
                //maker.left.equalTo(20)
                maker.left.equalToSuperview()
                maker.right.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.height.equalTo(184)
            }
            
            $0.isUserInteractionEnabled = true
            $0.delegate = self
            $0.dataSource = self
            $0.showsHorizontalScrollIndicator = false
            $0.register(cellWithClass: OnairDiscountCollectionCell.self)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValue(_ list: [PostInfoModel.Info]){
        dataList = list
        collectionView.reloadData()
    }
}

extension OnairDiscountTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: OnairDiscountCollectionCell.self, for: indexPath)
        cell.setValue(model: dataList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //onClickItem?(indexPath, dataList[indexPath.row])
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.doubleUrl = dataList[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "好康贈獎")
            Self.getLastPresentedViewController()?.present(next, animated: true)
        }
    }
}
