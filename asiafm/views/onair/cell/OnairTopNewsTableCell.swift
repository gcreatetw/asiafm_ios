//
//  OnairTopNewsTableCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/23.
//  大頭條

import UIKit

class OnairTopNewsTableCell: UITableViewCell {
    var collectionView: UICollectionView!
    //var newsCollectionView:
    var pageControl: UIPageControl!
    var asiaDataList = [PostInfoModel.Info]()
    var currentIndex: Int = 1
    var timer: Timer?
    
    var onClickItem: ((IndexPath, PostInfoModel.Info) -> ())?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .transparent
        selectionStyle = .none
        
        let flowlayout = CenterFlowLayout().apply {
            //$0.itemSize = .init(width: 300, height: 145)
            //$0.sectionInset = .init(top: 0, left: 10, bottom: 0, right: 10)
            $0.itemSize = CGSize(width: 350, height: 240)
            $0.minimumLineSpacing = 10
            $0.scrollDirection = .horizontal
        }
        
        collectionView = UICollectionView(frame: contentView.bounds, collectionViewLayout: flowlayout).apply {
            addSubview($0)
            $0.backgroundColor = .transparent
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(12)
                maker.left.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.right.equalToSuperview()
                maker.height.equalTo(240)
            }
            $0.isUserInteractionEnabled = true
            $0.delegate = self
            $0.dataSource = self
//            $0.isPagingEnabled = true
            $0.showsHorizontalScrollIndicator = false
            $0.register(cellWithClass: OnairTopNewsCollectionCell.self)
        }
        pageControl = UIPageControl().apply {
            addSubview($0)
            $0.backgroundColor = .transparent
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(collectionView.snp.bottom).offset(6)
                maker.centerX.equalToSuperview()
                maker.bottom.equalToSuperview()
            }
            $0.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            $0.pageIndicatorTintColor = .darkGray
            $0.currentPageIndicatorTintColor = .lightGray
        }
        startTimer()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValue(_ list: [PostInfoModel.Info]){
        asiaDataList = list
        pageControl.numberOfPages = list.count
        collectionView.reloadData()
    }
    
    private func startTimer() {
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(nextPage), userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        timer?.invalidate()
    }
    
    @objc func nextPage(){
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
        if let cell = collectionView.visibleCells.min(by: { abs(visibleRect.center.x - $0.center.x) < abs(visibleRect.center.x - $1.center.x) }) {
            let index = collectionView.indexPath(for: cell)?.row ?? 0
            collectionView.scrollToItem(at: IndexPath.init(item: index + 1, section: 0), at: .centeredHorizontally, animated: true)
            pageControl.currentPage = (index + 1) % asiaDataList.count
        }
    }
}

extension OnairTopNewsTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return asiaDataList.count * (10000 * asiaDataList.count)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: OnairTopNewsCollectionCell.self, for: indexPath)
        cell.setValue(model: asiaDataList[indexPath.row % asiaDataList.count])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.url = asiaDataList[indexPath.row % asiaDataList.count].guid ?? ""
            if RadioPlayer.shared.nowChannel == .FLY {
                next.setTitleLabel(title: "飛揚大頭條")
            } else if RadioPlayer.shared.nowChannel == .APAC {
                next.setTitleLabel(title: "亞太大頭條")
            } else if RadioPlayer.shared.nowChannel == .ASIA {
                next.setTitleLabel(title: "亞洲大頭條")
            }
            Self.getLastPresentedViewController()?.present(next, animated: true)
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
        if let cell = collectionView.visibleCells.min(by: { abs(visibleRect.center.x - $0.center.x) < abs(visibleRect.center.x - $1.center.x) }) {
            let index = collectionView.indexPath(for: cell)?.row ?? 0
            log(TAG, "index \(index)")
            pageControl.currentPage = index % asiaDataList.count
        }
    }
}

//extension OnairTopNewsTableCell: FSPagerViewDelegate, FSPagerViewDataSource {
//    func numberOfItems(in pagerView: FSPagerView) -> Int {
//        return asiaDataList.count
//    }
//
//    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
//        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "newsCollectionView", at: index)
//        cell.setValue(model: asiaDataList[index])
//        return cell
//    }
//
//    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
////        onClickItem?(indexPath, flyDataList[indexPath.row])
//        if RadioPlayer.shared.nowChannel == .FLY {
//            let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
//            if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
//                next.modalPresentationStyle = .fullScreen
//                next.url = asiaDataList[index].guid ?? ""
//                next.setTitleLabel(title: "飛揚大頭條")
//                let controller = UIViewController.getLastPresentedViewController()
//                controller?.present(next, animated: true)
//            }
//        }else if RadioPlayer.shared.nowChannel == .APAC {
//            //onClickAsiaItem?(indexPath, asiaDataList[indexPath.row])
//            let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
//            if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
//                next.modalPresentationStyle = .fullScreen
//                next.url = asiaDataList[index].guid ?? ""
//                next.setTitleLabel(title: "亞太大頭條")
//                let controller = UIViewController.getLastPresentedViewController()
//                controller?.present(next, animated: true)
//            }
//        }else {
//            let storyboard = UIStoryboard.init(name: "Website", bundle: nil)
//            if let next = storyboard.instantiateViewController(withClass: WebSiteViewController.self) {
//                next.modalPresentationStyle = .fullScreen
//                next.url = asiaDataList[index].guid ?? ""
//                next.setTitleLabel(title: "亞洲大頭條")
//                let controller = UIViewController.getLastPresentedViewController()
//                controller?.present(next, animated: true)
//            }
//        }
//    }
//
//}
