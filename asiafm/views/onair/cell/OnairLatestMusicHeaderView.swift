//
//  OnairLatestMusicHeaderView.swift
//  asiafm
//
//  Created by Aki Wang on 2021/8/29.
//

import UIKit

class OnairLatestMusicHeaderView: UITableViewHeaderFooterView {
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        UILabel().apply {
            self.contentView.addSubview($0)
            $0.text = "最近播放"
            $0.font = $0.font.withSize(12)
            $0.textColor = .txt_subtitle
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(24)
                maker.bottom.equalToSuperview().offset(-4)
                maker.centerX.equalToSuperview()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
