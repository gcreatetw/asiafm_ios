//
//  OnairHistoryTableCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/23.
//

import UIKit

class OnairLatestMusicTableCell: UITableViewCell {
    var collectionView: UICollectionView!
    var dataList = [ChannelInfoModel.LatestMusic]()
    var onClickItem: ((IndexPath, ChannelInfoModel.LatestMusic) -> ())?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .transparent
        selectionStyle = .none
        
        let flowlayout = UICollectionViewFlowLayout().apply {
            $0.itemSize = .init(width: 80, height: 80)
            $0.sectionInset = .init(top: 0, left: 4, bottom: 0, right: 4)
            $0.minimumLineSpacing = 4
            $0.scrollDirection = .horizontal
        }
        collectionView = UICollectionView(frame: contentView.bounds, collectionViewLayout: flowlayout).apply {
            addSubview($0)
            $0.backgroundColor = .transparent
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(12)
                maker.left.equalToSuperview()
                maker.right.equalToSuperview()
                maker.height.equalTo(80)
            }
            
            $0.isUserInteractionEnabled = true
            $0.delegate = self
            $0.dataSource = self
            $0.showsHorizontalScrollIndicator = false
            $0.register(cellWithClass: OnairLatestMusicCollectionCell.self)
        }
        UIView().apply {
            addSubview($0)
            $0.backgroundColor = .divider
            $0.snp.makeConstraints { maker in
                maker.height.equalTo(2)
                maker.width.equalTo(50)
                maker.top.equalTo(collectionView.snp.bottom).offset(12)
                maker.centerX.equalToSuperview()
                maker.bottom.equalToSuperview()
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setValue(_ list: [ChannelInfoModel.LatestMusic]){
        dataList = list
        collectionView.reloadData()
    }
}

extension OnairLatestMusicTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: OnairLatestMusicCollectionCell.self, for: indexPath)
        cell.setValue(model: dataList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            let singer = dataList[indexPath.row].artist_name
            let songName = dataList[indexPath.row].song_name
            next.modalPresentationStyle = .fullScreen
            next.doubleUrl = "https://www.youtube.com/results?search_query=\(singer ?? "")+\(songName ?? "")"
            next.setTitleLabel(title: "搜尋結果")
            Self.getLastPresentedViewController()?.present(next, animated: true)
        }
    }
    
}
