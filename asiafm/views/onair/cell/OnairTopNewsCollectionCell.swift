//
//  OnairTopNewsCollectionCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/23.
//  大頭條

import UIKit
import SnapKit

class OnairTopNewsCollectionCell: UICollectionViewCell {
    var imageView: UIImageView!
    var introLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView = UIImageView().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                //maker.bottom.equalToSuperview()
                maker.left.equalToSuperview()
                maker.right.equalToSuperview()
                maker.height.equalTo(180)
            }
            $0.clipsToBounds = true
            $0.layer.cornerRadius = 10
        }
        introLabel = UILabel().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(imageView.snp.bottom).offset(-5)
                maker.left.equalToSuperview().offset(4)
                maker.right.equalToSuperview().offset(-4)
                maker.bottom.equalToSuperview()
            }
            $0.textAlignment = .center
            $0.numberOfLines = 1
            $0.font = $0.font.withSize(18)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setValue(model: PostInfoModel.Info){
        if let img = model.imgUrl, let url = URL(string: img.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) {
            imageView.kf.setImage(with: url)
            imageView.contentMode = .scaleAspectFill
        } else {
            switch RadioPlayer.shared.nowChannel {
            case .ASIA:
                imageView.image = UIImage(named: "正在播放_927")
            case .APAC:
                imageView.image = UIImage(named: "正在播放_923")
            case .FLY:
                imageView.image = UIImage(named: "正在播放_895")
            default:
                imageView.image = UIImage(named: "正在播放_927")
            }
            imageView.contentMode = .scaleAspectFit
            imageView.backgroundColor = .white
        }
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        
        introLabel.text = model.postTitle
    }
    
//    var testImageView: UIImageView = {
//        let iv = UIImageView()
//        iv.snp.makeConstraints { maker in
//            maker.top.equalToSuperview()
//            maker.bottom.equalToSuperview()
//            maker.left.equalToSuperview()
//            maker.right.equalToSuperview()
//        }
//
//        iv.layer.cornerRadius = 12
//        return iv
//    }()
}

