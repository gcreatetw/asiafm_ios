//
//  OnairLIfeCollectionCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/23.
//  生活情報讚

import UIKit
import SnapKit

class OnairLifeCollectionCell: UICollectionViewCell {
    var imageView: UIImageView!
    var label: UILabel!
    var dateLabel: UILabel!
    var lifeDateString: String = ""
    
    func setLifeLabelFormatter() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM--dd"
        formatter.locale = Locale(identifier: "zh_TW")
        lifeDateString = formatter.string(from: Date())
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setLifeLabelFormatter()
        
        imageView = UIImageView().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                maker.left.equalToSuperview()
                maker.right.equalToSuperview()
                maker.height.equalTo(150)
            }
            $0.clipsToBounds = true
            $0.cornerRadius = 6
        }
        label = UILabel().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(imageView.snp.bottom).offset(2)
                maker.left.equalToSuperview().offset(4)
                maker.right.equalToSuperview().offset(-4)
                maker.bottom.equalToSuperview()
            }
            $0.numberOfLines = 2
            $0.font = $0.font.withSize(12)
        }
        dateLabel = UILabel().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(imageView.snp.bottom).offset(45)
                maker.left.equalToSuperview().offset(4)
                maker.right.equalToSuperview().offset(-4)
                maker.bottom.equalToSuperview()
            }
            $0.numberOfLines = 1
            $0.font = $0.font.withSize(12)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setValue(model: PostInfoModel.Info){
        if let img = model.imgUrl, let url = URL(string: img.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) {
            imageView.kf.setImage(with: url)
            imageView.contentMode = .scaleAspectFill
        } else {
            switch RadioPlayer.shared.nowChannel {
            case .ASIA:
                imageView.image = UIImage(named: "正在播放_927")
            case .APAC:
                imageView.image = UIImage(named: "正在播放_923")
            case .FLY:
                imageView.image = UIImage(named: "正在播放_895")
            default:
                imageView.image = UIImage(named: "正在播放_927")
            }
            imageView.contentMode = .scaleAspectFit
            imageView.backgroundColor = .white
        }

        imageView.layer.cornerRadius = 6
        label.text = model.postTitle
        
        lifeDateString = model.postDate ?? ""
        let nsString = lifeDateString as NSString
        if nsString.length >= 6 {
            dateLabel.text = nsString.substring(with: NSRange(location: 0, length: nsString.length > 10 ? 10 : nsString.length))
        }
        
        //dateLabel.text = model.postDate
        //        imageView.kf.setImage(with: URL(string: model.imgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        //        imageView.contentMode = .scaleAspectFill
    }
}
