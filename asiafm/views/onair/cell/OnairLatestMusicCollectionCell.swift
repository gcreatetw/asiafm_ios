//
//  OnairHistoaryCollectionCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/23.
//

import UIKit
import SnapKit

class OnairLatestMusicCollectionCell: UICollectionViewCell {
    var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView = UIImageView().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.left.equalToSuperview()
                maker.right.equalToSuperview()
            }
            
            $0.clipsToBounds = true
            $0.cornerRadius = 6
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setValue(model: ChannelInfoModel.LatestMusic){
        //imageView.backgroundColor = .blue
        if model.picture_url != nil {
            if let url = URL(string: model.picture_url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) {
                imageView.kf.setImage(with: url)
            }
        }else {
            imageView.image = UIImage(named: "image_cover_recently_played")
        }
        
        imageView.contentMode = .scaleAspectFill
    }
    
    func setFlyValue(model: FlyradioChannelInfoModel.LatestMusic){
        if model.picture_url != nil {
            if let url = URL(string: model.picture_url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)) {
                imageView.kf.setImage(with: url)
            }
        }else {
            imageView.image = UIImage(named: "image_cover_recently_played")
        }
        
        imageView.contentMode = .scaleAspectFill
    }
}
