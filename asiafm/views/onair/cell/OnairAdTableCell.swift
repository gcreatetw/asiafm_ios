//
//  OnairAdTableCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/24.
//

import UIKit

class OnairAdTableCell: UITableViewCell {
    var imgView: UIImageView!
    
    var onClickItem: (() -> ())?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .transparent
        selectionStyle = .none
        
        imgView = UIImageView().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.centerX.equalToSuperview()
                maker.width.equalTo(320)
                maker.height.equalTo(80)
            }
            $0.backgroundColor = .blue
            $0.image = UIImage(named: "廣告_1(320x80)")
        }
        
        UILabel().apply {
            addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.center.equalToSuperview()
            }
            $0.font = $0.font.withSize(36).bold
            $0.textColor = .txt_white
            $0.text = "AD"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        onClickItem?()
    }

    func setValue(ad: String){
        
    }
}
