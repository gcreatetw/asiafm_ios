//
//  OnairLifeTableCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/23.
//  生活情報讚

import UIKit

class OnairLifeTableCell: UITableViewCell {
    var collectionView: UICollectionView!
    var dataList = [PostInfoModel.Info]()
    var onClickItem: ((IndexPath, PostInfoModel.Info) -> ())?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .transparent
        selectionStyle = .none
        
        let flowlayout = UICollectionViewFlowLayout().apply {
            $0.itemSize = .init(width: 300, height: 210)
            $0.sectionInset = .init(top: 0, left: 28, bottom: 0, right: 8)
            $0.minimumLineSpacing = 28
            $0.scrollDirection = .horizontal
        }
        collectionView = UICollectionView(frame: contentView.bounds, collectionViewLayout: flowlayout).apply {
            addSubview($0)
            $0.backgroundColor = .transparent
            $0.snp.makeConstraints { maker in
                maker.top.equalToSuperview().offset(12)
                //maker.left.equalTo(20)
                maker.left.equalToSuperview()
                maker.right.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.height.equalTo(210)
            }
            
            $0.isUserInteractionEnabled = true
            $0.delegate = self
            $0.dataSource = self
            $0.showsHorizontalScrollIndicator = false
            $0.register(cellWithClass: OnairLifeCollectionCell.self)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setValue(_ list: [PostInfoModel.Info]){
        dataList = list
        collectionView.reloadData()
    }
}

extension OnairLifeTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: OnairLifeCollectionCell.self, for: indexPath)
        cell.setValue(model: dataList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.url = dataList[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "生活情報讚")
            Self.getLastPresentedViewController()?.present(next, animated: true)
        }
    }
}
