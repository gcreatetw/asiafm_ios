//
//  OnairCollectionCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/21.
//

import UIKit
import SnapKit

class OnairCollectionCell: UICollectionViewCell {
    var label: UILabel!
    var bg: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.snp.makeConstraints { maker in
            maker.width.equalTo(screenWidth).priority(.high)
//            maker.height.equalTo(100).priority(.high)
        }
        bg = UIImageView()
        addSubview(bg)
        bg.backgroundColor = .gray
        bg.image = UIImage(named: "logo_895")
        bg.contentMode = .scaleAspectFill
        bg.snp.remakeConstraints { maker in
//            maker.width.equalTo(screenWidth)
//            maker.height.equalTo(100)
            maker.top.equalToSuperview().offset(5)
            maker.bottom.equalToSuperview().offset(-5)
            maker.left.equalToSuperview().offset(5)
            maker.right.equalToSuperview().offset(-5)
        }
        label = UILabel()
        addSubview(label)
        label.snp.makeConstraints { maker in
            maker.center.equalToSuperview()
        }
        label.text = "label \(frame.width) \(frame.height)"
        
        self.backgroundColor = .yellow
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setValue(index: IndexPath){
//        bg.snp.remakeConstraints { maker in
//            maker.width.equalTo(screenWidth).priority(.high)
//            maker.height.equalTo(150).priority(.high)
//        }
        label.text = "label \(index) \(frame.width) \(frame.height)"
    }
}
