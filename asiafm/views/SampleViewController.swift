//
//  SampleViewController.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/5.
//

import UIKit

class SampleViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initData()
        initView()
    }
    
    private func initData(){
        
    }
    
    private func initView(){
        
    }

}
