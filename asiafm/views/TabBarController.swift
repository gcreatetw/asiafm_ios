//
//  TabBarController.swift
//  SampleApp
//
//  Created by Aki Wang on 2020/10/22.
//

import UIKit
import FRadioPlayer

var tabId: Int = 0
class TabBarController: UITabBarController {
    private let TAG = "TabBarController"

    private var flyMusicList: [FlyradioChannelInfoModel.LatestMusic] = []
    private var latestMusicList: [ChannelInfoModel.LatestMusic] = []
    var tabRadioIsOn = Bool()
    var tabAirTimer: Timer?
    var tabFlyTimer: Timer?
    
    var asiaFmTitle: String = ""
    var asiaFmName: String = ""
    var flyFmTitle: String = ""
    var flyFmName: String = ""
    var imageSongCover = UIImageView()
    var lbSongName = UILabel()
    var lbSingerName = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
           // Always adopt a light interface style.
           overrideUserInterfaceStyle = .light
        }
        initTab()
        initPlayerView()
        
        tabRadioIsOn = ud.getRadioIsOn()
        
        if tabRadioIsOn == true {
            RadioPlayer.shared.play()
            RadioPlayer.shared.player.play()
        }else {
            RadioPlayer.shared.stop()
            RadioPlayer.shared.player.stop()
        }
        
        let vPlayerHeight = 60.0
        let vPlayer = UIView().apply {
            view.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.bottom.equalTo(tabBar.snp.top)
                maker.width.equalToSuperview()
                maker.height.equalTo(vPlayerHeight)
            }
            $0.backgroundColor = .def_bottom_player_bg
        }
         imageSongCover = UIImageView().apply {
            vPlayer.addSubview($0)
            $0.snp.makeConstraints { maker in
                let width = vPlayerHeight * 0.8
                maker.top.equalToSuperview().offset((vPlayerHeight - width) / 2)
                maker.left.equalToSuperview().offset(24)
                maker.width.equalTo(width)
                maker.height.equalTo(width)
            }
            $0.cornerRadius = 4
            $0.clipsToBounds = true
            $0.image = UIImage(named: "userphoto")
            $0.contentMode = .scaleToFill
            $0.borderWidth = 1
            $0.borderColor = .gray
        }
        // 播放暫停圖 @tapPalyOrPause
        playOrPauseImageView = UIImageView().apply {
            vPlayer.addSubview($0)
            $0.snp.makeConstraints { maker in
                let width = vPlayerHeight * 0.8
                maker.top.equalToSuperview().offset((vPlayerHeight - width) / 2)
                maker.right.equalToSuperview().offset(-24)
                maker.width.equalTo(width)
                maker.height.equalTo(width)
            }
            if tabRadioIsOn == true {
                $0.image = UIImage(named: "icon_tabbar_pause")
            }else {
                $0.image = UIImage(named: "icon_tabbar_play")
            }
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPalyOrPause(_ :)))
            $0.isUserInteractionEnabled = true
            $0.addGestureRecognizer(tapGestureRecognizer)
        }
        // 歌曲名稱
        lbSongName = UILabel().apply {
            vPlayer.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(imageSongCover.snp.top).offset(4)
                maker.left.equalTo(imageSongCover.snp.right).offset(12)
                maker.right.equalTo(imageSongCover.snp.right).offset(230)
            }
            $0.text = "歌曲名稱"
            $0.textColor = .txt_black
            $0.font = $0.font.withSize(14).bold
        }
        // 歌手名稱
        lbSingerName = UILabel().apply {
            vPlayer.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(lbSongName.snp.bottom).offset(4)
                maker.left.equalTo(lbSongName.snp.left)
                maker.right.equalTo(lbSongName.snp.left).offset(200)
            }
            $0.text = "歌手名稱"
            $0.textColor = .txt_black
            $0.font = $0.font.withSize(12)
        }
        changeLatestMusicList()
    }
    
    private func initTab(){
        // 設定 item
        var arr = [UIViewController]()
        
        if let vc = UIStoryboard(name: "Onair", bundle:nil).instantiateViewController(withClass: OnairViewController.self) {
            vc.tabBarItem =
                UITabBarItem(title: "線上收聽", image: UIImage(named: "icon_Onair"), selectedImage: UIImage(named: "icon_Onair"))
            arr.append(vc)
        }
        
        if let vc = UIStoryboard(name: "Rundown", bundle:nil).instantiateViewController(withClass: RundownViewController.self) {
            vc.tabBarItem =
                UITabBarItem(title: "節目表", image: UIImage(named: "icon_showlist"), selectedImage: UIImage(named: "icon_showlist"))
            arr.append(vc)
        }
        
        if let vc = UIStoryboard(name: "Search", bundle:nil).instantiateViewController(withClass: SearchViewController.self) {
            vc.tabBarItem =
                UITabBarItem(title: "尋歌專區", image: UIImage(named: "icon_searchsong"),selectedImage: UIImage(named: "icon_searchsong"))
            arr.append(vc)
        }
        
//        if let vc = UIStoryboard(name: "Podcast", bundle:nil).instantiateViewController(withClass: PodcastViewController.self) {
//            vc.tabBarItem =
//                UITabBarItem(title: "Podcast", image: UIImage(named: "icon_podcast"),selectedImage: UIImage(named: "icon_podcast"))
//            arr.append(vc)
//        }
        
        if let vc = UIStoryboard(name: "VideoArea", bundle:nil).instantiateViewController(withClass: VideoAreaViewController.self) {
            vc.tabBarItem =
                UITabBarItem(title: "影音專區", image: UIImage(named: "icon_youtube"),selectedImage: UIImage(named: "icon_youtube"))
            arr.append(vc)
        }
        
        // set view contriller
        self.viewControllers = arr
        // setting tab ber style
        //去除 tab bar 上方黑線
        tabBar.layer.borderWidth = 0
        tabBar.clipsToBounds = true
        //加入 tab bar 上方 divider
        let border = CALayer()
        border.backgroundColor = UIColor.divider.cgColor
        border.frame = CGRect(x: 0, y: 0, width: tabBar.frame.width, height: 0.5)
        tabBar.layer.addSublayer(border)
        //設定selected and unselected icon 顏色
        tabBar.tintColor = .def_tab_selected
        tabBar.unselectedItemTintColor = .def_tab_unselected
        tabBar.barTintColor = .def_tab_bg_unselected
        tabBar.backgroundColor = .white
        //
        chingeTabItemBackground(tabBar, selectedIndex, .def_tab_bg_selected)
    }

    private func chingeTabItemBackground(_ tabBar: UITabBar, _ tabIndex: Int, _ backgroundColor: UIColor) {
//        tabBar.subviews.filter({ $0.layer.name == "bgView" }).first?.removeFromSuperview()
//        tabBar.subviews.filter({ $0.layer.name == "topLineView" }).first?.removeFromSuperview()
//        if let items = tabBar.items {
//            let tabIndex = CGFloat(tabIndex)
//            let tabWidth = tabBar.bounds.width / CGFloat(items.count)
//            //
//            let bgView = UIView(frame: CGRect(x: tabWidth * tabIndex, y: 0, width: tabWidth, height: 1000))
//            bgView.backgroundColor = backgroundColor
//            bgView.layer.name = "bgView"
//            tabBar.insertSubview(bgView, at: 0)
//            //
//            let border = CALayer()
//            border.backgroundColor = UIColor.def_tab_selected.cgColor
//            border.frame = CGRect(x: 0, y: 0, width: bgView.frame.width, height: 3)
//            bgView.layer.addSublayer(border)
//        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        log(TAG, "\(item.title)")
        chingeTabItemBackground(tabBar, tabBar.items?.firstIndex(of: item) ?? 0, .def_tab_bg_selected)
        
        tabId = tabBar.items?.firstIndex(of: item) ?? 0
        
    }

    var playOrPauseImageView: UIImageView!
    private func initPlayerView() {
        
    }
    let player = FRadioPlayer.shared
    //(好像)播放中點擊按鈕會變成『暫停鈕』，音樂停止播放
    @objc func tapPalyOrPause(_ sender : UITapGestureRecognizer) {
        if playOrPauseImageView.image == UIImage(named: "icon_tabbar_pause") {
           // RadioPlayer.shared.player.stop()
            RadioPlayer.shared.stop()
            self.tabAirTimer?.invalidate()
            self.tabFlyTimer?.invalidate()
            playOrPauseImageView.image = UIImage(named: "icon_tabbar_play")
            player.isAutoPlay = false
            print("TAB ====== STOP")
        } else if playOrPauseImageView.image == UIImage(named: "icon_tabbar_play") {
           // RadioPlayer.shared.player.play()
            RadioPlayer.shared.play()
            playOrPauseImageView.image = UIImage(named: "icon_tabbar_pause")
        }
    }
    
    private func changeLatestMusicList(_ latestMusicList: [ChannelInfoModel.LatestMusic] = RadioPlayer.shared.latestMusicList, _ channel: RadioChannel = RadioPlayer.shared.nowChannel){
        guard let latestMusic = latestMusicList.first else {
            return
        }
        if latestMusic.picture_url != nil {
            imageSongCover.kf.setImage(with:URL(string:  latestMusic.picture_url ?? "".addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed) ?? ""))
        }else {
            imageSongCover.image = UIImage(named: "image_cover_recently_played")
        }
        self.lbSongName.text = latestMusic.song_name
        self.lbSingerName.text = latestMusic.artist_name
    }
}

//與專輯Play按鈕連動
extension TabBarController: RadioPlayerDelegate {
    func latestMusicListDidChange(_ latestMusicList: [ChannelInfoModel.LatestMusic], _ channel: RadioChannel) {
        changeLatestMusicList(latestMusicList, channel)
    }
    
    func playerChannelDidChange(_ playerState: FRadioPlayerState, _ channel: RadioChannel) {
//        log(TAG, "playerChannelDidChange \(playerState.description) \(channel.title)")
    }
    
    func playbackChannelDidChange(_ playbackState: FRadioPlaybackState, _ channel: RadioChannel) {
//        log(TAG, "playbackChannelDidChange \(playbackState.description) \(channel.title)")
        if playbackState == .playing {
            playOrPauseImageView.image = UIImage(named: "icon_tabbar_pause")
        }
        else {
            playOrPauseImageView.image = UIImage(named: "icon_tabbar_play")
            self.tabAirTimer?.invalidate()
            self.tabFlyTimer?.invalidate()
        }
    }
}
