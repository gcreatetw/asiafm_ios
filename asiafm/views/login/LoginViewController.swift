//
//  LoginViewController.swift
//  SampleApp
//
//  Created by Aki Wang on 2020/10/22.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneTextField {
            phoneLabel.textColor = UIColor.red
            passwordLabel.textColor = UIColor(named: "txt_black")
        }
        if textField == passwordTextField {
            phoneLabel.textColor = UIColor(named: "txt_black")
            passwordLabel.textColor = UIColor.red
        }
    }
    
    @IBAction func forgotPasswordButtnAction(_ sender: Any) {
        if let next = UIStoryboard.Login.instantiateViewController(withClass: ForgotPasswordViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
            self.present(next, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .new_apac_bg
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap)
        
        phoneTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    @IBAction func backWellcomeView(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func onClickLogin() {
        let username = phoneTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        if username.count != 10 || username.hasPrefix("09") != true{
            showTextAlert(message: "手機號碼格式錯誤，請重新輸入。")
            return
        }
        if password.count < 4 {
            showTextAlert(message: "密碼格式錯誤")
            return
        }
        syncLogin(username, password)
    }
    
    private var progressAlert: UIAlertController?
    private func showProgressAlert(){
        if progressAlert != nil {
            dismissProgressAlert()
        }
        progressAlert = UIAlertController(title: "Please Waiting...", message: "  ", preferredStyle: .alert)
        if let progressAlert = progressAlert {
            let progressBar = UIProgressView(progressViewStyle: .default)
            progressBar.setProgress(0.0, animated: true)
            progressBar.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
            progressAlert.view.addSubview(progressBar)
            self.present(progressAlert, animated: true, completion: nil)
            
            // Do the time critical stuff asynchronously
            DispatchQueue.init(label: "progressAlert").async {
                var progress = 0
                while self.progressAlert != nil {
                    progress += 1
                    Thread.sleep(forTimeInterval: 0.01)
                    DispatchQueue.main.async {
                        progressBar.setProgress((progress % 100).toFloat() / 100.toFloat(), animated: false)
                    }
                }
            }
        }
    }
    
    private func dismissProgressAlert(_ closure:@escaping () -> () = {}) {
        DispatchQueue.main.async {
            self.progressAlert?.dismiss(animated: false){
                self.progressAlert = nil
                closure()
            }
        }
    }

    private func syncLogin(_ username: String, _ password: String){
        showProgressAlert()
        DispatchQueue.init(label: "api").async {
            cleanData()
            let response = ApiService.login(username: username, password: password)
            if let data = response.data?.data {
                // login sucess
                ud.setLogin(isLogin: true)
                ud.setUserAccount(username: username)
                ud.setPassword(password: password)
                ud.setLoginData(data: data)
                
                self.dismissProgressAlert() {
                    if let next = UIStoryboard.TabBar.instantiateViewController(withClass: TabBarController.self) {
                        next.modalPresentationStyle = .fullScreen
                        self.present(next, animated: true)
                    }
                }
            } else {
                self.dismissProgressAlert() {
                    self.showTextAlert(message: "密碼輸入有誤\n請重新輸入")
                }
            }
        }
    }
    
    private func showTextAlert(message: String, closure:(() -> Void)? = nil){
        let alertVc = GuestLoginViewController.loadFromNib()
    
        alertVc.guestAlertPop = {
            self.dismiss(animated: true, completion: .none)
        }
        alertVc.contentText = message
        self.present(alertVc, animated: true, completion: nil)
    }
}

