//
//  RegisteViewController.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/5.
//

import UIKit
import Firebase

class RegisteViewController: BaseViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var verifyTextField: UITextField!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var verifyLabel: UILabel!
    
    private var verificationID: String?
    var fromCheckPage: Bool = false
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneTextField {
            phoneLabel.textColor = UIColor.red
            passwordLabel.textColor = UIColor(named: "txt_black")
            verifyLabel.textColor = UIColor(named: "txt_black")
        }
        if textField == passwordTextField {
            phoneLabel.textColor = UIColor(named: "txt_black")
            passwordLabel.textColor = UIColor.red
            verifyLabel.textColor = UIColor(named: "txt_black")
        }
        if textField == verifyTextField {
            phoneLabel.textColor = UIColor(named: "txt_black")
            passwordLabel.textColor = UIColor(named: "txt_black")
            verifyLabel.textColor = UIColor.red
        }
    }
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .new_apac_bg
        // Do any additional setup after loading the view.
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        self.view.addGestureRecognizer(tap)
        
        phoneTextField.delegate = self
        passwordTextField.delegate = self
        verifyTextField.delegate = self
    }
    
    @IBAction func backWellcomeView(_ sender: UIButton) {
        if fromCheckPage == false {
            dismiss(animated: true)
        }else {
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        fromCheckPage = false
    }

    @IBAction func onClickRegiste(){
        let username = phoneTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let verificationCode = verifyTextField.text ?? ""
        if username.count != 10 {
            showTextAlert(message: "手機號碼格式錯誤，請重新輸入。")
            return
        }
        if password.count < 4 {
            showTextAlert(message: "密碼格式錯誤")
            return
        }
        guard let verificationID = verificationID else {
            showTextAlert(message: "請先送出驗證碼")
            return
        }
        if verificationCode.count != 6 {
            showTextAlert(message: "驗證碼格式錯誤")
            return
        }
        checkVerifyCode(verificationID, verificationCode) {
            self.syncLogin(username, password)
        }
    }
    
    @IBAction func sendVerifyCode(_ sender: Any) {
        if let phone = phoneTextField.text, phone.count == 10 {
            verifyPhone(phone) {
                self.showVerifyVc()
            }
        } else {
            showTextAlert(message: "手機號碼格式錯誤，請重新輸入。")
        }
    }
    
    private func verifyPhone(_ phone: String, completion: (() -> Void)? = nil) {
        PhoneAuthProvider.provider().verifyPhoneNumber("+886" + phone, uiDelegate: nil) { verificationID, error in
            if let error = error {
                // 發送驗證碼失敗
                log(self.TAG, error.localizedDescription)
                self.showTextAlert(message: error.localizedDescription)
                return
            }
            self.verificationID = verificationID
            completion?()
        }
    }
    
    private func checkVerifyCode(_ verificationID: String, _ verificationCode: String, completion: (() -> Void)? = nil){
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode
        )
        
        // 送出驗證
        Auth.auth().signIn(with: credential) { authResult, error in
            if let error = error {
                // 驗證失敗
                log(self.TAG, error.localizedDescription)
                self.showTextAlert(message: error.localizedDescription)
                return
            }
            // 驗證成功
//            log(self.TAG, "user \(authResult?.user)")
            completion?()
        }
    }
    
    private func showVerifyVc(){
        let editVc = CodeHadSendViewController.loadFromNib()
        
        editVc.dismissSuccessed = {
            self.dismiss(animated: true)
        }
        self.present(editVc, animated: true, completion: nil)
    }
    
    private var progressAlert: UIAlertController?
    private func showProgressAlert(){
        if progressAlert != nil {
            dismissProgressAlert()
        }
        progressAlert = UIAlertController(title: "Please Waiting...", message: "  ", preferredStyle: .alert)
        if let progressAlert = progressAlert {
            let progressBar = UIProgressView(progressViewStyle: .default)
            progressBar.setProgress(0.0, animated: true)
            progressBar.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
            progressAlert.view.addSubview(progressBar)
            self.present(progressAlert, animated: true, completion: nil)
            
            // Do the time critical stuff asynchronously
            DispatchQueue.init(label: "progressAlert").async {
                var progress = 0
                while self.progressAlert != nil {
                    progress += 1
                    Thread.sleep(forTimeInterval: 0.01)
                    DispatchQueue.main.async {
                        progressBar.setProgress((progress % 100).toFloat() / 100.toFloat(), animated: false)
                    }
                }
            }
        }
    }
    
    private func dismissProgressAlert(_ closure:@escaping () -> () = {}){
        DispatchQueue.main.async {
            self.progressAlert?.dismiss(animated: false){
                self.progressAlert = nil
                closure()
            }
        }
    }

    private func syncLogin(_ username: String, _ password: String){
        showProgressAlert()
        DispatchQueue.init(label: "api").async {
            cleanData()
            let response = ApiService.register(username: username, password: password)
            if let data = response.data?.data {
                // login sucess
                ud.setLogin(isLogin: true)
                ud.setUserAccount(username: username)
                ud.setPassword(password: password)
                ud.setLoginData(data: data)
                
                self.dismissProgressAlert() {
                    let storyboard = UIStoryboard.init(name: "Tab", bundle: nil)
                    if let next = storyboard.instantiateViewController(withClass: TabBarController.self) {
                        next.modalPresentationStyle = .fullScreen
                        self.present(next, animated: true)
                    }
                }
            } else {
                self.dismissProgressAlert() {
                    self.showTextAlert(message: response.data?.message ?? "")
                }
            }
        }
    }
    
    private func showTextAlert(message: String, closure:(() -> Void)? = nil){
        let alertVc = AlertTextViewController.loadFromNib()
        //
        alertVc.messageString = message
        alertVc.onViewDidLoad = {
            alertVc.btnLeft.isHidden = true
        }
        alertVc.onClickRight = {
            alertVc.dismiss(animated: true, completion: {
                closure?()
            })
        }
        alertVc.onClickLeft = {
            alertVc.dismiss(animated: true, completion: nil)
        }
        self.present(alertVc, animated: true, completion: nil)
    }
}
