//
//  CodeHadSendViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/23.
//

import UIKit

class CodeHadSendViewController: UIViewController {

    var msg: String = "驗證碼已發送"
    var dismissSuccessed: (() -> ())?
    
    @IBOutlet weak var msgLabel: UILabel!
    @IBAction func dismissSuccessPop(_ sender: Any) {
        self.dismissSuccessed?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        msgLabel.text = msg
    }
}
