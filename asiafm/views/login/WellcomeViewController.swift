//
//  WellcomeViewController.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/5.
//

import UIKit

var allowToSetting: Bool = true
class WellcomeViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .new_apac_bg
    }
    
    func setStoryBoard(vc: UIViewController) {
        vc.modalPresentationStyle = .fullScreen
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
        self.present(vc, animated: true)
    }
    
    @IBAction func onClickRegiste() {
        guard let next = UIStoryboard.Login.instantiateViewController(withClass: RegisteViewController.self) else { return }
        setStoryBoard(vc: next)
    }
    
    @IBAction func onClickLogin() {
        guard let next = UIStoryboard.Login.instantiateViewController(withClass: LoginViewController.self) else { return }
        setStoryBoard(vc: next)
    }
    
    @IBAction func onClickGuestLogin(){
        guard let next = UIStoryboard.TabBar.instantiateViewController(withClass: TabBarController.self) else { return }
        setStoryBoard(vc: next)
        allowToSetting = false
        
    }
}
