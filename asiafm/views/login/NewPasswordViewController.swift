//
//  NewPasswordViewController.swift
//  asiafm
//
//  Created by StevenTang on 2022/2/17.
//

import UIKit
import Firebase

class NewPasswordViewController: BaseViewController {

    var username: String = ""
    var verificationId: String = ""
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var verifyTextField: UITextField!
 
    @IBAction func onClickDismiss(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func updatePasswordAction(_ sender: Any) {
        let newPassword = newPasswordTextField.text ?? ""
        let verifyCode = verifyTextField.text ?? ""
        print("verifyCode === \(verifyCode)")
        
        guard verifyCode.count == 6 else {
            showTextAlert(message: "驗證碼格式錯誤")
            return
        }
        
        if newPassword.count < 4 {
            showTextAlert(message: "密碼格式錯誤")
        }else {
            checkVerifyCode(verificationId, verifyCode)
            syncSetNewPassword(username: username, newPassword: newPassword)
        }
        
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .new_apac_bg
        print("NewPasswordID === \(verificationId)")
    }
    
    private func showVerifyVc(){
        let editVc = CodeHadSendViewController.loadFromNib()
        
        editVc.msg = "密碼已修改成功\n請重新登入"
        editVc.dismissSuccessed = {
            self.dismiss(animated: true) {
                self.presentingViewController?.dismiss(animated: false)
                self.presentingViewController?.dismiss(animated: false)
            }
            
        }
        self.present(editVc, animated: true, completion: nil)
    }
    
    private func showTextAlert(message: String, closure:(() -> Void)? = nil){
        let alertVc = GuestLoginViewController.loadFromNib()
        alertVc.guestAlertPop = {
            self.dismiss(animated: true, completion: .none)
        }
        alertVc.contentText = message
        self.present(alertVc, animated: true, completion: nil)
    }
    
    private func checkVerifyCode(_ verificationID: String, _ verificationCode: String, completion: (() -> Void)? = nil){
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode
        )
        
        // 送出驗證
        Auth.auth().signIn(with: credential) { authResult, error in
            if let error = error {
                // 驗證失敗
                log(self.TAG, error.localizedDescription)
                self.showTextAlert(message: "驗證碼錯誤")
                return
            }
            // 驗證成功
//            log(self.TAG, "user \(authResult?.user)")
            
            completion?()
        }
    }
    
    private func syncSetNewPassword(username: String, newPassword: String) {
        var isSuccess: String = ""
        DispatchQueue.init(label: "newPassword").async {
            let response = ApiService.demoUserPasswordData(event: "nologged", username: username, oldPassword: "", newPassword: newPassword)
            if let data = response.data {
                isSuccess = data.message
            }
            
            DispatchQueue.main.async {
                if isSuccess == "success" {
                    self.showVerifyVc()
                }else {
                    self.showTextAlert(message: "修改失敗")
                }
            }
        }
    }
}
