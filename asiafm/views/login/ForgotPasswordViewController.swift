//
//  ForgotPasswordViewController.swift
//  asiafm
//
//  Created by StevenTang on 2022/2/16.
//

import UIKit
import Firebase

class ForgotPasswordViewController: BaseViewController {
        
    private var verificationID: String?
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBAction func dismissOnClick(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func sendDataAction(_ sender: Any) {
        let number = phoneNumberTextField.text ?? ""
        
        if number.count != 10 || number.hasPrefix("09") != true {
            showTextAlert(message: "手機號碼格式錯誤，請重新輸入。")
        }else {
            syncCheckRegister(number: number)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .new_apac_bg
        phoneNumberTextField.delegate = self
    }
    
    private func syncCheckRegister(number: String) {
        var errorMsg: String = ""
        DispatchQueue.init(label: "api").async {
            let response = ApiService.checkRegister(username: number)
            if let data = response.data {
                errorMsg = data.errors ?? ""
            }
            DispatchQueue.main.async {
                if errorMsg != "忘記密碼但有註冊" {
                    self.showTextAlert(message: "手機尚未註冊\n請重新註冊會員")
                }else {
                    self.verifyPhone(number)
                }
            }
        }
    }
    
    private func showTextAlert(message: String, closure:(() -> Void)? = nil){
        let alertVc = GuestLoginViewController.loadFromNib()
        
        alertVc.guestAlertPop = {
            self.dismiss(animated: true, completion: .none)
            if message == "手機尚未註冊\n請重新註冊會員" {
                if let next = UIStoryboard.Login.instantiateViewController(withClass: RegisteViewController.self) {
                    next.fromCheckPage = true
                    next.modalPresentationStyle = .fullScreen
                    next.hero.isEnabled = true
                    next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                    self.present(next, animated: true)
                }
            }
        }
        alertVc.contentText = message
        self.present(alertVc, animated: true, completion: nil)
    }
    
    private func verifyPhone(_ phone: String, completion: (() -> Void)? = nil) {
        PhoneAuthProvider.provider().verifyPhoneNumber("+886" + phone, uiDelegate: nil) { verificationID, error in
            if let error = error {
                // 發送驗證碼失敗
                log(self.TAG, error.localizedDescription)
                self.showTextAlert(message: error.localizedDescription)
                return
            }
            self.verificationID = verificationID
            self.showVerifyVc(username: phone)
            completion?()
        }
    }
    
    private func showVerifyVc(username: String){
        let editVc = CodeHadSendViewController.loadFromNib()
        
        editVc.dismissSuccessed = {
            self.dismiss(animated: true)
            if let next = UIStoryboard.Login.instantiateViewController(withClass: NewPasswordViewController.self) {
                next.modalPresentationStyle = .fullScreen
                next.username = username
                next.verificationId = self.verificationID ?? ""
                next.hero.isEnabled = true
                next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
                self.present(next, animated: true)
            }
        }
        self.present(editVc, animated: true, completion: nil)
    }
}
