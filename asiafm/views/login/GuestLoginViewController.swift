//
//  GuestLoginViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/9/30.
//

import UIKit

class GuestLoginViewController: UIViewController {

    @IBOutlet weak var alertLabel: UILabel!
    
    var guestAlertPop: (() -> ())?
    var contentText: String = ""
    
    @IBAction func guestAlertPopAction(_ sender: Any) {
        self.guestAlertPop?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertLabel.text = contentText
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
