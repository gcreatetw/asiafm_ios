//
//  EducationViewController.swift
//  asiafm
//
//  Created by StevenTang on 2021/10/7.
//

import UIKit

var eduId: String = ""
class EducationViewController: UIViewController {

    private var eduProgressAlert: UIAlertController?
    
    @IBOutlet weak var educationImageView: UIImageView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    @IBAction func firstViewAction(_ sender: Any) {
        firstButton.isHidden = true
        educationImageView.image = UIImage(named: "home_teaching_page02")
        secondButton.isHidden = false
    }
    
    @IBAction func secondButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        secondButton.isHidden = true
    }

    private func showTextAlert(message: String, closure:(() -> Void)? = nil){
        let alertVc = AlertTextViewController.loadFromNib()
        //
        alertVc.messageString = message
        alertVc.onViewDidLoad = {
            alertVc.btnLeft.isHidden = true
        }
        alertVc.onClickRight = {
            alertVc.dismiss(animated: true, completion: {
                closure?()
            })
        }
        alertVc.onClickLeft = {
            alertVc.dismiss(animated: true, completion: nil)
        }
        self.present(alertVc, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
