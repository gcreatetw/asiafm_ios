//
//  VideoAreaViewController.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/5.
//

import UIKit

class VideoAreaViewController: BaseViewController {

    @IBOutlet weak var videoListTableView: UITableView!
    
    var items = [YoutubeSearchModel.Item]()
    var videoImage = [YoutubeSearchModel.Snippet]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        videoListTableView.delegate = self
        videoListTableView.dataSource = self
        syncApi()
    }

    private func syncApi(){
        DispatchQueue.init(label: "api").async {
            let response = ApiService.getYoutubeSearch()
            if response.isSuccess {
                //log(self.TAG, "items?.count is \(items?.toJsonString())")
                self.items = response.data?.items ?? []
                
                DispatchQueue.main.async {
                    self.videoListTableView.reloadData()
                }
            }
        }
    }
}


extension VideoAreaViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(VideoListTableViewCell.self)", for: indexPath) as! VideoListTableViewCell
     
        if indexPath.row < items.count {
            cell.setValue(items[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /// TODO 開 youtube 網頁
        let id = items[indexPath.row].id?.videoId
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.titleString = "YOUTUBE"
            next.url = "https://www.youtube.com/watch?v=\(id ?? "")"
            self.present(next, animated: true)
        }
    }
    
}

