//
//  VideoListTableViewCell.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/22.
//

import UIKit

class VideoListTableViewCell: UITableViewCell {

    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    var iso8601String: String = ""
    var yearString: String = ""
    var monthString: String = ""
    var dateString: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValue(_ data: YoutubeSearchModel.Item) {
        videoImageView.kf.setImage(with: URL(string: data.snippet?.thumbnails?.medium?.url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        videoTitleLabel.text = data.snippet?.title
        iso8601String = data.snippet?.publishTime ?? ""
       
        let fullNameArray = iso8601String.components(separatedBy: "T")
        let dateNameArray = fullNameArray[0].components(separatedBy: "-")
        yearString = dateNameArray[0]
        monthString = dateNameArray[1]
        dateString = dateNameArray[2]
        
        let fullName = "\(yearString)年\(monthString)月\(dateString)日"
        daysLabel.text = fullName
    }

}
