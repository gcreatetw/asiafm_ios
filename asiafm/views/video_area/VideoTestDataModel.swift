//
//  VideoTestDataModel.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/22.
//

import Foundation
import UIKit

class VideoTestDataModel {
    let videoImageView: UIImage
    let title: String?
    let timesWatched: Int
    let days: String
    
    init(videoImage: UIImage, title: String?, timesWatched: Int, days: String) {
        self.videoImageView = videoImage
        self.title = title
        self.timesWatched = timesWatched
        self.days = days
    }
}
