//
//  ViewController.swift
//  SampleApp
//
//  Created by Aki Wang on 2020/10/21.
//

import UIKit
import SnapKit

class SplashViewController: BaseViewController {
    
    @IBOutlet weak var stackTitle: UIStackView!
    @IBOutlet weak var viewMessage: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .new_apac_bg
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initView()
        syncApi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        showAnimate()
    }
    
    private func initView(){
        stackTitle.snp.remakeConstraints { maker in
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
        }
        viewMessage.alpha = 0
    }
    
    private func showAnimate(){
        
        UIView.animate(withDuration: 2.0, animations: {
            self.stackTitle.snp.remakeConstraints { maker in
                maker.centerX.equalToSuperview()
                maker.centerY.equalToSuperview().multipliedBy(0.5)
            }
            self.view.layoutIfNeeded()
        }) { _ in
            //
        }
        
        UIView.animate(withDuration: 2.0, delay: 0.5, animations: {
            log(self.TAG, "animate")
            self.viewMessage.alpha = 1
        }) { _ in
            //
        }
    }
    
    private func syncApi(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            DispatchQueue(label: "api").async {
                // Sync api start
//                if let error = self.getApiTapieiYouBike() {
//                    DispatchQueue.main.async {
//                        self.showTextAlert(message: self.decodeError(error)) {
//                            // clean data
//                        }
//                    }
//                }
                // Sync api finish, go to TabBarController or LoginViewController ...
                DispatchQueue.main.async {
                    self.dismiss(animated: false)
                    if ud.isLogin() {
                        let storyboard = UIStoryboard.init(name: "Tab", bundle: nil)
                        if let next = storyboard.instantiateViewController(withClass: TabBarController.self) {
                            next.modalPresentationStyle = .fullScreen
                            self.present(next, animated: false)
                        }
                    } else {
                        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
                        if let next = storyboard.instantiateViewController(withClass: WellcomeViewController.self) {
                            next.modalPresentationStyle = .fullScreen
                            self.present(next, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    private func decodeError(_ error: ApiError) -> String{
        var result = ""
        if let message = error.errorJson["message"] as? String {
            result = message
        }
        if let errors = error.errorJson["errors"] as? Dictionary<String, Any> {
            for key in errors.keys {
                if let message = errors[key] as? [String] {
                    for str in message {
                        result.append(str.capitalizingFirstLetter())
                    }
                }
            }
        }
        return result
    }
    
    private func showTextAlert(message: String, closure:@escaping ()->()){
        let alertVc = AlertTextViewController.loadFromNib()
        //
        alertVc.messageString = message
        alertVc.onViewDidLoad = {
            alertVc.btnLeft.isHidden = true
        }
        alertVc.onClickRight = {
            alertVc.dismiss(animated: true, completion: {
                closure()
            })
        }
        alertVc.onClickLeft = {
            alertVc.dismiss(animated: true, completion: nil)
        }
        self.present(alertVc, animated: true, completion: nil)
    }
}

