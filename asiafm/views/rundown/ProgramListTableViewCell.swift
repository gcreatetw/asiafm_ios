//
//  ProgramListTableViewCell.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/19.
//

import UIKit

class ProgramListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var programImageView: UIImageView!
    
    @IBOutlet weak var programTitle: UILabel!
    
    @IBOutlet weak var programTime: UILabel!
    
    @IBOutlet weak var programLabel: UILabel!
    
    @IBOutlet weak var shadowView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setValue(_ data: RundownModel.Info) {
        programImageView.kf.setImage(with: URL(string: data.imgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        if programImageView.image == nil {
            programImageView.image = UIImage(named: "icon_Onair")
        }
    
        programTitle.text = data.eventTitle//?.htmlToString
        programTime.text = data.timeInterval
        programLabel.text = data.djName
        
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 5)
        shadowView.layer.shadowOpacity = 0.7
        shadowView.shadowColor = .lightGray
        shadowView.clipsToBounds = false
    }
    
    func setFlyValue(_ data: FlyradioChannelModel.Info) {
        programImageView.kf.setImage(with: URL(string: data.imgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
        if programImageView.image == nil {
            programImageView.image = UIImage(named: "icon_Onair")
        }
    
        programTitle.text = data.eventTitle//?.htmlToString
        programTime.text = data.timeInterval
        programLabel.text = data.djName
    }

}
