//
//  RundownViewController.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/5.
//

import UIKit
import FRadioPlayer

class RundownViewController: BaseViewController {
    
    private var rundownDataSource: [RundownModel.Info] = []
    var headerBackgroundView: UIImageView!
    var radioRemoteView: RadioRemoteView!
    
    let deviceSize: CGFloat = 413
    let smallSize: CGFloat = 389
    var dayId: Int = 0
    var asiaId: String = "asia927"
    
    @IBOutlet weak var vTitle: UIView!
    @IBOutlet weak var programListTableView: UITableView!
    @IBOutlet var weekDayButtonAry: [UIButton]!
    @IBOutlet var dotView: [UIView]!
    @IBOutlet weak var weekStackView: UIStackView!
    
    @IBAction func mondayBtnAct(_ sender: Any) {
        dayId = 1021
        syncApi(.MON)
        
        for dot in dotView {
            dot.isHidden = true
        }
        for hue in weekDayButtonAry {
            hue.setTitleColor(UIColor(named: "def_tab_unselected"), for: .normal)
        }
        dotView[0].isHidden = false
        weekDayButtonAry[0].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
    }
    
    @IBAction func tuesdayBtnAct(_ sender: Any) {
        dayId = 1022
        syncApi(.TUES)
        
        for dot in dotView {
            dot.isHidden = true
        }
        for hue in weekDayButtonAry {
            hue.setTitleColor(UIColor(named: "def_tab_unselected"), for: .normal)
        }
        dotView[1].isHidden = false
        weekDayButtonAry[1].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
    }
    
    @IBAction func wednesdayBtnAct(_ sender: Any) {
        dayId = 1023
        syncApi(.WED)
        
        for dot in dotView {
            dot.isHidden = true
        }
        for hue in weekDayButtonAry {
            hue.setTitleColor(UIColor(named: "def_tab_unselected"), for: .normal)
        }
        dotView[2].isHidden = false
        weekDayButtonAry[2].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
    }
    
    @IBAction func thursdayBtnAct(_ sender: Any) {
        dayId = 1024
        syncApi(.THUR)
        
        for dot in dotView {
            dot.isHidden = true
        }
        for hue in weekDayButtonAry {
            hue.setTitleColor(UIColor(named: "def_tab_unselected"), for: .normal)
        }
        dotView[3].isHidden = false
        weekDayButtonAry[3].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
    }
    
    @IBAction func fridayBtnAct(_ sender: Any) {
        dayId = 1025
        syncApi(.FRI)
        
        for dot in dotView {
            dot.isHidden = true
        }
        for hue in weekDayButtonAry {
            hue.setTitleColor(UIColor(named: "def_tab_unselected"), for: .normal)
        }
        dotView[4].isHidden = false
        weekDayButtonAry[4].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
    }
    
    @IBAction func saturdayBtnAct(_ sender: Any) {
        dayId = 1026
        syncApi(.SAT)
        
        for dot in dotView {
            dot.isHidden = true
        }
        for hue in weekDayButtonAry {
            hue.setTitleColor(UIColor(named: "def_tab_unselected"), for: .normal)
        }
        dotView[5].isHidden = false
        weekDayButtonAry[5].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
    }
    
    @IBAction func sundayBtnAct(_ sender: Any) {
        dayId = 1020
        syncApi(.SUN)
        
        for dot in dotView {
            dot.isHidden = true
        }
        for hue in weekDayButtonAry {
            hue.setTitleColor(UIColor(named: "def_tab_unselected"), for: .normal)
        }
        dotView[6].isHidden = false
        weekDayButtonAry[6].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
    }
    
//MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        initTitleView()
        initView()
        setWeekView()
        setWeekday()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        settingView()
        syncApi(ApiService.WeeklyType.allCases[Date().dayOfWeek()])
    }
  
    private func initTitleView(){
        // header 漸層背景
        headerBackgroundView = UIImageView().apply {
            vTitle.addSubview($0)
            $0.clipsToBounds = true
            $0.snp.makeConstraints { maker in
                maker.width.equalToSuperview()
                maker.top.equalToSuperview()
                maker.bottom.equalToSuperview()
            }
        }
        
        let lbTitle = UILabel().apply {
            vTitle.addSubview($0)
            $0.snp.makeConstraints { maker in
                maker.centerX.equalToSuperview()
                maker.top.equalToSuperview().offset(topSafeArea + 15)
            }
            $0.text = "節目表"
            $0.textColor = .txt_white
            $0.font = $0.font.withSize(20).bold
        }
        
        // radio remote view
        radioRemoteView = RadioRemoteView().apply {
            vTitle.addSubview($0)
            $0.delegate = self
            $0.isRealChangeChannel = false
            $0.snp.makeConstraints { maker in
                maker.top.equalTo(lbTitle.snp.bottom).offset(10)
                maker.width.equalToSuperview()
                maker.height.equalTo(110)
            }
        }
    }
    
    private func initView() {
        for dot in dotView {
            dot.isHidden = true
        }
        programListTableView.delegate = self
        programListTableView.dataSource = self
        programListTableView.separatorStyle = .singleLine
    }
    
    private func settingView(){
        let channel = RadioPlayer.shared.nowChannel
        radioRemoteView.scrollToRadioChannl(channel: channel, animated: false)
        changeRadioChannel(channel)
    }

    let player = FRadioPlayer.shared
    var nowChannel: RadioChannel = .ASIA
    func changeRadioChannel(_ channel: RadioChannel) {
        if let background = channel.background {
            headerBackgroundView.image = background
        }
    }
    
    func setWeekView() {
        if self.view.size.width >= deviceSize {weekStackView.spacing = 16}
        if self.view.size.width <= smallSize {weekStackView.spacing = 11}
    }
    
    func setWeekday() {
        let today = Date()
        let weekday = today.dayOfWeek()
        
        switch weekday {
        case 1:
            dotView[0].isHidden = false
            weekDayButtonAry[0].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
            dayId = 1021
        case 2:
            dotView[1].isHidden = false
            weekDayButtonAry[1].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
            dayId = 1022
        case 3:
            dotView[2].isHidden = false
            weekDayButtonAry[2].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
            dayId = 1023
        case 4:
            dotView[3].isHidden = false
            weekDayButtonAry[3].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
            dayId = 1024
        case 5:
            dotView[4].isHidden = false
            weekDayButtonAry[4].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
            dayId = 1025
        case 6:
            dotView[5].isHidden = false
            weekDayButtonAry[5].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
            dayId = 1026
        case 0:
            dotView[6].isHidden = false
            weekDayButtonAry[6].setTitleColor(UIColor(named: "def_tab_selected"), for: .normal)
            dayId = 1020
        default:
            return
        }
    }
    
    private var syncWeeklyType = ApiService.WeeklyType.MON
    private func syncApi(_ weeklyType: ApiService.WeeklyType) {
        DispatchQueue.init(label: "api").async {
            self.syncWeeklyType = weeklyType
            ApiService.getRundown(weeklyType, self.radioRemoteView.nowChannel).apply {
                if $0.isSuccess {
                    self.rundownDataSource = $0.data?.info ?? []
                }
            }
            DispatchQueue.main.async {
                self.programListTableView.reloadData()
            }
        }
    }
}

extension RundownViewController: RadioRemoteViewDelegate {
    func onChangeRadioChannel(channel: RadioChannel) {
        changeRadioChannel(channel)
        syncApi(syncWeeklyType)
    }
}

extension RundownViewController: RadioPlayerDelegate {
    func latestMusicListDidChange(_ latestMusicList: [ChannelInfoModel.LatestMusic], _ channel: RadioChannel) {}
    
    func playerChannelDidChange(_ playerState: FRadioPlayerState, _ channel: RadioChannel) {
//        if playerState != .loading {
//            return
//        }
        radioRemoteView.scrollToRadioChannl(channel: channel, animated: false)
        changeRadioChannel(channel)
    }
    
    func playbackChannelDidChange(_ playbackState: FRadioPlaybackState, _ channel: RadioChannel) {}
}

extension RundownViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rundownDataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(ProgramListTableViewCell.self)", for: indexPath) as! ProgramListTableViewCell
        if indexPath.row < rundownDataSource.count {
            cell.setValue(rundownDataSource[indexPath.row])
        }
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let next = UIStoryboard.Website.instantiateViewController(withClass: WebSiteViewController.self) {
            next.modalPresentationStyle = .fullScreen
            next.hero.isEnabled = true
            next.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .push(direction: .right))
            
            next.url = rundownDataSource[indexPath.row].guid ?? ""
            next.setTitleLabel(title: "DJ介紹")
            self.present(next, animated: true)
        }
    }
}
