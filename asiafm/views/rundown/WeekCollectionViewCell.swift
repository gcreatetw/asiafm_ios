//
//  WeekCollectionViewCell.swift
//  asiafm
//
//  Created by StevenTang on 2021/7/19.
//

import UIKit

class WeekCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var poleView: UIView!
}
