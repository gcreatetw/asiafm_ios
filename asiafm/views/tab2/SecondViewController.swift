//
//  SecondViewController.swift
//  SampleApp
//
//  Created by Aki Wang on 2020/10/21.
//

import UIKit

class SecondViewController: BaseViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func onClickBack(){
        dismiss(animated: true)
    }
}
