//
//  Tab2ViewController.swift
//  SampleApp
//
//  Created by Aki Wang on 2020/10/22.
//

import UIKit

class Tab2ViewController: BaseViewController {
    
    @IBOutlet weak var btnGoToSecond: UIButton!
    
    private var dataList = [BikeStationEntity]()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initData()
    }
    
    private func initData(){
        //dataList = db.bikeStation.fetchAll()
        log(TAG, "dataList.count is \(dataList.count)")
        dataList.forEach() {
            log(TAG, "\($0.sno ?? "") \($0.sna ?? "")")
        }
    }

    @IBAction func onClickGoToSecond(){
        if let next = storyboard?.instantiateViewController(withClass: SecondViewController.self) {
            next.modalPresentationStyle = .fullScreen
            self.present(next, animated: true, completion: nil)
        }
    }

}
