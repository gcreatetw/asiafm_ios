//
//  RadioRemoteView.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/14.
//

import Foundation
import UIKit

struct RadioChannel: Equatable, Hashable {
    let id: Int
    let title: String
    let image: String
    let backgroundColor: UIColor
    let background: UIImage?
    let streamURL: String
    let albumImage: UIImage?
    private init(id: Int, title: String, image: String, backgroundColor: UIColor, background: UIImage?, streamURL: String, albumImage: UIImage?) {
        self.id = id
        self.title = title
        self.image = image
        self.backgroundColor = backgroundColor
        self.background = background
        self.streamURL = streamURL
        self.albumImage = albumImage
    }
    
    /// 飛揚 895
    static let FLY = RadioChannel(id: 3, title: "飛揚電台", image: "circle89.5", backgroundColor: .def_fly_bg, background: UIImage(named: "fly_top_bg"), streamURL: "https://stream.rcs.revma.com/e0tdah74hv8uv", albumImage: UIImage(named: "正在播放_895"))
    /// 亞洲 927
    static let ASIA = RadioChannel(id: 1, title: "亞洲電台", image: "circle92.7", backgroundColor: .def_asia_bg, background: UIImage(named: "asia_top_bg"), streamURL: "https://stream.rcs.revma.com/xpgtqc74hv8uv", albumImage: UIImage(named: "正在播放_927"))
    /// 亞太 923
    static let APAC = RadioChannel(id: 2, title: "亞太電台", image: "circle92.3", backgroundColor: .def_apac_bg, background: UIImage(named: "apac_top_bg"), streamURL: "https://stream.rcs.revma.com/kydend74hv8uv", albumImage: UIImage(named: "正在播放_923"))
    /// all item
    static let items = [ASIA, FLY, APAC]
    
    func nextChannal() -> RadioChannel {
        let idx = RadioChannel.items.firstIndex(of: self) ?? 0
        if idx + 1 < RadioChannel.items.count {
            return RadioChannel.items[idx + 1]
        } else {
            return RadioChannel.items[0]
        }
    }
    
    func prevChannal() -> RadioChannel {
        let idx = RadioChannel.items.firstIndex(of: self) ?? 0
        if idx - 1 >= 0 {
            return RadioChannel.items[idx - 1]
        } else {
            return RadioChannel.items[RadioChannel.items.count - 1]
        }
    }
}

//不用理這個
enum RadioType: Int, CaseIterable {
    /// 飛揚
    case FLY = 0
    /// 亞洲
    case ASIA = 1
    /// 亞太
    case APAC = 2
}

protocol RadioRemoteViewDelegate {
    func onChangeRadioChannel(channel: RadioChannel)
}

class RadioRemoteView: UIView {
    
    var view: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: RadioRemoteViewDelegate?
    
    var nowIndex: Int {
        get {
            //DispatchQueue.main.async { }
            let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
            if let cell = collectionView.visibleCells.min(by: { abs(visibleRect.center.x - $0.center.x) < abs(visibleRect.center.x - $1.center.x) }) {
                return collectionView.indexPath(for: cell)?.row ?? 0
            }
            return 0
        }
    }
    
    var isRealChangeChannel = true
    
    var nowChannel: RadioChannel {
        get {
            return .items[nowIndex % RadioChannel.items.count]
        }
    }
    
    override class func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
     
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
     
    private func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        initCollectionView()
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "RadioRemoteView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
         
        return view
    }
    
    private func initCollectionView() {
        collectionView.apply {
            let layout = ZoomToCenterFlowLayout()
            $0.setCollectionViewLayout(layout, animated: false)
            $0.dataSource = self
            $0.delegate = self
            let nib = UINib(nibName: "RadioRemoteCollectionViewCell", bundle: nil)
            $0.register(nib, forCellWithReuseIdentifier: "RadioRemoteCollectionViewCell")
            $0.showsHorizontalScrollIndicator = false
            $0.reloadData()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.scrollToItem(row: self.collectionView.numberOfItems() / 2, animated: false)
        scrollToRadioChannl(channel: RadioPlayer.shared.nowChannel, animated: false)
    }
    
    func scrollToItem(row: Int, scrollPosition: UICollectionView.ScrollPosition = .centeredHorizontally, animated: Bool = true) {
        collectionView.scrollToItem(at: IndexPath(row: row, section: 0), at: scrollPosition, animated: animated)
    }
    
    func scrollToRadioChannl(channel: RadioChannel, animated: Bool){

        let items = RadioChannel.items
        let index = items.firstIndex { $0 == channel } ?? 0
        let nowIndex = self.collectionView.numberOfItems() / 2
        scrollToItem(row: nowIndex + index, animated: animated)
    }
}

extension RadioRemoteView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return RadioChannel.items.count * 100000
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RadioRemoteCollectionViewCell", for: indexPath) as! RadioRemoteCollectionViewCell
        let idx = indexPath.row % RadioChannel.items.count
        cell.setValue(indexPath.row, .items[idx])
        return cell
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        log(TAG, "scrollViewDidEndDragging \(decelerate)")
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        log(TAG, "scrollViewDidEndScrollingAnimation")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
        if let cell = collectionView.visibleCells.min(by: { abs(visibleRect.center.x - $0.center.x) < abs(visibleRect.center.x - $1.center.x) }) {
            let index = collectionView.indexPath(for: cell)?.row ?? 0
            let channel = RadioChannel.items[index % RadioChannel.items.count]
            if RadioPlayer.shared.nowChannel == channel {
                return
            }
            if isRealChangeChannel == true {
                RadioPlayer.shared.play(channel: channel)
            }
            delegate?.onChangeRadioChannel(channel: channel)
            log(TAG, "index \(index) \(channel.title) \(channel.id)")
        }
    }
}
