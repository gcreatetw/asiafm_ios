//
//  ZoomAndSnapFlowLayout.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/6.
//

import UIKit

class ZoomToCenterFlowLayout: UICollectionViewFlowLayout {

    let activeDistance: CGFloat = 100
    let zoomFactor: CGFloat = 0.2
    let minZoom: CGFloat = 0.7
    var isPagingEnabled = true

    override init() {
        super.init()
        if UIDevice.isPhone() {
            scrollDirection = .horizontal
            minimumLineSpacing = 4
            itemSize = CGSize(width: 80, height: 80)
        }else if UIDevice.isPad() {
            scrollDirection = .horizontal
            minimumLineSpacing = 4
            itemSize = CGSize(width: 100, height: 100)
        }
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepare() {
        guard let collectionView = collectionView else { fatalError() }
        let verticalInsets = (collectionView.frame.height - collectionView.adjustedContentInset.top - collectionView.adjustedContentInset.bottom - itemSize.height) / 2
        let horizontalInsets = (collectionView.frame.width - collectionView.adjustedContentInset.right - collectionView.adjustedContentInset.left - itemSize.width) / 2
        sectionInset = UIEdgeInsets(top: verticalInsets, left: horizontalInsets, bottom: verticalInsets, right: horizontalInsets)

        super.prepare()
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let collectionView = collectionView else { return nil }
//        let rectAttributes2 = super.layoutAttributesForElements(in: rect)?.map { $0.copy() as! UICollectionViewLayoutAttributes } ?? []
        let rectAttributes = super.layoutAttributesForElements(in: rect) ?? []
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)

        // Make the cells be zoomed when they reach the center of the screen
        for attributes in rectAttributes where attributes.frame.intersects(visibleRect) {
//            let distance = visibleRect.midX - attributes.center.x
//            let normalizedDistance = distance / activeDistance

//            if distance.magnitude < activeDistance {
//                let zoom = 1 + zoomFactor * (1 - normalizedDistance.magnitude)
//                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1)
//                attributes.zIndex = Int(zoom.rounded())
//            }
            //
            let distance = abs(visibleRect.midX - attributes.center.x)
            if UIDevice.isPhone() {
                let zoom = 1 - (distance / (collectionView.frame.width / 2.3))
                var zoom2 = zoom * 1.5
                if zoom2 > 1 {
                    zoom2 = 1
                }
                if zoom2 < 0.25 {
                    zoom2 = 0
                }
                attributes.alpha = zoom2
                attributes.transform3D = CATransform3DMakeScale(zoom2, zoom2, 1)
            }else if UIDevice.isPad() {
                let zoom = 1 - (distance / (collectionView.frame.width / 5))
                var zoom2 = zoom * 1.5
                if zoom2 > 1 {
                    zoom2 = 1
                }
                if zoom2 < 0.25 {
                    zoom2 = 0
                }
                attributes.alpha = zoom2
                attributes.transform3D = CATransform3DMakeScale(zoom2, zoom2, 1)
            }
            
//            if zoom < minZoom {
//                attributes.alpha = minZoom
//                attributes.transform3D = CATransform3DMakeScale(minZoom, minZoom, 0)
//            }
//            else {
//                attributes.alpha = zoom
//                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1)
//            }
        }

        return rectAttributes
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else { return .zero }
        if isPagingEnabled == true {
            // Page width used for estimating and calculating paging.
            let pageWidth = itemSize.width + minimumLineSpacing
            // Make an estimation of the current page position.
            let approximatePage = collectionView.contentOffset.x / pageWidth
            // Determine the current page based on velocity.
            let currentPage = velocity.x == 0 ? round(approximatePage) : (velocity.x < 0.0 ? floor(approximatePage) : ceil(approximatePage))
            // Create custom flickVelocity.
            let flickVelocity = velocity.x * 0.3
            // Check how many pages the user flicked, if <= 1 then flickedPages should return 0.
            let flickedPages = (abs(round(flickVelocity)) <= 1) ? 0 : round(flickVelocity)
            let newHorizontalOffset = ((currentPage + flickedPages) * pageWidth) - collectionView.contentInset.top
//            log("TAG", "index is \(newHorizontalOffset / (itemSize.width + minimumLineSpacing))")
            return CGPoint(x: newHorizontalOffset, y: proposedContentOffset.y)
        } else {
            // Add some snapping behaviour so that the zoomed cell is always centered
            let targetRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionView.frame.width, height: collectionView.frame.height)
            guard let rectAttributes = super.layoutAttributesForElements(in: targetRect) else { return .zero }
            var offsetAdjustment = CGFloat.greatestFiniteMagnitude
            let horizontalCenter = proposedContentOffset.x + collectionView.frame.width / 2

            for layoutAttributes in rectAttributes {
                let itemHorizontalCenter = layoutAttributes.center.x
                if (itemHorizontalCenter - horizontalCenter).magnitude < offsetAdjustment.magnitude {
                    offsetAdjustment = itemHorizontalCenter - horizontalCenter
                }
            }
//            let x = proposedContentOffset.x + offsetAdjustment
//            log("TAG", "index is \(x / (itemSize.width + minimumLineSpacing))")
            return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
        }
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        // Invalidate layout so that every cell get a chance to be zoomed when it reaches the center of the screen
        return true
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
}
