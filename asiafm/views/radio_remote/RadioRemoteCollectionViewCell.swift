//
//  RadioRemoteCollectionViewCell.swift
//  asiafm
//
//  Created by Aki Wang on 2021/7/15.
//

import UIKit

class RadioRemoteCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var bg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
        if UIDevice.isPhone() {
            bg.layer.masksToBounds = false
            bg.layer.cornerRadius = 40
            bg.clipsToBounds = true
        }else if UIDevice.isPad() {
            bg.layer.masksToBounds = false
            bg.layer.cornerRadius = 50
            bg.clipsToBounds = true
        }
        
    }
    
    func setValue(_ idx: Int, _ channel: RadioChannel){
        iv.image = UIImage(named: channel.image)
    }

}
