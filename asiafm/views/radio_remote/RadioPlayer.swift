//
//  RadioPlayer.swift
//  SwiftRadio
//
//  Created by Fethi El Hassasna on 2018-01-05.
//  Copyright © 2018 matthewfecher.com. All rights reserved.
//

import UIKit
import FRadioPlayer
import MediaPlayer

// Track struct
struct Track {
    var title: String
    var artist: String
    var artworkImage: UIImage?
    var artworkLoaded = false
    
    init(title: String, artist: String) {
        self.title = title
        self.artist = artist
    }
}

//MARK: - RadioPlayerDelegate: Sends FRadioPlayer and Station/Track events

protocol RadioPlayerDelegate: AnyObject {
    func playerChannelDidChange(_ playerState: FRadioPlayerState, _ channel: RadioChannel)
    func playbackChannelDidChange(_ playbackState: FRadioPlaybackState, _ channel: RadioChannel)
    func latestMusicListDidChange(_ latestMusicList: [ChannelInfoModel.LatestMusic], _ channel: RadioChannel)
}

//MARK: - RadioPlayer: App Radio Player

class RadioPlayer: NSObject {
    
    private static let instance = RadioPlayer()
    private let TAG = "RadioPlayer"
    
    static var shared : RadioPlayer { return instance }
    weak var delegate: RadioPlayerDelegate?
    
    var nowChannel: RadioChannel = .ASIA
    let player = FRadioPlayer.shared
    
    override init() {
        super.init()
        player.delegate = self
        player.isAutoPlay = true
        player.enableArtwork = true
        player.artworkSize = 600
        startTimer()
        nowChannel = RadioChannel.items.first(where: { $0.id == ud.getLastTimeChannel() }) ?? RadioChannel.ASIA
    }
    
    func play(channel: RadioChannel? = nil) {
        showMPNowPlayingInfoCenter()
        if player.radioURL != nil {
            player.isAutoPlay = player.playbackState == .playing
        }
        // play
        let channel = channel ?? nowChannel
        if player.radioURL?.absoluteString != channel.streamURL {
            nowChannel = channel
            player.radioURL = URL(string: channel.streamURL)
        } else {
            player.togglePlaying()
        }
        updateMPNowPlayingInfoCenter(channel: channel)
        
        if let delegate = UIApplication.topViewController() as? RadioPlayerDelegate {
            delegate.latestMusicListDidChange(latestMusicList, channel)
            ud.setLastTimeChannel(channel: channel.id)
        }
        if let delegate = UIApplication.topTabViewController() as? RadioPlayerDelegate {
            delegate.latestMusicListDidChange(latestMusicList, channel)
            ud.setLastTimeChannel(channel: channel.id)
        }
    }
    
    func togglePlaying() {
        player.togglePlaying()
    }
    
    func pause() {
        player.pause()
    }
    
    func stop() {
        player.stop()
    }
    
    func nextChannel() {
        nowChannel = nowChannel.nextChannal()
        play()
    }
    
    func prevChannal() {
        nowChannel = nowChannel.prevChannal()
        play()
    }
    
// MARK: - ChannelInfo Api Sync
    //封面更新 5分鐘
    var apiTimer: Timer?
    private func startTimer() {
        stopTimer()
        syncApi()
        apiTimer = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(syncApi), userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        apiTimer?.invalidate()
    }
    
    var latestMusicList : [ChannelInfoModel.LatestMusic] {
        get {
            return latestMusicDict[nowChannel] ?? []
        }
    }
    private var latestMusicDict: [RadioChannel : [ChannelInfoModel.LatestMusic]] = [:]
    @objc func syncApi(){
        for channel in RadioChannel.items {
            DispatchQueue.init(label: "\(TAG)_\(channel.id)").async {
                var hasChange = false
                let repsonse = ApiService.getChannelInfo(channel)
                if repsonse.isSuccess {
                    if let lastetMusic = self.latestMusicDict[channel], lastetMusic.first?.song_name != repsonse.data?.latestMusic?.first?.song_name {
                        hasChange = true
                    }
                   // self.latestMusicDict[channel] = repsonse.data?.latestMusic ?? []
                }
                DispatchQueue.main.async {
                    //self.nowChannel == channel && hasChange == true 會Crash
                    if self.nowChannel == channel || hasChange == true {
                        if let delegate = UIApplication.topViewController() as? RadioPlayerDelegate {
                            delegate.latestMusicListDidChange(self.latestMusicList, self.nowChannel)
                        }
                        if let delegate = UIApplication.topTabViewController() as? RadioPlayerDelegate {
                            delegate.latestMusicListDidChange(self.latestMusicList, self.nowChannel)
                        }
                    }
                }
            }
        }
    }
        
// MARK: - MPNowPlayingInfoCenter
    
    private var nowPlayingInfoCenterIsShow = false
    func showMPNowPlayingInfoCenter() {
        if nowPlayingInfoCenterIsShow == true {
            return
        }
        nowPlayingInfoCenterIsShow = true
        UIApplication.shared.beginReceivingRemoteControlEvents()
    }
    
    func dismissMPNowPlayingInfoCenter(){
        nowPlayingInfoCenterIsShow = false
        UIApplication.shared.beginReceivingRemoteControlEvents()
    }
    
    private func updateMPNowPlayingInfoCenter(channel: RadioChannel) {
        var nowPlayingInfo = [String : Any]()
        
        nowPlayingInfo[MPMediaItemPropertyTitle] = channel.title
        if let image = UIImage(named: channel.image) {
            nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: image.size, requestHandler: { size -> UIImage in
                return image
            })
        }
        
        nowPlayingInfo[MPNowPlayingInfoPropertyIsLiveStream] = true
        // Set the metadata
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
}

extension RadioPlayer: FRadioPlayerDelegate {
    
    func radioPlayer(_ player: FRadioPlayer, playerStateDidChange state: FRadioPlayerState) {
        updateMPNowPlayingInfoCenter(channel: nowChannel)
        delegate?.playerChannelDidChange(state, nowChannel)
        if let delegate = UIApplication.topViewController() as? RadioPlayerDelegate {
            delegate.playerChannelDidChange(state, nowChannel)
        }
        if let delegate = UIApplication.topTabViewController() as? RadioPlayerDelegate {
            delegate.playerChannelDidChange(state, nowChannel)
        }
    }
    
    func radioPlayer(_ player: FRadioPlayer, playbackStateDidChange state: FRadioPlaybackState) {
        updateMPNowPlayingInfoCenter(channel: nowChannel)
        delegate?.playbackChannelDidChange(state, nowChannel)
        if let delegate = UIApplication.topViewController() as? RadioPlayerDelegate {
            delegate.playbackChannelDidChange(state, nowChannel)
        }
        if let delegate = UIApplication.topTabViewController() as? RadioPlayerDelegate {
            delegate.playbackChannelDidChange(state, nowChannel)
        }
    }
}

//*****************************************************************
// MARK: - Track loading/updates
//*****************************************************************

// Update the track with an artist name and track name
//    func updateTrackMetadata(artistName: String, trackName: String) {
//        if track == nil {
//            track = Track(title: trackName, artist: artistName)
//        } else {
//            track?.title = trackName
//            track?.artist = artistName
//        }
//
//        delegate?.trackDidUpdate(track)
//    }
//
//    // Update the track artwork with a UIImage
//    func updateTrackArtwork(with image: UIImage, artworkLoaded: Bool) {
//        track?.artworkImage = image
//        track?.artworkLoaded = artworkLoaded
//        delegate?.trackArtworkDidUpdate(track)
//    }
//
//    // Reset the track metadata and artwork to use the current station infos
//    func resetTrack(with station: RadioChannel?) {
//        guard let station = station else { track = nil; return }
//        updateTrackMetadata(artistName: station.title, trackName: station.title)
//        resetArtwork(with: station)
//    }
//
//    // Reset the track Artwork to current station image
//    func resetArtwork(with station: RadioChannel?) {
//        guard let station = station else { track = nil; return }
//        getStationImage(from: station) { image in
//            self.updateTrackArtwork(with: image, artworkLoaded: false)
//        }
//    }

//*****************************************************************
// MARK: - Private helpers
//*****************************************************************

//    private func getStationImage(from station: RadioChannel, completionHandler: @escaping (_ image: UIImage) -> ()) {
//
//        if station.imageURL.range(of: "http") != nil {
//            // load current station image from network
//            ImageLoader.sharedLoader.imageForUrl(urlString: station.imageURL) { (image, stringURL) in
//                completionHandler(image ?? #imageLiteral(resourceName: "albumArt"))
//            }
//        } else {
//            // load local station image
//            let image = UIImage(named: station.imageURL) ?? #imageLiteral(resourceName: "albumArt")
//            completionHandler(image)
//        }
//    }
